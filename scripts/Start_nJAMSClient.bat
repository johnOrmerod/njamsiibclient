@echo off

setlocal
rem version and app
set version=Beta-1.0.4.0
set app=nJAMS Client for IIB

title App=%app% version=%version%
rem Start the nJAMS Client [use javaw if you don't want a console window]
"%JAVA_HOME%\bin\java" -Dversion=%version% -DclientRunMode=DEV -DprodFastStart=true -DclientSleepInterval=60000 -DconfigName=njamsDev_IIB_DEV_JMS_TestAll_Win.xml -DconfigSchema=njamsClientProperties.xsd -DconfigFolder=%cd%\conf -Dlog4j.configurationFile=file:///%cd%\conf\log4j2.xml -jar njamsClient-%version%.jar

endlocal
