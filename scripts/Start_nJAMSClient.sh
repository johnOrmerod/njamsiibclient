#!/bin/sh
#
# Based on: StartIntegrationAPIExerciser.sh
#
# It is assumed that an appropriate environment has been set up to
# run this command. Sourcing 'mqsiprofile' is not necessary to run
# IntegrationAPI applications, though this script assumes this to be the case;
# for all IntegrationAPI applications, the following JARs MUST be in the CLASSPATH:
#     IntegrationAPI.jar
# Java and its core libraries must also be available.
#
# This sample also requires IntegrationAPISamples.jar; this
# is where the compiled sample can be found.
#CLASSPATH=$MQSI_FILEPATH/sample/IntegrationAPI/IntegrationAPISamples.jar:$CLASSPATH

# version and app
version=beta-1.0.0-SNAPSHOT
app=HTTPInput+AddressLookup

INSTALLPATH=/opt/iib/iib-10.0.0.3/server
MQSI_FILEPATH=$INSTALLPATH
export INSTALLPATH

# Run mqsiprofile
if [ -r "$MQSI_FILEPATH/bin/mqsiprofile" ]
then
	echo mqsiprofile found
  . "$MQSI_FILEPATH/bin/mqsiprofile"
fi

# Provide access to the MQ Native libraries (JNI)
export LD_LIBRARY_PATH=/opt/mqm/java/lib64:$LD_LIBRARY_PATH
#echo ld-lib-path2 = $LD_LIBRARY_PATH


#nJAMS Client install dir is current directory
CLIENT_INSTALL_DIR=$PWD
echo client dir= $CLIENT_INSTALL_DIR
# export CLIENT_INSTALL_DIR

# title App=%app% version=%version%
echo App=$app Version=$version

nohup java -Dversion=$version -DclientRunMode=DEV -DprodFastStart=true -DclientSleepInterval=60000 -DconfigName=njams_appTestingClientProperties.xml -DconfigSchema=njamsClientProperties.xsd -DconfigFolder=$PWD/conf -Dlog4j.configurationFile=file:///$PWD/conf/log4j2.xml -jar nJAMS_Client_$version.jar > /dev/null 2>&1 &
