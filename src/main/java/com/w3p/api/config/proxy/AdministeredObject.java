package com.w3p.api.config.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(AdministeredObject.class);
	protected com.ibm.broker.config.proxy.AdministeredObject adminObject;

	public AdministeredObject() {
	}
	
	public abstract String getName() throws ConfigManagerProxyPropertyNotInitializedException;
	
	public void setAdminObject(com.ibm.broker.config.proxy.AdministeredObject adminObject) {
		logger.trace("setAdminObjec() - entry");
		this.adminObject = adminObject;
		logger.trace("setAdminObjec() - exit");
	}

	public boolean hasBeenPopulatedByBroker() {
		return adminObject.hasBeenPopulatedByBroker();
	}
}
