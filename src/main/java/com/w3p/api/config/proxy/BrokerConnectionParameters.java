package com.w3p.api.config.proxy;

public interface BrokerConnectionParameters {
	
	public String getIntegrationNodeHost();

	public int getIntegrationNodePort();

	public String getUsername();
	
	public String getPassword();

	public boolean isUseSSL();
}
