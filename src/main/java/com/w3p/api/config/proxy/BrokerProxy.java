package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

//import com.ibm.broker.config.proxy.LogEntry;

//import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;
//import com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
//import com.ibm.broker.config.proxy.ConfigurableService;
//import com.ibm.broker.config.proxy.ExecutionGroupProxy;

public class BrokerProxy extends AdministeredObject {
	
	private static com.ibm.broker.config.proxy.BrokerProxy ibmBrokerProxy;
	private static BrokerProxy INSTANCE;

	private BrokerProxy() {
	}
	
	public static BrokerProxy getInstance(BrokerConnectionParameters bcp) throws ConfigManagerProxyLoggedException {
	
		if(INSTANCE == null) {
            try {
            	com.ibm.broker.config.proxy.BrokerConnectionParameters iibConnParms = 
            		new com.ibm.broker.config.proxy.IntegrationNodeConnectionParameters(bcp.getIntegrationNodeHost(), bcp.getIntegrationNodePort(), bcp.getUsername(), bcp.getPassword(), bcp.isUseSSL());
            	ibmBrokerProxy = com.ibm.broker.config.proxy.BrokerProxy.getInstance(iibConnParms);
            	INSTANCE = new BrokerProxy();
            	INSTANCE.setAdminObject(ibmBrokerProxy.getParent());
			} catch (com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException e) {
				throw new ConfigManagerProxyLoggedException(e);
			}
        }
		return INSTANCE;
	}
	
	public com.ibm.broker.config.proxy.BrokerProxy getBrokerProxy() {
		return ibmBrokerProxy; 
	}
	
	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return ibmBrokerProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}

	public ExecutionGroupProxy getExecutionGroupByName(String name) throws ConfigManagerProxyPropertyNotInitializedException {		
		 try {
			return new ExecutionGroupProxy(ibmBrokerProxy.getExecutionGroupByName(name));
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
	
	
	public List<ExecutionGroupProxy> getExecutionGroups() throws ConfigManagerProxyPropertyNotInitializedException {
		Properties props = new Properties();
		try {
			List<com.ibm.broker.config.proxy.ExecutionGroupProxy> iibEGProxies = Collections.list(ibmBrokerProxy.getExecutionGroups(props));
			List<ExecutionGroupProxy> egProxies = new ArrayList<>();
			iibEGProxies.forEach(iibegp -> {
				ExecutionGroupProxy egp = new ExecutionGroupProxy(iibegp);
				egProxies.add(egp);
			});
			return egProxies;
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}

	public void createConfigurableService(String serviceType, String serviceName) throws ConfigManagerProxyLoggedException, IllegalArgumentException {
		try {
			ibmBrokerProxy.createConfigurableService(serviceType, serviceName);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException e) {
			throw new ConfigManagerProxyLoggedException(e);
		}
	}
	
	public ConfigurableService getConfigurableService(String serviceType, String serviceName) throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			com.ibm.broker.config.proxy.ConfigurableService iibConfigService = ibmBrokerProxy.getConfigurableService(serviceType, serviceName);
			if (iibConfigService != null) {
				return new ConfigurableService(iibConfigService);
			} else {
				return null;
			}
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
	
	public void setSynchronous(int timeToWaitMs) {
		ibmBrokerProxy.setSynchronous(timeToWaitMs);
	}
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return ibmBrokerProxy.isRunning();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}

	@Override
	public String toString() {
		return ibmBrokerProxy.toString();
	}
}
