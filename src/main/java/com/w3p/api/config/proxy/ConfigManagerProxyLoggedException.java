package com.w3p.api.config.proxy;

public class ConfigManagerProxyLoggedException extends Exception {

	private static final long serialVersionUID = 1L; 

	private final com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException iibConfigManagerProxyLoggedException;

	public ConfigManagerProxyLoggedException(com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException iibConfigManagerProxyLoggedException) {
		this.iibConfigManagerProxyLoggedException = iibConfigManagerProxyLoggedException;
	}

	public Throwable getCause() {
		return iibConfigManagerProxyLoggedException.getCause();
	}

	public String getLocalizedMessage() {
		return iibConfigManagerProxyLoggedException.getLocalizedMessage();
	}

	public String getMessage() {
		return iibConfigManagerProxyLoggedException.getMessage();
	}

	public String getServiceInformation() {
		return iibConfigManagerProxyLoggedException.getServiceInformation();
	}

	public StackTraceElement[] getStackTrace() {
		return iibConfigManagerProxyLoggedException.getStackTrace();
	}
}