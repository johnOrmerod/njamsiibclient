package com.w3p.api.config.proxy;

public class ConfigManagerProxyPropertyNotInitializedException extends Exception {

	private static final long serialVersionUID = 1L;

	private final com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException iibConfigManagerProxyPropertyNotInitializedException;

	public ConfigManagerProxyPropertyNotInitializedException(com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException iibConfigManagerProxyPropertyNotInitializedException) {
		this.iibConfigManagerProxyPropertyNotInitializedException = iibConfigManagerProxyPropertyNotInitializedException;
	}

	public Throwable getCause() {
		return iibConfigManagerProxyPropertyNotInitializedException.getCause();
	}

	public String getLocalizedMessage() {
		return iibConfigManagerProxyPropertyNotInitializedException.getLocalizedMessage();
	}

	public String getMessage() {
		return iibConfigManagerProxyPropertyNotInitializedException.getMessage();
	}

	public StackTraceElement[] getStackTrace() {
		return iibConfigManagerProxyPropertyNotInitializedException.getStackTrace();
	}
}