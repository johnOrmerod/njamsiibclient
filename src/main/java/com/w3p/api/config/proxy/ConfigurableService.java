package com.w3p.api.config.proxy;

import java.util.Properties;

public class ConfigurableService {

	private final com.ibm.broker.config.proxy.ConfigurableService iibConfigurableService;
	
	public ConfigurableService(com.ibm.broker.config.proxy.ConfigurableService iibConfigurableService) {
		this.iibConfigurableService = iibConfigurableService;
	}
	
	public String getName() {
		return iibConfigurableService.getName();
	}

	public Properties getProperties() {
		return iibConfigurableService.getProperties();
	}

	public String getType() {
		return iibConfigurableService.getType();
	}

	public void setProperty(String propertyName, String propertyValue) throws ConfigManagerProxyLoggedException {
		try {
			iibConfigurableService.setProperty(propertyName, propertyValue);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException e) {
			throw new ConfigManagerProxyLoggedException(e);
		}
		
	}

	public void delete() throws ConfigManagerProxyLoggedException {
		try {
			iibConfigurableService.delete(); 
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException e) {
			throw new ConfigManagerProxyLoggedException(e);
		}		
	}
}
