package com.w3p.api.config.proxy;

public abstract class DeployableProxy extends AdministeredObject {

	public DeployableProxy() {
		super();

	}

	@Override
	public abstract String getName() throws ConfigManagerProxyPropertyNotInitializedException;
	
	public abstract boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException;	

}
