package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;

public class ExecutionGroupProxy extends AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(ExecutionGroupProxy.class);
	private final com.ibm.broker.config.proxy.ExecutionGroupProxy iibExecutionGroupProxy;

	public ExecutionGroupProxy(com.ibm.broker.config.proxy.ExecutionGroupProxy iibExecutionGroupProxy) {
		this.iibExecutionGroupProxy = iibExecutionGroupProxy;
		try {
			super.setAdminObject(iibExecutionGroupProxy.getParent());
		} catch (ConfigManagerProxyLoggedException e) {
			// Avoid the constructor having to throw an exception
			String name = this.getClass().getSimpleName();
			logger.error("Exception getting the IIB "+name+"'s parent. In constructor and avoiding it throwing an exception. This will cause a leter error.", e);			
		}
	}

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibExecutionGroupProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}

	public Enumeration<DeployableProxy> getApplications(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<DeployableProxy> appProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.ApplicationProxy> iibApps = Collections.list(iibExecutionGroupProxy.getApplications(props));
			iibApps.forEach(iibap -> {
				ApplicationProxy ap = new ApplicationProxy(iibap);
				appProxies.add(ap);
			});	
			return Collections.enumeration(appProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<DeployableProxy> getServices(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<DeployableProxy> serviceProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.ApplicationProxy> iibApps = Collections.list(iibExecutionGroupProxy.getApplications(props));
			iibApps.forEach(iibap -> {
				ServiceProxy sp = new ServiceProxy(iibap);
				serviceProxies.add(sp);
			});	
			return Collections.enumeration(serviceProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<SharedLibraryProxy> getSharedLibraries(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<SharedLibraryProxy> slProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.SharedLibraryProxy> iibSLs = Collections.list(iibExecutionGroupProxy.getSharedLibraries(props));
			iibSLs.forEach(iibsl -> {
				SharedLibraryProxy slp = new SharedLibraryProxy(iibsl);
				slProxies.add(slp);
			});	
			return Collections.enumeration(slProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<StaticLibraryProxy> getStaticLibraries(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<StaticLibraryProxy> slProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.StaticLibraryProxy> iibSLs = Collections.list(iibExecutionGroupProxy.getStaticLibraries(props));
			iibSLs.forEach(iibsl -> {
				StaticLibraryProxy slp = new StaticLibraryProxy(iibsl);
				slProxies.add(slp);
			});	
			return Collections.enumeration(slProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<DeployableProxy> getRestApis(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<DeployableProxy> raProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.RestApiProxy> iibRAs = Collections.list(iibExecutionGroupProxy.getRestApis(props));
			iibRAs.forEach(iibra -> {
				RestApiProxy rap = new RestApiProxy(iibra);
				raProxies.add(rap);
			});	
			return Collections.enumeration(raProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibExecutionGroupProxy.isRunning();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
}
