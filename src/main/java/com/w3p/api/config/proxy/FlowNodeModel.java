package com.w3p.api.config.proxy;

public class FlowNodeModel {
	
	private final com.ibm.broker.config.proxy.MessageFlowProxy.Node iibFlowNode;

	public FlowNodeModel(com.ibm.broker.config.proxy.MessageFlowProxy.Node iibFlowNode) {
		this.iibFlowNode = iibFlowNode;
	}
	
	public String getName() {
		return iibFlowNode.getName();
	}

}
