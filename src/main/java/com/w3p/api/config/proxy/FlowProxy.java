package com.w3p.api.config.proxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;

public class FlowProxy extends AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(FlowProxy.class);
	protected final com.ibm.broker.config.proxy.FlowProxy iibFlowProxy;

	public FlowProxy(com.ibm.broker.config.proxy.FlowProxy iibFlowProxy) {
		this.iibFlowProxy = iibFlowProxy;
		try {
			if (iibFlowProxy != null) {  // Will be null if this was created for a subflow's Node or NodeConnections
				super.setAdminObject(iibFlowProxy.getParent());
			}
		} catch (ConfigManagerProxyLoggedException e) {
			// Avoid the constructor having to throw an exception
			String name = this.getClass().getSimpleName();
			logger.error("Exception getting the IIB "+name+"'s parent. In constructor and avoiding it throwing an exception. This will cause a leter error.", e);			
		}
	}	

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibFlowProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
}
