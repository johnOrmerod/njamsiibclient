package com.w3p.api.config.proxy;

public class IntegrationNodeConnectionParameters implements BrokerConnectionParameters {
	
	private final com.ibm.broker.config.proxy.IntegrationNodeConnectionParameters iibIntegrationNodeConnectionParameters;
	private final String password;
	private final boolean useSSL;
	

	public IntegrationNodeConnectionParameters(String ip, int port, String userName, String password, boolean useSSL) {
		iibIntegrationNodeConnectionParameters = new com.ibm.broker.config.proxy.IntegrationNodeConnectionParameters(ip, port, userName, password, useSSL);
		this.password = password;
		this.useSSL = useSSL;
	}

	@Override
	public String getIntegrationNodeHost() {
		return iibIntegrationNodeConnectionParameters.getHostname();
	}

	@Override
	public int getIntegrationNodePort() {
		return iibIntegrationNodeConnectionParameters.getPort();
	}

	@Override
	public String getUsername() {
		return iibIntegrationNodeConnectionParameters.getUserID();
	}

	@Override
	public String getPassword() {
		return password; //iibIntegrationNodeConnectionParameters.get;	
	}

	@Override
	public boolean isUseSSL() {
		return useSSL; //iibIntegrationNodeConnectionParameters.ss;
	}
}
