package com.w3p.api.config.proxy;

import java.awt.Point;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


public class MessageFlowProxy extends FlowProxy {
	
	private com.ibm.broker.config.proxy.FlowProxy iibMessageFlowProxy; // MessageFlowProxy iibMessageFlowProxy;
	
	private List<MessageFlowProxy.Node> msgflowNodes = new LinkedList<>();
	private List<MessageFlowProxy.NodeConnection> msgflowConnections = new LinkedList<>();
	
	
	public MessageFlowProxy(com.ibm.broker.config.proxy.FlowProxy iibMessageFlowProxy) {  //MessageFlowProxy iibMessageFlowProxy) {
		super(iibMessageFlowProxy);
		this.iibMessageFlowProxy = iibMessageFlowProxy; //? should we cast the flowProxy?
	}

	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			return ((com.ibm.broker.config.proxy.MessageFlowProxy)iibFlowProxy).isRunning();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
	
	public void setRuntimeProperty(String propertyName, String propertyValue) throws ConfigManagerProxyLoggedException, IllegalArgumentException {
		try {
			iibMessageFlowProxy.setRuntimeProperty(propertyName, propertyValue);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException e) {
			throw new ConfigManagerProxyLoggedException(e);
		}
	}	
	public String getRuntimeProperty(String name) throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibMessageFlowProxy.getRuntimeProperty(name);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}	
	
	public Enumeration<Node> getNodes() throws ConfigManagerProxyPropertyNotInitializedException {
		if (msgflowNodes.isEmpty()) {
			populateNodes();
		}
		return Collections.enumeration(msgflowNodes);
	}	
	private void populateNodes() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			List<com.ibm.broker.config.proxy.MessageFlowProxy.Node> iibMsgflowNodes = Collections.list(((com.ibm.broker.config.proxy.MessageFlowProxy) iibMessageFlowProxy).getNodes());
			iibMsgflowNodes.forEach(imfn -> {
				Node node = new Node(imfn);
				if (node.getType().startsWith("ComIbmMQ")) {
					node.mappingValues = String.format("{\"Queue name\": \"%s\"}", node.getProperties().get("queueName"));
				} else {
					node.mappingValues = "{}";		
				}
				msgflowNodes.add(node);
			});			
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<NodeConnection> getNodeConnections() throws ConfigManagerProxyPropertyNotInitializedException {
		if (msgflowConnections.isEmpty()) {
			populateConnections();
		}
		return Collections.enumeration(msgflowConnections);
	}	
	private void populateConnections() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			List<com.ibm.broker.config.proxy.MessageFlowProxy.NodeConnection> iibMsgflowConnections = Collections.list(((com.ibm.broker.config.proxy.MessageFlowProxy) iibMessageFlowProxy).getNodeConnections());
			iibMsgflowConnections.forEach(imfc -> {
				NodeConnection conn = new NodeConnection(imfc);
				msgflowConnections.add(conn);
			});			
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}

	/*
	 * Inner Classes: Node and NodeConnection
	 */
	public class Node {
		private final com.ibm.broker.config.proxy.MessageFlowProxy.Node iibNode;
		private String mappingValues;
		public Node(com.ibm.broker.config.proxy.MessageFlowProxy.Node iibNode) {
			this.iibNode = iibNode;
		}		
		public String getName() {
			return iibNode.getName();    //model.getName();
		}
		public String getType() {
			return iibNode.getType();   //model.getType();
		}
		public Point getLocation() { // Only available for ACE
			return new Point(0, 0);  //model.getLocation();
		}
		public Properties getProperties() {
			return iibNode.getProperties();
		}
		public String getMappingValues() {
			return mappingValues;
		}
		public String getUUID() {
			return iibNode.getUUID();
		}
		@Override
		public String toString() {
			return "Node [name=" + getName() + "]";
		}
	}

	public class NodeConnection {
		private com.ibm.broker.config.proxy.MessageFlowProxy.NodeConnection iibNodeConnection;		
		public NodeConnection(com.ibm.broker.config.proxy.MessageFlowProxy.NodeConnection iibNodeConnection) {
			this.iibNodeConnection = iibNodeConnection;
		}		
		public FlowNodeModel getSourceNode() {
			com.ibm.broker.config.proxy.MessageFlowProxy.Node iibSourceNode = iibNodeConnection.getSourceNode();
			FlowNodeModel fnm = new FlowNodeModel(iibSourceNode);
			return fnm;
		}
		public String getSourceOutputTerminal() {
			return iibNodeConnection.getSourceOutputTerminal();  //model.getSourceOuputTerminal();
		}
		public FlowNodeModel getTargetNode() {
			//return model.getTargetNode();
			com.ibm.broker.config.proxy.MessageFlowProxy.Node iibTargetNode = iibNodeConnection.getTargetNode();
			FlowNodeModel fnm = new FlowNodeModel(iibTargetNode);
			return fnm;
		}
		public String getTargetInputTerminal() {
			return iibNodeConnection.getTargetInputTerminal(); //model.getTargetInputTerminal();
		}
		@Override
		public String toString() {
			return "NodeConnection [getSourceNode()=" + getSourceNode() + ", getTargetNode()=" + getTargetNode() + "]";
		}	
	}
}
