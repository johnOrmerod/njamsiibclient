package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;

public class RestApiProxy extends DeployableProxy { //AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(RestApiProxy.class);
	private final com.ibm.broker.config.proxy.RestApiProxy iibRestApiProxy;

	public RestApiProxy(com.ibm.broker.config.proxy.RestApiProxy iibRestApiProxy) {
		this.iibRestApiProxy = iibRestApiProxy;
		try {
			super.setAdminObject(iibRestApiProxy.getParent());
		} catch (ConfigManagerProxyLoggedException e) {
			// Avoid the constructor having to throw an exception
			String name = this.getClass().getSimpleName();
			logger.error("Exception getting the IIB "+name+"'s parent. In constructor and avoiding it throwing an exception. This will cause a leter error.", e);			
		}
	}

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibRestApiProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}	

	public Enumeration<MessageFlowProxy> getMessageFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<MessageFlowProxy> mfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.MessageFlowProxy> iibMFs = Collections.list(iibRestApiProxy.getMessageFlows(props));
			iibMFs.forEach(iibmf -> {
				MessageFlowProxy mfp = new MessageFlowProxy(iibmf);
				mfProxies.add(mfp);
			});	
			return Collections.enumeration(mfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<SubFlowProxy> getSubFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<SubFlowProxy> sfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.SubFlowProxy> iibSFs = Collections.list(iibRestApiProxy.getSubFlows(props));
			iibSFs.forEach(iibsf -> {
				SubFlowProxy sfp = new SubFlowProxy(iibsf);
				sfProxies.add(sfp);
			});	
			return Collections.enumeration(sfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<StaticLibraryProxy> getStaticLibraries(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<StaticLibraryProxy> slProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.StaticLibraryProxy> iibSLs = Collections.list(iibRestApiProxy.getStaticLibraries(props));
			iibSLs.forEach(iibsl -> {
				StaticLibraryProxy slp = new StaticLibraryProxy(iibsl);
				slProxies.add(slp);
			});	
			return Collections.enumeration(slProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public boolean isRunning() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibRestApiProxy.isRunning();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
}
