package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;

public class SharedLibraryProxy extends AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(SharedLibraryProxy.class);
	
	private com.ibm.broker.config.proxy.SharedLibraryProxy iibSharedLibraryProxy;

	public SharedLibraryProxy(com.ibm.broker.config.proxy.SharedLibraryProxy iibSharedLibraryProxy) {
		this.iibSharedLibraryProxy = iibSharedLibraryProxy;
		try {
			super.setAdminObject(iibSharedLibraryProxy.getParent());
		} catch (ConfigManagerProxyLoggedException e) {
			// Avoid the constructor having to throw an exception
			String name = this.getClass().getSimpleName();
			logger.error("Exception getting the IIB "+name+"'s parent. In constructor and avoiding it throwing an exception. This will cause a leter error.", e);
		}
	}

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibSharedLibraryProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}	

	@Deprecated
	public Enumeration<MessageFlowProxy> getMessageFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<MessageFlowProxy> mfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.MessageFlowProxy> iibMFs = Collections.list(iibSharedLibraryProxy.getMessageFlows(props));
			iibMFs.forEach(iibmf -> {
				MessageFlowProxy mfp = new MessageFlowProxy(iibmf);
				mfProxies.add(mfp);
			});	
			return Collections.enumeration(mfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<SubFlowProxy> getSubFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<SubFlowProxy> sfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.SubFlowProxy> iibSFs = Collections.list(iibSharedLibraryProxy.getSubFlows(props));
			iibSFs.forEach(iibsf -> {
				SubFlowProxy sfp = new SubFlowProxy(iibsf);
				sfProxies.add(sfp);
			});	
			return Collections.enumeration(sfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
}
