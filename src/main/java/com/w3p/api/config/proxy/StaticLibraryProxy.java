package com.w3p.api.config.proxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.broker.config.proxy.ConfigManagerProxyLoggedException;


public class StaticLibraryProxy extends AdministeredObject {
	private static final Logger logger = LoggerFactory.getLogger(StaticLibraryProxy.class);	
	private com.ibm.broker.config.proxy.StaticLibraryProxy iibStaticLibraryProxy;

	public StaticLibraryProxy(com.ibm.broker.config.proxy.StaticLibraryProxy iibStaticLibraryProxy) {
		this.iibStaticLibraryProxy = iibStaticLibraryProxy;
		try {
			super.setAdminObject(iibStaticLibraryProxy.getParent());
		} catch (ConfigManagerProxyLoggedException e) {
			// Avoid the constructor having to throw an exception
			String name = this.getClass().getSimpleName();
			logger.error("Exception getting the IIB "+name+"'s parent. In constructor and avoiding it throwing an exception. This will cause a leter error.", e);			
		}
	}

	@Override
	public String getName() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			return iibStaticLibraryProxy.getName();
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}
	}
	
	public String getUri() throws ConfigManagerProxyPropertyNotInitializedException {
		// This manufactures a uri for compatibility with ACE, which can rerturn a uri.
		try {
			String subflowProp = iibStaticLibraryProxy.getProperty("subcomponent.1"); // Subflow+/TestStaticLibApp/TestStaticLibA/SubflowTestStaticLibA
			String subflowPath = StringUtils.substringAfter(subflowProp, "Subflow+"); // /TestStaticLibApp/TestStaticLibA/SubflowTestStaticLibA
			// Create the subflowUri format: "/apiv1/servers/server-name/runnable/TestStaticLibApp/libraries/TestStaticLib/subflows/SubflowTestStaticLib
			String subflow = StringUtils.substringAfterLast(subflowPath, "/"); 
			String preSubflow = StringUtils.substringBefore(subflowPath, "/"+subflow); //  /TestStaticLibApp/TestStaticLibA
			String staticLib = StringUtils.substringAfterLast(preSubflow, "/");
			String runnable = StringUtils.substringBeforeLast(preSubflow, "/"); // /TestStaticLibApp
			String uri = "/apiv1/servers/server-name/runnable".concat(runnable).concat("/libraries/").concat(staticLib).concat("/subflows/").concat(subflow);

			return uri;
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e); 
		}
	}
	
	public Enumeration<MessageFlowProxy> getMessageFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<MessageFlowProxy> mfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.MessageFlowProxy> iibMFs = Collections.list(iibStaticLibraryProxy.getMessageFlows(props));
			iibMFs.forEach(iibmf -> {
				MessageFlowProxy mfp = new MessageFlowProxy(iibmf);
				mfProxies.add(mfp);
			});	
			return Collections.enumeration(mfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<SubFlowProxy> getSubFlows(Properties props) throws ConfigManagerProxyPropertyNotInitializedException {		
		try {
			List<SubFlowProxy> sfProxies = new ArrayList<>();
			List<com.ibm.broker.config.proxy.SubFlowProxy> iibSFs = Collections.list(iibStaticLibraryProxy.getSubFlows(props));
			iibSFs.forEach(iibsf -> {
				SubFlowProxy sfp = new SubFlowProxy(iibsf);
				sfProxies.add(sfp);
			});	
			return Collections.enumeration(sfProxies);
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
}
