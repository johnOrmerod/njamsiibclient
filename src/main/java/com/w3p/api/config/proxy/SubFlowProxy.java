package com.w3p.api.config.proxy;

import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import com.w3p.api.config.proxy.MessageFlowProxy.Node;
import com.w3p.api.config.proxy.MessageFlowProxy.NodeConnection;

public class SubFlowProxy extends FlowProxy {
	
	private com.ibm.broker.config.proxy.SubFlowProxy iibSubFlowProxy;
	
	private List<MessageFlowProxy.Node> subflowNodes = new LinkedList<>();
	private List<MessageFlowProxy.NodeConnection> subflowConnections = new LinkedList<>();
	
	public SubFlowProxy(com.ibm.broker.config.proxy.SubFlowProxy iibSubFlowProxy) {
		super(iibSubFlowProxy);
		this.iibSubFlowProxy = iibSubFlowProxy;
	}
	
	public Enumeration<Node> getNodes() throws ConfigManagerProxyPropertyNotInitializedException {
		if (subflowNodes.isEmpty()) {
			populateNodes();
		}
		return Collections.enumeration(subflowNodes);
	}	
	private void populateNodes() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			List<com.ibm.broker.config.proxy.MessageFlowProxy.Node> iibSubflowNodes = Collections.list(iibSubFlowProxy.getNodes());
			iibSubflowNodes.forEach(isfn -> {
				Node node =  new MessageFlowProxy(null).new Node(isfn);
				subflowNodes.add(node);
			});			
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}
	
	public Enumeration<NodeConnection> getNodeConnections() throws ConfigManagerProxyPropertyNotInitializedException {
		if (subflowConnections.isEmpty()) {
			populateConnections();
		}
		return Collections.enumeration(subflowConnections);
	}	
	private void populateConnections() throws ConfigManagerProxyPropertyNotInitializedException {
		try {
			List<com.ibm.broker.config.proxy.MessageFlowProxy.NodeConnection> iibSubflowConnections = Collections.list(iibSubFlowProxy.getNodeConnections());
			iibSubflowConnections.forEach(isfc -> {				
				NodeConnection conn = new MessageFlowProxy(null).new NodeConnection(isfc);
				subflowConnections.add(conn);
			});			
		} catch (com.ibm.broker.config.proxy.ConfigManagerProxyPropertyNotInitializedException e) {
			throw new ConfigManagerProxyPropertyNotInitializedException(e);
		}		
	}

}
