package com.w3p.im.iib.mon.client;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IIBMonitoringClient {
	private static final Logger logger = LoggerFactory.getLogger(IIBMonitoringClient.class);
	
	private static final String CLIENT_RUN_MODE = "clientRunMode"; // PROD or DEV
	private static final String PROD_FAST_START = "prodFastStart"; // Skip applying the Monitoring Profiles, if sure they haven't changed
	private static final String CONFIG_FOLDER = "configFolder";
	private static final String CONFIG_NAME = "configName";
	private static final String CONFIG_SCHEMA = "configSchema";
	private static final String CLIENT_SLEEP_INTERVAL = "clientSleepInterval";
	
	private static final String RUN_MODE = System.getProperty(CLIENT_RUN_MODE);
	private static final boolean PROD_QUICK_START = System.getProperty(PROD_FAST_START) == null ? false : 
		System.getProperty(PROD_FAST_START).equals("true") ? true : false;
	private static final String NJAMS_CONFIG_FOLDER = System.getProperty(CONFIG_FOLDER);
	private static final String NJAMS_CONFIG_FILE = System.getProperty(CONFIG_NAME);
	private static final String NJAMS_CONFIG_SCHEMA = System.getProperty(CONFIG_SCHEMA);
	private static final long NJAMS_CLIENT_SLEEP_INTERVAL = Long.parseLong(System.getProperty(CLIENT_SLEEP_INTERVAL));


	public static void main(String[] args) {
		logger.info("IIBMonitoringClient#main() starting");
		
		Path configFilePath = Paths.get(NJAMS_CONFIG_FOLDER, NJAMS_CONFIG_FILE);
		Path configSchemaPath = Paths.get(NJAMS_CONFIG_FOLDER, NJAMS_CONFIG_SCHEMA);

		// Start the Client
		try {
			MonitoringClient client = new MonitoringClient(RUN_MODE, configFilePath, configSchemaPath, PROD_QUICK_START);
			client.runClient();			
			while (client.isRunning()) {
				try {
					Thread.sleep(NJAMS_CLIENT_SLEEP_INTERVAL);
				} catch (InterruptedException e) {
					logger.error(e.toString(), e);
					logger.info("IIBMonitoringClient has been stopped");
					System.exit(98);
				}
			}
		}catch (Exception e) {
			logger.error("Error establishing  the Client. {}.", e.toString(), e);
			System.out.println(String.format("Error establishing  the Client. %s.", e.toString()));
			System.exit(99);
		}
		logger.info("IIBMonitoringClient has stopped");
		System.exit(0);
	}
}
