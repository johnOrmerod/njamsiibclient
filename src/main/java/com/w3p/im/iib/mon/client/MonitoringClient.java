package com.w3p.im.iib.mon.client;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.CLIENT_VERSION;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.CLOUD_SERVER;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.FWD_SLASH;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.IBM_SERVER_IMAGE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.IMAGES;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.MON_PROF;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.PROD;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.USER_DIR;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.im.njams.sdk.Njams;
import com.im.njams.sdk.communication.CommunicationFactory;
import com.im.njams.sdk.communication.cloud.CloudConstants;
import com.im.njams.sdk.communication.jms.JmsConstants;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.image.FileImageSupplier;
import com.im.njams.sdk.settings.Settings;
import com.w3p.api.config.proxy.ApplicationProxy;
import com.w3p.api.config.proxy.BrokerConnectionParameters;
import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.DeployableProxy;
import com.w3p.api.config.proxy.ExecutionGroupProxy;
import com.w3p.api.config.proxy.IntegrationNodeConnectionParameters;
import com.w3p.api.config.proxy.MessageFlowProxy;
import com.w3p.im.iib.mon.client.config.data.ClientConfig;
import com.w3p.im.iib.mon.client.config.data.CloudServer;
import com.w3p.im.iib.mon.client.config.data.Connections;
import com.w3p.im.iib.mon.client.config.data.IIBConfig;
import com.w3p.im.iib.mon.client.config.data.IntegrationServer;
import com.w3p.im.iib.mon.client.config.data.JmsEvents;
import com.w3p.im.iib.mon.client.config.data.JmsServer;
import com.w3p.im.iib.mon.client.config.data.MqttConfig;
import com.w3p.im.iib.mon.client.config.data.NjamsServer;
import com.w3p.im.iib.mon.client.config.groovy.ParseClientConfig;
import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.ReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;
import com.w3p.im.iib.mon.client.utils.io.readers.FileContent;
import com.w3p.im.iib.mon.client.utils.io.readers.FileReader;
import com.w3p.im.iib.mon.client.utils.serialize.FlowToProcessModelCacheDeserializer;
import com.w3p.im.iib.mon.client.utils.serialize.FlowToProcessModelCacheSerializer;
import com.w3p.im.iib.mon.client.utils.serialize.ProcessModelDeserializer;
import com.w3p.im.iib.mon.client.utils.serialize.ProcessModelSerializer;
import com.w3p.im.iib.mon.event.processors.FlowToProcessModelCacheRun;
import com.w3p.im.iib.mon.exceptions.internal.ClientConfigException;
import com.w3p.im.iib.mon.exceptions.internal.ClientProblemException;
import com.w3p.im.iib.mon.exceptions.internal.CreateMonitoringEventHandlerException;
import com.w3p.im.iib.mon.exceptions.internal.IIBConnectionException;
import com.w3p.im.iib.mon.exceptions.internal.IIBTopologyCreationException;
import com.w3p.im.iib.mon.exceptions.internal.NJAMSConnectionException;
import com.w3p.im.iib.mon.exceptions.internal.NJAMSTopologyCreationException;
import com.w3p.im.iib.mon.exceptions.internal.ParseClientConfigException;
import com.w3p.im.iib.mon.jms.producers.EventHandlersCreator;
import com.w3p.im.iib.mon.jms.producers.EventHandlersCreatorForTopicSubscription;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.jms.producers.MonitoringScopeCreator;
import com.w3p.im.iib.mon.jms.producers.TopologyProducerForNjams;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;
import com.w3p.im.iib.mon.monitor.data.ApplyProfileRequest;
import com.w3p.im.iib.mon.monitor.data.EventHandlingForTopicRequest;
import com.w3p.im.iib.mon.monitor.data.EventHandlingRequest;
import com.w3p.im.iib.mon.monitor.data.IntNodeConnectionObject;
import com.w3p.im.iib.mon.monitor.data.ProcessMonitoringProfileResponse;
import com.w3p.im.iib.mon.monitor.profile.GenerateMonitoringProfiles;
import com.w3p.im.iib.mon.monitor.profile.ProcessMonitoringProfile;
import com.w3p.im.iib.mon.mqtt.service.MqttConnectionProperties;
import com.w3p.im.iib.mon.serializer.Base64Serializer;
import com.w3p.im.iib.mon.topology.TopologyCreator;
import com.w3p.im.iib.mon.topology.data.GenerateMonitoringProfileRequest;
import com.w3p.im.iib.mon.topology.data.GenerateMonitoringProfileResponse;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeRequest;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeResponse;
import com.w3p.im.iib.mon.topology.data.TopologyForNjamsRequest;
import com.w3p.im.iib.mon.topology.data.TopologyForNjamsResponse;
import com.w3p.im.iib.mon.topology.data.TopologyRequest;
import com.w3p.im.iib.mon.topology.data.TopologyResponse;
import com.w3p.im.iib.mon.topology.model.MessageFlow;
import com.w3p.im.iib.mon.topology.model.TopologyObject;


/**
 * @author John Ormerod
 *
 */
public class MonitoringClient{
	
	private static final String LOG4J_CONFIG_FILE = "log4j.configurationFile";

	private static final Logger logger = LoggerFactory.getLogger(MonitoringClient.class);
	
	private static final String IS_CONNECTING = "Connecting";
	private static final String IS_RUNNING = "Running"; 
	private static final String NOT_RUNNING = "Not Running";
	private final Map<String, String> njamsStatus = new HashMap<>(); // k=intSvrName, v= 'Connected' | 'Disconnected'
	
	public static Map<String, Njams> njamsClients = new HashMap<>();  // FIXME Temp workaround 	
	private boolean running;	
	
	private final String runMode;
	private final boolean prodFastStart; // Skip applying the Monitoring Profiles, if sure they haven't changed
	private final Path configFilePath;
	private final Path configSchemaPath;
	private ClientConfig clientConfig;




	public MonitoringClient(String runMode, Path configFilePath, Path configSchemaPath, boolean prodFastStart) {
		this.runMode = runMode;
		this.prodFastStart = prodFastStart;
		this.configFilePath = configFilePath;
		this.configSchemaPath = configSchemaPath;
	}

	public boolean runClient() throws ClientConfigException, IIBConnectionException, ClientProblemException {   
		logger.trace("#runClient() enter");
		running = true;
		String clientVersion = System.getProperty(CLIENT_VERSION);
		logger.info("nJAMS Client version is: {}", clientVersion );
		logger.info("Log4j2 configutation file is: '{}'.", System.getProperty(LOG4J_CONFIG_FILE));

		// Validate the client config xml file and create ClientConfig 
		clientConfig = createClientConfig(runMode, configFilePath, configSchemaPath); // throws ClientConfigException
		
		int intSvrCount = clientConfig.getMonitoring().getIntegrationServers().size(); // Number of Int Svrs being monitored
		
		/*
		 *  Orchestrate the steps for creating the nJAMS Client for the specified Int Server
		 *  
		 *  - connect to the nJAMS server via the Client SDK
		 *  
		 *  PROD MODE
		 *  - read Monitoring Profiles from folder and apply toIIB
		 *  - import the serialized Process Models
		 *  - import the serialized FlowToProcessModelCache and populate it
		 *  - start the client => send the proc models to the server (read from serialized PorcessModels)
		 *  - create the handler to process IIB monitoring events read from a queue
		 *  
		 *  DEV MODE
		 *  - create the Monitoring Scope from the njams.properties file
		 *  - create the IIB topology as constrained by the monitoring scope
		 *  - write Monitoring Profiles to folder 
		 *  - create the njams topology, add to njamsClient
		 *  - export serialized Proc Models and FlowToProcessModelCache to their folder
		 *  - import the serialized Process Models [this is to verify the serialized ProcModels in DEV mode]
		 *  - import the serialized FlowToProcessModelCache and populate it [this is to verify the serialized ProcModels in DEV mode]
		 *  - start the client => send the proc models to the server (read from serialized PorcessModels)  
		 *  - create the handlers to process IIB monitoring events published to a dynamic topic per message flow
		 *  - read Monitoring Profile FOR EACH MSGFLOW from folder and apply [- BUT ONLY ONCE = MAYBE NOT]
		 */

		// Connect to IIB - local or remote // FIXME allow retry if IIB is not running
		IntNodeConnectionObject intNodeConnection = connectToIntegrationNode(clientConfig); // throws IIBConnectionException
//		String intNodeName = intNodeConnection.getIntNodeName();
//		Njams njamsClient = null;
/*		
		if (runMode.equals(PROD)) {
			// Use a single Client when reading mon events from a queue - has to handle all IntSvrs that write to that queue
			String eventsSource = clientConfig.getConnections().getIibEventsSource(); // Format: queue://<qmgr>/<queue>
			String queueName = StringUtils.substringAfterLast(eventsSource, FWD_SLASH);
			String intNodeName = intNodeConnection.getIntNodeName();
			String njamsClientId = intNodeName.concat(UNDERSCORE).concat(queueName); // nJAMS client id must be unique
			try {
				njamsStatus.put(njamsClientId, IS_CONNECTING);
				njamsClient = connectToNjamsServerForQueue(clientConfig, intNodeConnection, clientVersion); // throws NJAMSConnectionException
				njamsClient.addSerializer(String.class, new Base64Serializer<String>()); // For decoding non-XML payloads when tracing is enabled
				// Add Client to the Map of Clients
				njamsClients.put(njamsClientId, njamsClient);
				njamsStatus.put(njamsClientId, IS_RUNNING);
			} catch (NJAMSConnectionException e ) { 				
//				handleNJAMSConnectionException(intSvrCount, intSvr, e);
				e.printStackTrace(); // FIXME need alternative exception
			}
		}
*/		
		FlowToProcessModelCache prodModeFlowToProcessModelCache = null;
		EventHandlingRequest eventHandlingRequest = null;
		String intNodeName = intNodeConnection.getIntNodeName();
		
		for (IntegrationServer intSvr : clientConfig.getMonitoring().getIntegrationServers()) {
			Njams njamsClient = null;			
			String intSvrName = intSvr.getName();			
			try {
				njamsStatus.put(intSvrName, IS_CONNECTING);
				njamsClient = connectToNjamsServer(intSvrName, clientConfig, intNodeConnection, clientVersion); // throws NJAMSConnectionException				
				njamsClient.addSerializer(String.class, new Base64Serializer<String>()); // For decoding Base64-encoded payloads when tracing is enabled
				
				// Add Client to the Map of Clients
				njamsClients.put(intSvrName, njamsClient);
				njamsStatus.put(intSvrName, IS_RUNNING);
			} catch (NJAMSConnectionException e ) { 				
				handleNJAMSConnectionException(intSvrCount, intSvr, e);
			}

			// FIXME refactor this straggly code
			if ( runMode != null && runMode.equals(PROD)) { // TODO verify that all args from the run client are present
				try {
					if (!prodFastStart) { // Note: to avoid long wait at startup caused by re-applying Mon Profiles for lots of message flows
						// Apply Monitoring Profiles
						BrokerProxy intNodeProxy = intNodeConnection.getIntNode();
						ExecutionGroupProxy intSvrProxy = intNodeProxy.getExecutionGroupByName(intSvrName);
						Enumeration<DeployableProxy> appProxies = intSvrProxy.getApplications(null); // FIXME get APIs and SERVICES
						while (appProxies.hasMoreElements()) {
							ApplicationProxy appProxy = (ApplicationProxy) appProxies.nextElement();
							String appName = appProxy.getName();
							String monProfileFolder = clientConfig.getMonitoring().getProfile().getDirectory().concat("\\").concat(intSvrName).concat("\\").concat(appName);	
							FileContent fc = getMonitoringProfilesForApp(intSvrName, appName, monProfileFolder);
							Enumeration<MessageFlowProxy> msgflowProxies = appProxy.getMessageFlows(null);

							while (msgflowProxies.hasMoreElements()) {
								MessageFlowProxy msgflowProxy = (MessageFlowProxy) msgflowProxies.nextElement();							
								String msgFlowProfileFileName = msgflowProxy.getName().concat(MON_PROF).concat(".xml");
								String profileAsXml = fc.getContent(msgFlowProfileFileName);
								if (profileAsXml != null && !profileAsXml.isEmpty()) {
									// Apply the MonProfile

									// Now, apply the monitoring profile, if it is new or has changed
									MessageFlow mf = new MessageFlow(msgflowProxy); // FIXME getWithSchemeName to be checked
									ApplyProfileRequest apr = new ApplyProfileRequest(intNodeProxy, profileAsXml, mf);
									apr.setTimeToWait(clientConfig.getMonitoring().getProfile().getTimeToWait());
									apr.setApplyProfileIfUnchanged(clientConfig.getMonitoring().getProfile().isApplyIfUnchanged());
									apr.setMonProfileDir(monProfileFolder);
									apr.setMessageFlowProxy(msgflowProxy);

									ProcessMonitoringProfile processMonitoringProfile = new ProcessMonitoringProfile(); 
									ProcessMonitoringProfileResponse processMonitoringProfileResponse = processMonitoringProfile.applyProfile(apr);	// FIXME do something with the response -like log it?	
									logger.info("\nApplied Monitoring Profile for Message flow '{}'. \nResults: {}", mf.getName(), processMonitoringProfileResponse.getAllMessages());

								} else {
									// log not found and  not used							
								}
							}						
						}
/*
 *  //FIXME Add RestAPIs and Services etc						
 */
						
					}  // End of !fast-start
// FIXME Include only those PMs that are in the scope of being monitored BELOW
/*					
					importProcessModels(njamsClient, intNodeName, intSvrName); // This will add the Proc Models form each Int Svr to the client	
					FlowToProcessModelCache ftpmc = importFlowToProcessModelCache(intNodeName, intSvrName);
					// Restore the Process Models removed to enable serialization
					FlowToProcessModelCacheRun flowToProcessModelCacheRun = createRuntimeFlowToProcessModelCache(njamsClient, ftpmc);
		
					if (eventHandlingRequest == null) {
						eventHandlingRequest = createEventHandlingForQueueRequest(intNodeConnection.getIntNodeName(), intSvrName, clientConfig, ftpmc);
					}
					eventHandlingRequest.addNjamsClient(intSvrName, njamsClient);
					eventHandlingRequest.addFlowToProcessModelCache(intSvrName, ftpmc);
					njamsClient.start();
*/					
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {  // DEV mode
/*
				try {
//					intSvrName = intSvr.getName();
					njamsStatus.put(intSvrName, IS_CONNECTING);
					njamsClient = connectToNjamsServer(intSvrName, clientConfig, intNodeConnection, clientVersion); // throws NJAMSConnectionException
					
					njamsClient.addSerializer(String.class, new Base64Serializer<String>()); // For decoding non-XML payloads when tracing is enabled
					
					// Add Client to the Map of Clients
					njamsClients.put(intSvrName, njamsClient);
					njamsStatus.put(intSvrName, IS_RUNNING);
				} catch (NJAMSConnectionException e ) { 				
					handleNJAMSConnectionException(intSvrCount, intSvr, e);
				}
*/				
				try {  // runMode.equals(DEV)
					MonitoringScopeResponse monScopeResponse = createMonitoringScope(intSvr, intNodeConnection);
					TopologyResponse topologyResponse = null;
					try {				
						if (monScopeResponse.isSuccess()) {
							TopologyRequest topologyRequest = createTopologyRequest(intNodeConnection, monScopeResponse, intSvr.getName(), clientConfig);
							topologyResponse = new TopologyCreator().createTopology(topologyRequest);
						}
					} catch (IIBTopologyCreationException e) {
						handleIIBTopologyCreationException(intSvrCount, intSvr, e);
					}
					
					// Generate and apply monitoring profiles for the msgflows being monitored
					GenerateMonitoringProfiles generateMonitoringProfiles = createGenerateMonitoringProfiles(intNodeConnection, intSvrName, topologyResponse);
					GenerateMonitoringProfileResponse gmpResp = generateMonitoringProfiles.generateProfile();
					
					TopologyForNjamsResponse njamsResponse = null;
					try {
						if (gmpResp.isSuccess()) {
							njamsResponse = createNjamsProcessModel(njamsClient, intSvr, topologyResponse);  // throws NJAMSConnectionException, NJAMSTopologyCreationException							
							// Write serialized objects to a file, for reading by this class (Process  Models)  and the Mesages Handler (FlowToProcessModel Cache)
 							exportProcessModels(njamsClient, intSvrName, topologyResponse.getTopology()); //intNodeName, e);
							exportFlowToProcessModelCache(njamsResponse.getFlowToProcessModelCache(), intNodeName); // intSvrName, topologyResponse.getTopology());
						} else {
							logger.info(topologyResponse.getMessage());
						}
					} catch (NJAMSTopologyCreationException e) {
						handleNJAMSTopologyCreationException(intSvrCount, intSvr, e);
					} catch (NJAMSConnectionException e ) { 				
						handleNJAMSConnectionException(intSvrCount, intSvr, e);
					} catch (Exception e) {
						throw new RuntimeException(String.format("Unable to establish the nJAMS Client. Reason: '%s'.", e.toString()), e);
					}

					if (njamsResponse.isSuccess()) {
						try {
							logger.info(njamsResponse.getMessage());
							// Import the just-serialized objects in order to validate them
							importProcessModels(njamsClient, intSvrName, topologyResponse.getTopology());
							FlowToProcessModelCache flowToProcessModelCache = importFlowToProcessModelCache(intNodeName); //njamsClient, intSvrName, topologyResponse.getTopology());							
							createRuntimeFlowToProcessModelCache(njamsClient, flowToProcessModelCache);  // Restore the Process Models removed to enable serialization
							njamsResponse.setFlowToProcessModelCache(flowToProcessModelCache);
							njamsClient.start();
							logger.info("Topolgy for '{}' was sent to nJAMS", intSvr.getName());
						} catch (Exception e) {
							e.printStackTrace();
						} 						
						// The Client is now established, except for creating the listener threads that subscribe to the IIB monitoring events.
						try {
							createEventsHandlerForTopics(njamsClient, intNodeConnection.getIntNodeName(), intSvr.getName(), clientConfig, topologyResponse, njamsResponse);
						} catch (CreateMonitoringEventHandlerException e) {
							// Error occurred on one or more monitoring threads, which prevented the creation of monitoring
							logger.error("Unable to establish monitoring for a topic. Cause: '{}'.", e.toString(), e);
						} catch (Exception e) {								
							// This is a catch-all block to prevent the client from crashing, should an unhandled exception find its way here. 
							logger.error("Unhandled exception received from the running Client. It has been logged rather than causing the Client to fail."
									+ " The error is: '{}'.", e.toString() , e);
						}
					} else {
						// FIXME handle njams model not succes
						logger.info(njamsResponse.getMessage());
					}
				} catch (Exception e) {
					handleException(intSvrCount, intSvr, e);
				}
			}

		}

		if (runMode !=  null && runMode.equals(PROD)) {				
			try {
				new EventHandlersCreator(eventHandlingRequest).createEventsHandlers();
			} catch (CreateMonitoringEventHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		logger.trace("#runClient() exit");
		return isRunning();
	}

	public GenerateMonitoringProfiles createGenerateMonitoringProfiles(IntNodeConnectionObject intNodeConnection, String intSvrName, TopologyResponse topologyResponse) {
		GenerateMonitoringProfileRequest gmpr = new GenerateMonitoringProfileRequest(intNodeConnection.getIntNode(), intSvrName, topologyResponse.getTopology());					
		gmpr.setTimeToWait(clientConfig.getMonitoring().getProfile().getTimeToWait())
			.setIntNodeName(clientConfig.getConnections().getIibConfig().getIntNode()) // Note: Comes from <IIB><IntegrationNode> when making a connection to a local Int Node
			.setMonProfileDir(clientConfig.getMonitoring().getProfile().getDirectory())
			.setApplyProfileIfUnchanged(clientConfig.getMonitoring().getProfile().isApplyIfUnchanged())
			.setIntegrationServer(clientConfig.getMonitoring().getIntegrationServer(intSvrName));					
		GenerateMonitoringProfiles generateMonitoringProfiles = new GenerateMonitoringProfiles(gmpr);
		return generateMonitoringProfiles;
	}
	
	private void exportProcessModels(Njams njamsClient, String intSvrName, TopologyObject topology) {
		String intNodeName = topology.getIntegrationNodeName();
		String modelDir =  "processModels/".concat(intNodeName).concat("/").concat(intSvrName);
		Path modelDirPath = Paths.get(modelDir);
		try {
			Files.createDirectories(modelDirPath); // Create on first use
			FileUtils.cleanDirectory(new File(modelDir));
			ProcessModelSerializer pms =  new ProcessModelSerializer(modelDir);
			pms.exportAllProcessModels(njamsClient);
		} catch (IOException e) {
			logger.warn("Unable to clean serialized process models directory: {}.", modelDir, e);
		} catch (IllegalArgumentException e) {
			logger.info("Folder '{}' does not exist - so will not be emptied", modelDir);
		}
	}


	private void importProcessModels(Njams njamsClient, String intSvrName, TopologyObject topology) {
		String intNodeName = topology.getIntegrationNodeName();
		String modelDir =  "processModels/".concat(intNodeName).concat("/").concat(intSvrName); //.concat("/").concat(app.getName());
		ProcessModelDeserializer pmds = new ProcessModelDeserializer();
		pmds.importAllProcessModels(njamsClient, modelDir);
	}

	private void exportFlowToProcessModelCache(FlowToProcessModelCache flowToProcessModelCache, String intNodeName) {
		String cacheDir = "flowToProcessModelCache/".concat(intNodeName);
		Path cacheDirPath = Paths.get(cacheDir);
		try {
			Files.createDirectories(cacheDirPath); // Create on first use
			FileUtils.cleanDirectory(new File(cacheDir));
		} catch (IOException e) {
			logger.warn("Unable to clean serialized flow-to-process-model -cache directory: {}.", cacheDir, e);
		} catch (IllegalArgumentException e) {
			logger.info("Folder '{}' does not exist - so will not be emptied", cacheDir);
		}
		FlowToProcessModelCacheSerializer ftpmcs = new FlowToProcessModelCacheSerializer();		
		ftpmcs.export(cacheDir, flowToProcessModelCache);

	}
	
	private FlowToProcessModelCache importFlowToProcessModelCache(String intNodeName) {

		String cacheDir = "flowToProcessModelCache/".concat(intNodeName);
		FlowToProcessModelCacheDeserializer ftpmcd = new FlowToProcessModelCacheDeserializer();
		FlowToProcessModelCache ftpmc = ftpmcd.importFlowToProcessModelCache(cacheDir);
		return ftpmc;
	}

	private FileContent getMonitoringProfilesForApp(String intSvrName, String appNane, String monProfileFolder) {
		// Read all files in the MonProfiles folder
		Response resp = new Response();
		IReadFileRequest rfReq = new ReadFileRequest();			 
		rfReq.setSource(monProfileFolder);
		rfReq.setNamePattern("*.xml");
		
		FileContent fc = null;
		try (FileReader fr = new FileReader()) {
			fr.init(rfReq, resp);
			fc = (FileContent)fr.readAll(rfReq, resp);
		}catch (Exception e) {
			// Create message
			resp.setMessage(Response.ERROR, e.toString());
			logger.error(e.toString(), e);
		}
		
		return fc;
	}
	
	private FlowToProcessModelCacheRun createRuntimeFlowToProcessModelCache(Njams njamsClient, FlowToProcessModelCache ftpmc) {
		// Copy the topology to the runtime cache
		FlowToProcessModelCacheRun flowToProcessModelCacheRun = new FlowToProcessModelCacheRun();
		Hashtable<String, List<String>> flowToPathAsList = ftpmc.getFlowToPath();
		flowToPathAsList.forEach((k,v) -> flowToProcessModelCacheRun.addPathForFlow(k, v));
		ftpmc.setFlowToProcessModelCacheRun(flowToProcessModelCacheRun); // This is for the MessageFlow change listener in DEV mode FIXME NOT USED NOW
		// Now complete the runtime cache
		com.im.njams.sdk.common.Path clientPath = njamsClient.getClientPath();
		Collection<ProcessModel> allPMs = njamsClient.getProcessModels();
		flowToProcessModelCacheRun.completeTheCache(allPMs, clientPath);
		flowToProcessModelCacheRun.setSubprocessModels(); // Sets Sub-procs in PMs for cloud server
		return flowToProcessModelCacheRun;
	}

	protected ClientConfig createClientConfig(String runMode, Path configFilePath, Path configSchemaPath) throws ClientConfigException {
		logger.trace("#createClientConfig() enter");
		validateClientConfig(configFilePath, configSchemaPath);
		logger.trace("#createClientConfig() exit");
		return readClientConfig(runMode, configFilePath);		
	}

	private void validateClientConfig(Path configFilePath, Path schemaFilePath) throws ClientConfigException {
		logger.trace("#validateClientConfig() enter");
		if (Files.exists(configFilePath) && Files.exists(schemaFilePath)) {
			try {
				// SchemaFactory for schema language W3C XML Schema 1.0
				SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
				// Parse xsd a provides a schema object
				Schema schema = schemaFactory.newSchema(schemaFilePath.toFile());		   
				// Processor to check XML is valid against schema
				Validator validator = schema.newValidator();		   
				// Validates the specified input
				validator.validate(new StreamSource(configFilePath.toFile()));
			}  catch (SAXException e) {
				int line = ((SAXParseException)e).getLineNumber();
				int col = ((SAXParseException)e).getColumnNumber();
				String expnMsg = e.getMessage();
				String message = "Error when validating XML ''{0}'' against XSD Schema ''{1}''\n  line: {2}\n  column: {3}\n  message: {4}";
				throw new ClientConfigException(message, 
						configFilePath.getFileName().toString(), schemaFilePath.getFileName().toString(),
						line, col, expnMsg.substring(expnMsg.indexOf(":") + 1),	e);
			} catch (IOException e) {
				throw new ClientConfigException(e.toString(), e);
			}  catch (Exception e) {
				throw new ClientConfigException(e.toString(), e);
			}
		} else {
			if (Files.exists(schemaFilePath)) {
				// The config file not found
				throw new ClientConfigException("Unable to find the Client's configuration file. \"'{0}'\", in directory \"'{1}'\". Client is closing.",
					configFilePath.getFileName().toString(), configFilePath.getParent().toString());
			} else {
				// The schema file not found
				throw new ClientConfigException("Unable to find the configuration file's schema. \"'{0}'\", in directory \"'{1}'\". Client is closing.",
						schemaFilePath.getFileName().toString(), schemaFilePath.getParent().toString());
			}
		}
		logger.trace("#validateClientConfig() exit");
	}

	private ClientConfig readClientConfig(String runMode, Path configFilePath) throws ClientConfigException {
		logger.trace("#readClientConfig() enter");
		//Read the file to a string  //FIXME use the file read utility
		try (InputStream is = Files.newInputStream(configFilePath, StandardOpenOption.READ)) {
			BOMInputStream bis = new BOMInputStream(is); // Use this to handle XML files with a leading BOM
			logger.debug("#readClientConfig() - input file has a BOM? = {}", bis.hasBOM());
			StringWriter sw = new StringWriter();
			IOUtils.copy(bis, sw, UTF_8);
			logger.trace("#createClientConfig() call 'ParseClientConfig'");
			ClientConfig clientConfig = new ParseClientConfig().parse(sw.toString());
			logger.trace("#createClientConfig() return from 'ParseClientConfig'");
			clientConfig.setClientMode(runMode);

			// FIXME Single source of client install directory - Not sure this is the best way to have a single source of client install dir
			clientConfig.setClientHome(System.getProperty(USER_DIR)); 
			clientConfig.getMonitoring().getProfile().setClientHome(clientConfig.getClientHome());
			clientConfig.getConnections().getIbmMqConfig().getJmsMQ().setClientHome(clientConfig.getClientHome());
			logger.trace("#readClientConfig() exit");
			return clientConfig;
		} catch (IOException e) {
			throw new ClientConfigException("Error reading the config xml and its xsd when starting the Client. Error is: '''{0}''.", e.toString(), e);
		} catch (ParseClientConfigException e) {
			throw new ClientConfigException("Error parsing the config xml file when starting Client. Error is: '''{0}''.", e.toString(), e);
		} catch (Exception e) {
			throw new ClientConfigException("Error starting Client. Error is: '''{0}''.", e.toString(), e);
		}
	}

	protected Njams connectToNjamsServerForQueue(ClientConfig clientConfig, IntNodeConnectionObject intNodeConnection, String clientVersion) throws NJAMSConnectionException {
		Njams njamsClient = null;
		// The nJAMS SDK throws various RuntimeExceptions, which we need to catch and handle.
		try {
			// Define the base Client path, which are displayed as the top 3 'Processes' on the server: Host / IntNode / IntSvr  FIXME Changes 
			com.im.njams.sdk.common.Path clientPath = createBaseClientPathForQueue(clientConfig, intNodeConnection);
			// Define the IIB version
			String iibTechnology = clientConfig.getType(); //IBM_SERVER_VERSION;
			// Create the server Settings to connect to the NJAMS on-Prem or Cloud server  
			Settings settings = createServerSetting(clientConfig);
			// Create the client for the Int Server (name is part of 'clientPath'
			njamsClient = new Njams(clientPath, clientVersion, iibTechnology, settings);			
			// Create the images for useby  the nJAMS server
			addBaseImagesForServer(iibTechnology, njamsClient);
		} catch (Exception e) {
			if (e.getClass().isAssignableFrom(NJAMSConnectionException.class)) {
				throw (NJAMSConnectionException)e;
			}
			throw new NJAMSConnectionException("Error creating the nJAMS JMS config vfor the server connection. Message is: ''{0}''.", e.toString(), e);
		}
		return njamsClient; 
	}
	
	protected Njams connectToNjamsServer(String intSvrName, ClientConfig clientConfig, IntNodeConnectionObject intNodeConnection, String clientVersion) throws NJAMSConnectionException {
		Njams njamsClient = null;
		// The nJAMS SDK throws various RuntimeExceptions, which we need to catch and handle.
		try {
			// Define the base Client path, which are displayed as the top 3 'Processes' on the server: IntNode / IntSvr / Host 
			com.im.njams.sdk.common.Path clientPath = createBaseClientPath(clientConfig, intNodeConnection, intSvrName);
			// Define the IIB version			
			String iibTechnology = clientConfig.getType(); //IBM_SERVER_VERSION;
			// Create the server Settings to connect to the NJAMS on-Prem or Cloud server  
			Settings settings = createServerSetting(clientConfig);
			// Create the client for the Int Server (name is part of 'clientPath'
			njamsClient = new Njams(clientPath, clientVersion, iibTechnology, settings);			
			// Create the images for use by  the nJAMS server
			addBaseImagesForServer(iibTechnology, njamsClient);
		} catch (Exception e) {
			if (e.getClass().isAssignableFrom(NJAMSConnectionException.class)) {
				throw (NJAMSConnectionException)e;
			}
			throw new NJAMSConnectionException("Error creating the nJAMS JMS config vfor the server connection. Message is: ''{0}''.", e.toString(), e);
		}
		return njamsClient; 
	}

	private void addBaseImagesForServer(String iibTechnology, Njams njamsClient) {
		FileImageSupplier fis = createServerJobImage(iibTechnology);
		njamsClient.addImage(fis);
		// Add IIB or ACE icons to the 'Process' tree
		njamsClient.addImage(createServerProcessTreeImage("njams.taxonomy.root"));
//		njamsClient.addImage(createServerProcessTreeImage("njams.taxonomy.folder"));
		// Add custom images for ActivityModels
		njamsClient.addImage("startType", "images/njams_java_sdk_process_start.png");
		njamsClient.addImage("endType", "images/njams_java_sdk_process_end.png");
	}

	private FileImageSupplier createServerProcessTreeImage(String processTreeLocation) {
		String ibmTechnologyImage = IBM_SERVER_IMAGE;		
		File image = new File(IMAGES, ibmTechnologyImage);
		FileImageSupplier fis = new FileImageSupplier(processTreeLocation, image);
		return fis;
	}
	
	private FileImageSupplier createServerJobImage(String iibTechnology) {
		String ibmTechnologyImage = IBM_SERVER_IMAGE;		
		File image = new File(IMAGES, ibmTechnologyImage);
		FileImageSupplier fis = new FileImageSupplier(iibTechnology, image);
		return fis;
	}

	private Settings createServerSetting(ClientConfig clientConfig) {
		Settings communicationProperties;
		boolean connectToCloudServer =  clientConfig.getConnections().getServerConfig().getServerType().equals(CLOUD_SERVER); 
		if (connectToCloudServer) {
			communicationProperties = createNjamsCloudServerConnection(clientConfig.getConnections().getServerConfig());
		} else {
			communicationProperties = createNjamsOnPremConnection(clientConfig.getConnections().getServerConfig());	
		}
		return communicationProperties;
	}

	private com.im.njams.sdk.common.Path createBaseClientPathForQueue(ClientConfig clientConfig, IntNodeConnectionObject intNodeConnection) {
		String intNodeName = intNodeConnection.getIntNodeName();
		String eventsSource = clientConfig.getConnections().getIibEventsSource(); // Format: queue://<qmgr>/<queue>
		String queueName = StringUtils.substringAfterLast(eventsSource, FWD_SLASH);
		String hostName = clientConfig.getConnections().getIibConfig().getHost();
		com.im.njams.sdk.common.Path clientPath = new com.im.njams.sdk.common.Path(hostName, intNodeName, queueName); //, hostName); // FIXME why queue and not IntSvr????
		return clientPath;
	}
	
	private com.im.njams.sdk.common.Path createBaseClientPath(ClientConfig clientConfig, IntNodeConnectionObject intNodeConnection, String intSvrName) {
		String intNodeName = intNodeConnection.getIntNodeName();
		String hostName = clientConfig.getConnections().getIibConfig().getHost();
		com.im.njams.sdk.common.Path clientPath = new com.im.njams.sdk.common.Path(hostName, intNodeName, intSvrName); // FIXME CHANGED
		return clientPath;
	}

	protected void handleException(int intSvrCount, IntegrationServer intSvr, Exception e) throws ClientProblemException {
		njamsStatus.put(intSvr.getName(), NOT_RUNNING);
		if (njamsStatus.containsValue(IS_RUNNING)) {
			// There must be another Int Svr being monitored - just log this error
			logger.error("Unhandled error when creating nJAMS Client for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", e.toString(), e);
		} else {
			if (njamsStatus.size() == intSvrCount) {
				// This is the only/last one being monitored - so the Client can't monitor anything. Terminate it.
				throw new ClientProblemException("Unhandled error when creating nJAMS Client for Int Svr ''{0}''. Nothing is being monitored - closing the client. The cause is: ''{1}''.",
						intSvr.getName(), e.toString(), e);
			} else {
				// Not all Int Svrs are being monitored yet - just log this error
				logger.error("Unhandled error when creating nJAMS Client for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", intSvr.getName(), e.toString(), e);
			}
		}
	}

	protected void handleNJAMSTopologyCreationException(int intSvrCount, IntegrationServer intSvr, NJAMSTopologyCreationException e) throws ClientProblemException {
		njamsStatus.put(intSvr.getName(), NOT_RUNNING);
		if (njamsStatus.containsValue(IS_RUNNING)) {
			// There must be another Int Svr being monitored - just log this error
			logger.error("Error creating the nJAMS Process Model for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", e.toString(), e);
		} else {
			if (njamsStatus.size() == intSvrCount) {
				// This is the only/last one being monitored - so the Client can't monitor anything. Terminate it.
				throw new ClientProblemException("Error creating the nJAMS Process Model for Int Svr ''{0}''. Nothing is being monitored - closing the client. The cause is: ''{1}''.",
						intSvr.getName(), e.toString(), e);
			} else {
				// Not all Int Svrs are being monitored yet - just log this error
				logger.error("Error creating the nJAMS Process Model for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", e.toString(), e);
			}
		}
	}

	protected void handleIIBTopologyCreationException(int intSvrCount, IntegrationServer intSvr, IIBTopologyCreationException e) throws ClientProblemException {
		njamsStatus.put(intSvr.getName(), NOT_RUNNING);
		if (njamsStatus.containsValue(IS_RUNNING)) {
			// There must be another Int Svr being monitored - just log this error
			logger.error("Error creating the IIB topology for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", e.toString(), e);
		} else {
			if (njamsStatus.size() == intSvrCount) {
				// This is the only/last one being monitored - so the Client can't monitor anything. Terminate it.
				throw new ClientProblemException("Error creating the IIB topology for Int Svr ''{0}''. Nothing is being monitored - closing the client. The cause is: ''{1}''.",
						intSvr.getName(), e.toString(), e);
			} else {
				// Not all Int Svrs are being monitored yet - just log this error
				logger.error("Error creating the IIB topology for Int Svr '{0}'. It will not be monitored. The cause is: {1}.", e.toString(), e);
			}	
		}
	}

	protected void handleNJAMSConnectionException(int intSvrCount, IntegrationServer intSvr, NJAMSConnectionException e) throws ClientProblemException {
		if (njamsStatus.get(intSvr.getName()).equals(IS_CONNECTING)) {
			njamsStatus.put(intSvr.getName(), NOT_RUNNING);						
			// Failed to connect to the nJAMS server
			if (njamsStatus.containsValue(IS_RUNNING)) {
				// There must be another Int Svr being monitored - just log this error
				logger.error("Client unable to connect to the nJAMS server for Int Server '{}'. The cause is: {}.", intSvr.getName(), e.toString(), e);							
			} else {							
				if (njamsStatus.size() == intSvrCount) {
					// No existing server connection - throw exception to stop the Client
					throw new ClientProblemException("Client unable to connect to the nJAMS server for Int Server ''{0}''. The cause is: ''{1}''.",
							intSvr.getName(), e.toString(), e);
				}
			}
		} else if (njamsStatus.get(intSvr.getName()).equals(IS_RUNNING)) {
			njamsStatus.put(intSvr.getName(), NOT_RUNNING);	
			// The connections to the njams server has been lost
			if (njamsStatus.containsValue(IS_RUNNING)) {
				// At least onde other Int Svr is running. This oner has lost its connection to nJAMs server
				logger.error("Client has lost its connection to the nJAMS server for Int Server '{}'. The cause is: '{}'.", intSvr.getName(), e.toString(), e);
			} else {
				if (njamsStatus.size() == intSvrCount) {
					// No existing server connection - throw exception to stop the Client
					throw new ClientProblemException("Client has lost all connections to the nJAMS server. The cause is: ''{0}''.",
							e.toString(), e);
				}
			}
		}
	}

	protected MonitoringScopeResponse createMonitoringScope(IntegrationServer intSvr, IntNodeConnectionObject intNodeConnection) {
		MonitoringScopeRequest monScopeRequest = createMonitoringScopeRequest(intNodeConnection, intSvr);
		MonitoringScopeResponse monScopeResponse = new MonitoringScopeCreator().createScope(monScopeRequest);
		return monScopeResponse;
	}

	protected Settings createNjamsOnPremConnection(NjamsServer njamsServer) {
		JmsServer server = njamsServer.getJmsConfig();

		Settings communicationProperties = new Settings();
		communicationProperties.put(CommunicationFactory.COMMUNICATION, "JMS"); // FIXME get from config
		communicationProperties.put(JmsConstants.INITIAL_CONTEXT_FACTORY, server.getInitialContextFactory()); // "org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		communicationProperties.put(JmsConstants.SECURITY_PRINCIPAL, server.getJndi().getUser().getUserName()); // "njams");
		communicationProperties.put(JmsConstants.SECURITY_CREDENTIALS, server.getJndi().getUser().getPasswordInClear()); // "njams");
		communicationProperties.put(JmsConstants.PROVIDER_URL, server.getProviderURL()); // "tcp://localhost:61616");
		communicationProperties.put(JmsConstants.CONNECTION_FACTORY, server.getConnectionFactory()); // "ConnectionFactory");
		communicationProperties.put(JmsConstants.USERNAME, server.getJmsUser().getUserName()); // "njams");
		communicationProperties.put(JmsConstants.PASSWORD, server.getJmsUser().getPasswordInClear()); // "njams");
		communicationProperties.put(JmsConstants.DESTINATION, server.getDestination()); //"njams");
		if (!server.getDestinationCommands().isEmpty()) {
			communicationProperties.put(JmsConstants.COMMANDS_DESTINATION, server.getDestinationCommands()); // Optional - alternative topic for njams cmds
		}	
		communicationProperties.put(Settings.PROPERTY_USE_DEPRECATED_PATH_FIELD_FOR_SUBPROCESSES, Boolean.toString(njamsServer.isSubflowDrillDown()));
	    return communicationProperties;   
	
	}
	
    protected Settings createNjamsCloudServerConnection(NjamsServer njamsServer) {
    	CloudServer cs = njamsServer.getCloudConfig();
    	
        Settings communicationProperties = new Settings();
        communicationProperties.put(CommunicationFactory.COMMUNICATION, CloudConstants.NAME);
        communicationProperties.put(CloudConstants.ENDPOINT, cs.getEndpoint());
        communicationProperties.put(CloudConstants.APIKEY, cs.getApiKey());
        communicationProperties.put(CloudConstants.CLIENT_INSTANCEID, cs.getClientInstanceId());
        communicationProperties.put(CloudConstants.CLIENT_CERTIFICATE, cs.getClientCertificate());
        communicationProperties.put(CloudConstants.CLIENT_PRIVATEKEY, cs.getClientPrivateKey());        
        communicationProperties.put(Settings.PROPERTY_USE_DEPRECATED_PATH_FIELD_FOR_SUBPROCESSES, Boolean.toString(njamsServer.isSubflowDrillDown()));
        
        return communicationProperties;
    }

/*
	protected TopologyResponse createTopology(topologyRequest); //IntegrationServer intSvr, MonitoringScopeResponse monScopeResponse,  IntNodeConnectionObject intNodeConnection) throws IIBTopologyCreationException {
//		TopologyRequest topologyRequest = createTopologyRequest(intNodeConnection, monScopeResponse, intSvr.getName(), clientConfig);
		TopologyResponse topologyResponse = new TopologyCreator().createTopology(topologyRequest);
		return topologyResponse;
	}
*/
    
	private void createEventsHandlerForQueues(Njams njamsClient, String intNodeName, String intSvrName, ClientConfig clientConfig, FlowToProcessModelCache ftpmc) throws CreateMonitoringEventHandlerException {
		EventHandlingRequest request = createEventHandlingForQueueRequest(intNodeName, intSvrName, clientConfig, ftpmc); // Removed njamsClident
		new EventHandlersCreator(request).createEventsHandlers(); 
	}

	protected void createEventsHandlerForTopics(Njams njamsClient, String intNodeName, String intSvrName, ClientConfig clientConfig, TopologyResponse topologyResponse, TopologyForNjamsResponse njamsResponse) throws CreateMonitoringEventHandlerException {
		EventHandlingForTopicRequest request = createEventHandlingForTopicRequest(njamsClient, intNodeName, intSvrName, clientConfig, topologyResponse, njamsResponse);
		new EventHandlersCreatorForTopicSubscription(request).createEventsHandlers();
	}
	
	protected TopologyForNjamsResponse createNjamsProcessModel(Njams njamsClient, IntegrationServer is, TopologyResponse topologyResponse) throws NJAMSTopologyCreationException, NJAMSConnectionException {
		TopologyForNjamsRequest njamsRequest = new TopologyForNjamsRequest(njamsClient, topologyResponse.getTopology(), is.getName());
		njamsRequest
			.setIncludeIntSvrName(is.isIncludeName())
			.setClientHome(clientConfig.getClientHome());					
		TopologyForNjamsResponse njamsResponse = new TopologyProducerForNjams(njamsRequest).createProcessModels(); // throws NJAMSConnectionException
//		if (njamsResponse.isSuccess()) {
//			njamsResponse.setMessage(String.format("Topolgy for '%s' was sent to nJAMS", is.getName()));
//			njamsRequest.getNjamsClient().start();
//		}
		return njamsResponse;
	}

	private IntNodeConnectionObject connectToIntegrationNode(ClientConfig clientConfig) throws IIBConnectionException {
		IIBConfig iibConfig = clientConfig.getConnections().getIibConfig();
		IntNodeConnectionObject brokerConnection;
		if (iibConfig.isBrokerLocal()) {
			brokerConnection = connectToLocalBroker(iibConfig);
		} else {
			brokerConnection = connectToRemoteBroker(iibConfig);
			// Int Node name is not provided in the config xml file for a remote 'broker'
			clientConfig.getConnections().getIibConfig().setIntNode(brokerConnection.getIntNodeName());
		}
		return brokerConnection;
	}

	private MonitoringScopeRequest createMonitoringScopeRequest(IntNodeConnectionObject brokerConnection, IntegrationServer is) {
		MonitoringScopeRequest request = new MonitoringScopeRequest(brokerConnection.getIntNode(), is);
		return request;
	}

	private TopologyRequest createTopologyRequest(IntNodeConnectionObject intNodeConnection, MonitoringScopeResponse monScopeResponse, String intSvrName, ClientConfig clientConfig) {
		TopologyRequest request = new TopologyRequest(intNodeConnection.getIntNode(), monScopeResponse.getMonitoringScope(), intSvrName);
		
		logger.info("Mon Profile Dir: '{}' ", clientConfig.getMonitoring().getProfile().getDirectory());		
		request.setTimeToWait(clientConfig.getMonitoring().getProfile().getTimeToWait())
			.setIntNodeName(clientConfig.getConnections().getIibConfig().getIntNode()) // Note: Comes from <IIB><IntegrationNode> when making a connection to a local Int Node
			.setMonProfileDir(clientConfig.getMonitoring().getProfile().getDirectory())
			.setApplyProfileIfUnchanged(clientConfig.getMonitoring().getProfile().isApplyIfUnchanged())
			.setIntegrationServer(clientConfig.getMonitoring().getIntegrationServer(intSvrName));
		return request;
	}

	/**
     * Connects to the local broker with the supplied name.
	 * @param iibConfig 
     * @param Int Node (Broker) name
	 * @throws IIBConnectionException 
     */
    private IntNodeConnectionObject connectToLocalBroker(IIBConfig iibConfig) throws IIBConnectionException {
    	logger.trace("#connectToLocalBroker() enter");
    	IntNodeConnectionObject bkrConn = new IntNodeConnectionObject();
    	bkrConn.setIntNodeName(iibConfig.getIntNode());
//		LocalBrokerUtilities.clearCache();
//		try {
//			bkrConn.setIntNode(BrokerProxy.getLocalInstance(bkrConn.getIntNodeName()));
//			bkrConn.setLocal(true);
//			logger.info("Connected to local Int Node: '{}'", bkrConn.getIntNodeName());			
//		} catch (Exception e) {
//    		throw new IIBConnectionException("Unable to connect to local broker, exception is: ''{0}''.", e.toString(), e);
//    	}
		logger.trace("#connectToLocalBroker() exit");
		return bkrConn;
    }
	
    /**
     * Connects to the broker
     * @param iibConfig 
     * @param ip Hostname or IP address (or empty String, to use MQ Java Bindings)
     * @param port Port on which the SVRCONN channel is listening (a valid port number,
     * or the empty string to use MQ Java Bindings)
     * @param qmgr Queue Manager on which the broker is
     * running (or empty String to use the default queue manager)
     * @throws IIBConnectionException 
     */
    private IntNodeConnectionObject connectToRemoteBroker(IIBConfig iibConfig) throws IIBConnectionException {
    	logger.trace("#connectToRemoteBroker() enter");
    	IntNodeConnectionObject bkrConn = new IntNodeConnectionObject();
    	bkrConn.setIpaddress(iibConfig.getHost());
    	bkrConn.setPort(iibConfig.getAdminPort());
    	String userName = iibConfig.getIibUser().getUserName();
    	String password  = iibConfig.getIibUser().getPasswordInClear();
    	boolean bUseSSL = false;
    	try {
    		BrokerConnectionParameters bcp = new IntegrationNodeConnectionParameters(bkrConn.getIpaddress(), bkrConn.getPort(), userName, password, bUseSSL); 
    		// The next statement is THE most important one in this method. When writing a
    		// Message Broker Administration application, this is the one you NEED to call. It starts up the connection to the broker.
    		bkrConn.setIntNode(BrokerProxy.getInstance(bcp));
    		bkrConn.setIntNodeName(bkrConn.getIntNode().getName());
    		bkrConn.setLocal(false);
    		logger.info("Connected to remote Int Node: '{}' [ipaddr:{} port:{} q mgr:{}", bkrConn.getIntNodeName(), bkrConn.getIpaddress(), bkrConn.getPort(), bkrConn.getQmgr());
    	} catch (Exception e) {
    		throw new IIBConnectionException("Unable to connect to remote broker at: host ''{0}'', port ''{1}'' exception is: {2}: ",
    				bkrConn.getIpaddress(), bkrConn.getPort(), e.toString(), e);
    	}
    	logger.trace("#connectToRemoteBroker() exit");
    	return bkrConn;
    }

    private EventHandlingRequest createEventHandlingForQueueRequest(String intNodeName, String intSvrName, ClientConfig clientConfig, FlowToProcessModelCache ftpmc) { //, TopologyResponse topologyResponseX, TopologyForNjamsResponse njamsResponseX) {;
    	Connections connections = clientConfig.getConnections();
    	// JMS Connection Properties *PROD & DEV*
    	MqJmsConnectionProperties mqJmsConnProps = createMqJmsConnProps(connections);    
    	EventHandlingRequest request = new EventHandlingRequest(intNodeName, intSvrName, mqJmsConnProps); // njamsClient, 
     	request.setEventsSource(connections.getIibEventsSource());
    	request.setIibEventsProblemsQueue(connections.getIibEventsProblemsQueue());
    	request.addFlowToProcessModelCache(intSvrName, ftpmc);
    	request.setConsumerThreads(clientConfig.getMonitoring().getConsumerThreads());
    	
    	return request;
    }

    private EventHandlingForTopicRequest createEventHandlingForTopicRequest(Njams njamsClient, String intNodeName, String intSvrName, ClientConfig clientConfig, TopologyResponse topologyResponse, TopologyForNjamsResponse njamsResponse) {
    	Connections connections = clientConfig.getConnections();

    	// MQTT Connection Properties  *DEV* // FIXME subclass for when MQTT is used
    	MqttConnectionProperties mqttConnProps = createMqttConnProps(connections);
    	
    	// JMS Connection Properties *PROD & DEV* // FIXME subclass for when JMS is used
    	MqJmsConnectionProperties mqJmsConnProps = createMqJmsConnProps(connections);    		
    	EventHandlingForTopicRequest request = new EventHandlingForTopicRequest(njamsClient, intNodeName, intSvrName, topologyResponse.getTopology(), mqJmsConnProps, mqttConnProps);
    	request.setEventsSource(clientConfig.getConnections().getIibEventsSource()); // FIXME the nexst three lines are repeated for Topic & Queue requests 
    	request.setFlowToProcessModelCache(njamsResponse.getFlowToProcessModelCache());
    	request.setConsumerThreads(clientConfig.getMonitoring().getConsumerThreads()); 

    	return request;
	}

	protected MqJmsConnectionProperties createMqJmsConnProps(Connections connections) {
		MqJmsConnectionProperties mqJmsConnProps = new MqJmsConnectionProperties();
		JmsEvents jmsMQ = connections.getIbmMqConfig().getJmsMQ();
		if (jmsMQ != null) {
			mqJmsConnProps
			.setMqJmsConnectionFactory(jmsMQ.getConnectionFactory())
			.setMqJmsInitialContextFactory(jmsMQ.getInitialContextFactory())
			.setMqJmsProviderUrl(jmsMQ.getProviderURL())
			.setMqJmsUser(jmsMQ.getJmsMqUser())
			.setJmsClientId(jmsMQ.getJmsClientId());
			logger.debug("JMS Provider URL = '{}'", jmsMQ.getProviderURL());
		}
		return mqJmsConnProps;
	}

	protected MqttConnectionProperties createMqttConnProps(Connections connections) {
		MqttConnectionProperties mqttConnProps = new MqttConnectionProperties();
		MqttConfig mqttConfig = connections.getMqttConfig();
		if (mqttConfig.getClientId() != null) {
			// If ClientId is present then the rest of the values must have been parsed from the xml
			mqttConnProps
				.setClientId(mqttConfig.getClientId())
				.setHost(mqttConfig.getHost())
				.setPort(mqttConfig.getPort())
				.setKeepAliveInterval(mqttConfig.getKeepAliveInterval())
				.setQos(mqttConfig.getQos())
				.setUser(mqttConfig.getMQTTUser()); 		
			logger.debug("MQTT Broker URL = '{}'", String.format("tcp://%1$s:%2$d", mqttConnProps.getHost(), mqttConnProps.getPort()));	
		}
		return mqttConnProps;
	}

	public ClientConfig getClientConfig() {
		return clientConfig;
	}

	public boolean isRunning() {
		return running;
	}
}
