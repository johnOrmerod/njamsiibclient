package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationDataQuery {
	private final List<Query> queries = new ArrayList<>();
	private final Map<String, Content> nodeTypeToQueryContent = new HashMap<>();
	

	public ApplicationDataQuery() {
	}

	public boolean isPopulated() {
		return !queries.isEmpty(); // != null; //nodeTypeToQueryText == null ? false : nodeTypeToQueryText.isPopulated();
	}
	
	public Content getQueryContent(String nodeType) {
		if (nodeTypeToQueryContent.isEmpty()) {
			queries.forEach(q -> {
				String nt = q.getType();
				Content c = q.getContent();
				nodeTypeToQueryContent.put(nt, c);
			});
		}
		
		return nodeTypeToQueryContent.get(nodeType);
	}
	
	public List<Query> getQueries() {
		return queries;
	}

	public void addQuery(Query query) {
		queries.add(query);
	}

	@Override
	public String toString() {
		return "ApplicationDataQuery [queries=" + queries + "]";
	}
}
