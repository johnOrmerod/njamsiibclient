package com.w3p.im.iib.mon.client.config.data;

public class CloudServer {
	
	private static final String PATH_DELIM = "/";

	private String propertiesLoc;
	private String endpoint;
	private String apiKey;
	private String clientInstanceId;
	private String clientCertificate; 
	private String clientPrivateKey;
	
	
	public String getPropertiesLoc() {
		return propertiesLoc;
	}
	public CloudServer setPropertiesLoc(String propertiesLoc) {
		this.propertiesLoc = propertiesLoc.concat(PATH_DELIM);
		return this;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public CloudServer setEndpoint(String endpoint) {
		this.endpoint = endpoint;
		return this;
	}
	public String getApiKey() {
		return propertiesLoc.concat(apiKey);
	}
	public CloudServer setApiKey(String apiKey) {
		this.apiKey = apiKey;
		return this;
	}
	public String getClientInstanceId() {
		return propertiesLoc.concat(clientInstanceId);
	}
	public CloudServer setClientInstanceId(String clientInstanceId) {
		this.clientInstanceId = clientInstanceId;
		return this;
	}
	public String getClientCertificate() {
		return propertiesLoc.concat(clientCertificate);
	}
	public CloudServer setClientCertificate(String clientCertificate) {
		this.clientCertificate = clientCertificate;
		return this;
	}
	public String getClientPrivateKey() {
		return propertiesLoc.concat(clientPrivateKey);
	}
	public CloudServer setClientPrivateKey(String clientPrivateKey) {
		this.clientPrivateKey = clientPrivateKey;
		return this;
	}

}
