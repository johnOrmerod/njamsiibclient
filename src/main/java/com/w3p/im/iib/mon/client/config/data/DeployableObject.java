package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


public class DeployableObject implements IDeployableObject  {
	
	private final String name;
	private final boolean includeSchemaName;
	private GlobalCorrelationId globalCorrelationId;
	private ApplicationDataQuery applicationDataQuery;
	protected final List<MsgFlow> msgFlows = new ArrayList<>();
	protected final List<SubFlow> subFlows= new ArrayList<>();
	private final List<StaticLibrary> staticLibs = new ArrayList<>();

	
	public DeployableObject(String name, boolean includeSchemaName) {
		this.name = name;
		this.includeSchemaName = includeSchemaName;
	}

	@Override
	public boolean isIncludeSchemaName() {
		return includeSchemaName;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setGlobalCorrelationId(GlobalCorrelationId globalCorrelationId) {
		this.globalCorrelationId = globalCorrelationId;
	}

	@Override
	public GlobalCorrelationId getGlobalCorrelationId(String msgFlowName) {
		if (msgFlows.size() == 1 && msgFlows.get(0).getName().equals("*")) {
			return globalCorrelationId; // use defaults from parent
		}
				
		for (MsgFlow msgFlow : msgFlows) {
			if (includeSchemaName  || !msgFlow.getName().contains(".") ? msgFlow.getName().equals(msgFlowName) : StringUtils.substringAfterLast(msgFlow.getName(), ".").equals(msgFlowName)) {
				GlobalCorrelationId mfGlobalCorrelationId = msgFlow.getGlobalCorrelationId();
				if (mfGlobalCorrelationId != null) {					
					if (mfGlobalCorrelationId.isPopulated()) {
						return mfGlobalCorrelationId;
					} else {
						return globalCorrelationId; // Use parent value if none for msgflow 
					}					
				}
			}
		}
		
		return globalCorrelationId;
	}
	
	@Override
	public void setApplicationDataQuery(ApplicationDataQuery applicationDataQuery) {
	this.applicationDataQuery = applicationDataQuery;
}

	@Override
	public ApplicationDataQuery getApplicationDataQuery(String msgFlowName) {
		if (msgFlows.size() == 1 && msgFlows.get(0).getName().equals("*")) {
			return applicationDataQuery; // use defaults from parent
		}
		
		for (MsgFlow msgFlow : msgFlows) {
			if (includeSchemaName  || !msgFlow.getName().contains(".") ? msgFlow.getName().equals(msgFlowName) : StringUtils.substringAfterLast(msgFlow.getName(), ".").equals(msgFlowName)) {
				ApplicationDataQuery mfApplicationDataQuery = msgFlow.getApplicationDataQuery();
				if (mfApplicationDataQuery != null) {
					if (mfApplicationDataQuery.isPopulated()) {
						return mfApplicationDataQuery;
					} else {
						return applicationDataQuery;
					}
				}
			}
		}
		
		return applicationDataQuery;
	}
	@Override
	public List<MsgFlow> getMsgFlows() {
		return msgFlows;
	}
	
	@Override
	public List<SubFlow> getSubFlows() {
		return subFlows;
	}
	
	@Override
	public List<StaticLibrary> getStaticLibs() {
		// An Application / RestApi / Service must always monitor all Static Libs that it uses.
		if (staticLibs.isEmpty()) {
			StaticLibrary sl = new StaticLibrary("*", includeSchemaName);
			sl.getMsgFlows(); // Getter initialises with name = "*"
			sl.getSubFlows();
			staticLibs.add(sl);
		}
		return staticLibs;
	}

	@Override
	public String toString() {
		return "DeployableObject [name=" + name + "]";
	}
}
