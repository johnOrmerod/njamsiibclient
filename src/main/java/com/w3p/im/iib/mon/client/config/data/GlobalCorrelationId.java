package com.w3p.im.iib.mon.client.config.data;

import java.util.ArrayList;
import java.util.List;

public class GlobalCorrelationId {
	
	private List<Node> nodes = new ArrayList<>();
	private NodeTypeToCorrelationIds nttcis = null; 
	private NodeTypeToNamespaces nttns = null;
	

	public List<Node> getNodes() {
		return nodes;
	}
	
	public boolean isPopulated() {
		return !nodes.isEmpty();
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	@Override
	public String toString() {
		return "GlobalCorrelationId [nodes=" + nodes + "]";
	}

	
	public NodeTypeToCorrelationIds getNodeTypeToCorrelationIds () {
		if (nttcis == null) {
			nttcis = new NodeTypeToCorrelationIds();
			for (Node node : nodes) {
				nttcis.getNodeTypeToCorrelationIdsMap().put(node.getType(), node.getXpath());
			}
		}
		return nttcis;		
	}
	
	public NodeTypeToNamespaces getNodeTypeToNamespaces () {
		if (nttns == null) {
			nttns = new NodeTypeToNamespaces();
			for (Node node : nodes) {
				List<Namespace> nsl = new ArrayList<>();				
				for (Namespace namespace : node.getNamespaces().getNamespaces()) {
					nsl.add(namespace);					
				}
				nttns.getNodeTypeToNamespacesMap().put(node.getType(), nsl);	
			}
		}
		return nttns;		
	}
}
