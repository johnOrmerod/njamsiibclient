package com.w3p.im.iib.mon.client.config.data;

import java.util.List;

public interface IDeployableObject extends IConfigObject{
	
	String getName();
	
	List<MsgFlow> getMsgFlows();

	List<SubFlow> getSubFlows();
	
	void setGlobalCorrelationId(GlobalCorrelationId globalCorrelationId);
	
	GlobalCorrelationId getGlobalCorrelationId(String msgFlowName);
	
	void setApplicationDataQuery(ApplicationDataQuery applicationDataQuery);
	
	ApplicationDataQuery getApplicationDataQuery(String msgFlowName);
	
	List<StaticLibrary> getStaticLibs();
}
