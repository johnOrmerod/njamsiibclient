package com.w3p.im.iib.mon.client.config.data;

public class Namespace {
	
	private final String prefix;
	private final String uri;
	
	
	public Namespace(String prefix, String uri) {
		this.prefix = prefix;
		this.uri = uri;
	}

	public String getPrefix() {
		return prefix;
	}
	public String getUri() {
		return uri;
	}
	@Override
	public String toString() {
		return "Namespace [prefix=" + prefix + ", uri=" + uri + "]";
	}	
}
