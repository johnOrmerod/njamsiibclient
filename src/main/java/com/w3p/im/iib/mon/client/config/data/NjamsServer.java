package com.w3p.im.iib.mon.client.config.data;

public class NjamsServer {
	
	private String serverType;
	private boolean  subflowDrillDown;
	private JmsServer jmsServer;
	private CloudServer cloudServer;

	
	public String getServerType() {
		return serverType;
	}
	public void setServerType(String serverType) {
		this.serverType = serverType;
	}
	public boolean isSubflowDrillDown() {
		return subflowDrillDown;
	}
	public void setSubflowDrillDown(boolean subflowDrillDown) {
		this.subflowDrillDown = subflowDrillDown;
	}
	public JmsServer getJmsConfig() {
		return jmsServer;
	}
	public void setJmsConfig(JmsServer jmsConfig) {
		this.jmsServer = jmsConfig;
	}
	public CloudServer getCloudConfig() {
		return cloudServer;
	}
	public void setCloudConfig(CloudServer cloudConfig) {
		this.cloudServer = cloudConfig;
	}
	@Override
	public String toString() {
		return "NjamsServer [serverType=" + serverType + ", jmsConfig=" + jmsServer + ", cloudConfig=" + cloudServer + "]";
	}	
}
