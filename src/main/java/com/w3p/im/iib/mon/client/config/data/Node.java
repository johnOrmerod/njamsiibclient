package com.w3p.im.iib.mon.client.config.data;

public class Node {
	
	private final String type;
	private final String xpath;
	private Namespaces namespaces;
	
	
	public Node(String type, String xpath) {
		this.type = type;
		this.xpath = xpath;
	}

	public String getType() {
		return type;
	}
	public String getXpath() {
		return xpath;
	}
	public Namespaces getNamespaces() {
		return namespaces;
	}

	public void setNamespaces(Namespaces namespaces) {
		this.namespaces = namespaces;
	}

	@Override
	public String toString() {
		return "Node [type=" + type + ", xpath=" + xpath + ", namespaces=" + namespaces + "]";
	}	
}
