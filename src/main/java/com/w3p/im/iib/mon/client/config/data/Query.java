package com.w3p.im.iib.mon.client.config.data;

public class Query {
	
	private String type; // ACE/IIB node type
	private Content content;
	

	public Query() {
	}

	public Query(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Content getContent() {
		return content;
	}

	public void setContent(Content content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "Query [type=" + type + ", content=" + content + "]";
	}
}
