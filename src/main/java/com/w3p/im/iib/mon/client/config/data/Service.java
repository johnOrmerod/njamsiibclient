package com.w3p.im.iib.mon.client.config.data;

public class Service extends DeployableObject { // IRunnableObject {
	
	
	public Service(String name, boolean includeSchemaName) {
		super(name, includeSchemaName);
	}

	@Override
	public String toString() {
		return "Service [name=" + getName() + "]";
	}
}
