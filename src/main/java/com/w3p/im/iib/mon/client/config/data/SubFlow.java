package com.w3p.im.iib.mon.client.config.data;

public class SubFlow extends Flow{	

	public SubFlow(String name, boolean includePayload, boolean labelNodeStartsGroup) {
		super(name, includePayload, labelNodeStartsGroup);
	}

	@Override
	public String toString() {
		return "SubFlow [name=" + name + "]";
	}	
}
