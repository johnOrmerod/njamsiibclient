package com.w3p.im.iib.mon.client.config.data;

import java.nio.charset.Charset;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
//import org.shaded.apache.commons.codec.binary.Base64;

public class User {
	
	protected String userName;
	protected String password;
	
	
	public User() {
	}

	public User(String name, String password) {
		super();
		this.userName = name;
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * If the password is between '{' and '}' the value is decoded as Base64.
	 * If not in parenthesis, the stored value is returned. 
	 * @return 
	 */
	public String getPasswordInClear() {
		String clearPwd = password;
        // Look for a value inside braces '{password}'
        if (password.startsWith("{") && password.endsWith("}")) {
        	String encodedPwd = StringUtils.substringBetween(password, "{", "}");
        	clearPwd = new String(Base64.getDecoder().decode(encodedPwd.getBytes()), Charset.defaultCharset()); 
        } 
		return clearPwd;
	}
	
	/**
	 * Searches for a value between '{'and '}'.<br>
	 * If found, returns the value between {}, otherwise the actual value.
	 * @return
	 */
	public String getPasswordEncoded() {
		if (password.startsWith("{") && password.endsWith("}")) {
			return StringUtils.substringBetween(password, "{", "}");
		}
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", password=" + password + "]";
	}
}
