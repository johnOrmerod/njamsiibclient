package com.w3p.im.iib.mon.client.config.groovy

import com.im.njams.sdk.communication.cloud.CloudSender
import com.w3p.im.iib.mon.client.config.data.Application
import com.w3p.im.iib.mon.client.config.data.ApplicationDataQuery
import com.w3p.im.iib.mon.client.config.data.ClientConfig
import com.w3p.im.iib.mon.client.config.data.CloudServer
import com.w3p.im.iib.mon.client.config.data.Connections
import com.w3p.im.iib.mon.client.config.data.Content
import com.w3p.im.iib.mon.client.config.data.DataTypeToQueryText
import com.w3p.im.iib.mon.client.config.data.GlobalCorrelationId
import com.w3p.im.iib.mon.client.config.data.IIBConfig
import com.w3p.im.iib.mon.client.config.data.IbmMqConfig
import com.w3p.im.iib.mon.client.config.data.IntegrationServer
import com.w3p.im.iib.mon.client.config.data.JmsEvents
import com.w3p.im.iib.mon.client.config.data.JmsServer
import com.w3p.im.iib.mon.client.config.data.Jndi
import com.w3p.im.iib.mon.client.config.data.Monitoring
import com.w3p.im.iib.mon.client.config.data.MqttConfig
import com.w3p.im.iib.mon.client.config.data.MsgFlow
import com.w3p.im.iib.mon.client.config.data.Namespace
import com.w3p.im.iib.mon.client.config.data.Namespaces
import com.w3p.im.iib.mon.client.config.data.NjamsServer
import com.w3p.im.iib.mon.client.config.data.Node
import com.w3p.im.iib.mon.client.config.data.Profile
import com.w3p.im.iib.mon.client.config.data.Query
import com.w3p.im.iib.mon.client.config.data.RestApi
import com.w3p.im.iib.mon.client.config.data.Service
import com.w3p.im.iib.mon.client.config.data.SharedLibrary
//import com.w3p.im.iib.mon.client.config.data.StaticLibrary
import com.w3p.im.iib.mon.client.config.data.SubFlow
import com.w3p.im.iib.mon.client.config.data.User
import com.w3p.im.iib.mon.exceptions.internal.ParseClientConfigException
import groovy.util.slurpersupport.NodeChild
import groovy.util.slurpersupport.NodeChildren
import net.sf.saxon.expr.TryCatch.CatchClause

import java.util.regex.*

import org.codehaus.groovy.ast.stmt.CatchStatement

class ParseClientConfig {
	
	
	def ClientConfig parse (String clientConfigXML) throws ParseClientConfigException {

		ClientConfig cc = new ClientConfig();

//		slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true)
		def config = new XmlSlurper(false, false).parseText(clientConfigXML);

		cc.type = config.'Type'
		cc.clientMode = config.'ClientMode'
		// CONNECTIONS
		Connections conns = new Connections()
		cc.connections = conns
		NodeChildren connections = config.Connections
		NodeChildren iibEventSource = connections.IibEventSource
		conns.iibEventsSource = connections.IibEventsSource.size() == 0 ? 'JMS_TOPIC' : (connections.IibEventsSource == 'MQTT_TOPIC') ? 'MQTT_TOPIC' : connections.IibEventsSource // Default to TOPIC
		conns.iibEventsProblemsQueue = connections.IibEventsProblemsQueue.size() == 0 ? null : connections.IibEventsProblemsQueue // Can be null for DEV mode // FIXME - pass MODE to this

		// IBM MQ Config
		createIbmMQ(conns, connections)
		// MQTT config
		createMQTT(conns, connections)
		//nJAMS Server Config
		createNjamsServer(conns, connections)
		// IIB Config
		createIIBConfig(conns, connections)
		
		// MONITORING
		Monitoring mon = new Monitoring()
		cc.monitoring = mon
		def monitoring = config.Monitoring

		// Validate and default Consumer Thread count - if present in the XML, the XSD forces a value > 0
		def tc = monitoring.ConsumerThreads
		def threadCount = tc.text()
		if (threadCount == "") {
			mon.consumerThreads = 1  // Default value
		} else {
			mon.consumerThreads = threadCount as Integer
		}
		
		createMonitoringProfileConfig(mon, monitoring)
		
		// INTEGRATION SERVERS
		def integrationServers = monitoring.IntegrationServers
		integrationServers.IntegrationServer.each { intSvr ->
			// println intSvr.@name.text()
			createIntServer(mon, intSvr)			
		}

		return cc;
	}

	def createIbmMQ = { Connections conns, NodeChildren nodeConns ->
		// IbmMqConfig - Optional		
		IbmMqConfig ibmMqConf = new IbmMqConfig()
		conns.ibmMqConfig = ibmMqConf
		def ibmMq = nodeConns.IbmMQ
		if (ibmMq.size() == 1) {			// note: isEmpty() or 'empty' always returns true? 
			JmsEvents jmsEventsConf = new JmsEvents()
			ibmMqConf.jmsEvents = jmsEventsConf
			// JmsEvents
			def jmsEvents = ibmMq.JmsEvents
			jmsEventsConf.providerURL = jmsEvents.JMSProviderURL
			jmsEventsConf.initialContextFactory = jmsEvents.JMSInitialContextFactory
			jmsEventsConf.connectionFactory = jmsEvents.JMSConnectionFactory
			jmsEventsConf.jmsClientId = jmsEvents.JMSClientId
			jmsEventsConf.jmsMqUser = new User(jmsEvents.JMSMQUser.@name.text(), jmsEvents.JMSMQUser.@password.text())
		}
	}
	
	def createMQTT = { Connections conns, NodeChildren nodeConns ->
		// MqttConfig - Optional		
		MqttConfig mqttConf = new MqttConfig()
		conns.mqttConfig = mqttConf
		def mqtt = nodeConns.MQTT
		if (mqtt.size() == 1) {			
			mqttConf.clientId = mqtt.ClientId
			mqttConf.host = mqtt.Host
			mqttConf.port = mqtt.Port.text() as Integer
			mqttConf.keepAliveInterval = mqtt.KeepAliveInterval.text() as Integer
			mqttConf.qos = mqtt.QoS.text() as Integer
			mqttConf.MQTTUser = new User(mqtt.MQTTUser.@name.text(), mqtt.MQTTUser.@password.text())
		}
	}
	
	def createNjamsServer = {Connections conns, NodeChildren nodeConns ->
		// NjamsServer Config
		NjamsServer serverConf = new NjamsServer()
		serverConf.serverType = nodeConns.NjamsServer.ServerType
		serverConf.subflowDrillDown = nodeConns.NjamsServer.SubflowDrillDown
		conns.serverConfig = serverConf		
		JmsServer jmsServerConf = new JmsServer()
		serverConf.jmsConfig = jmsServerConf
		CloudServer cloudServerConf = new CloudServer()
		serverConf.cloudConfig = cloudServerConf

		def njamsServer = nodeConns.NjamsServer
		
		// JmsServer				
		def jmsServer = njamsServer.JmsServer
		jmsServerConf.jmsUser = new User(jmsServer.JmsUser.@name.text(), jmsServer.JmsUser.@password.text())
		jmsServerConf.destination = jmsServer.JmsDestination
		jmsServerConf.destinationCommands = jmsServer.JmsDestinationCommands
		jmsServerConf.providerURL = jmsServer.JMSProviderURL
		jmsServerConf.initialContextFactory = jmsServer.JMSInitialContextFactory
		jmsServerConf.connectionFactory = jmsServer.JMSConnectionFactory
		jmsServerConf.useSSL = jmsServer.UseSSL.toBoolean()
		// JNDI		
		Jndi jndiConf = new Jndi()
		jmsServerConf.jndi = jndiConf
		def jndi = jmsServer.JNDI
		jndiConf.user = new User(jndi.JndiUser.@name.text(), jndi.JndiUser.@password.text())
		
		// CloudServer
		def cloudServer = njamsServer.CloudServer
		cloudServerConf.propertiesLoc = cloudServer.PropertiesLoc
		cloudServerConf.apiKey = cloudServer.API_Key 
		cloudServerConf.endpoint = cloudServer.Endpoint
		cloudServerConf.clientCertificate = cloudServer.ClientCertificate
		cloudServerConf.clientInstanceId = cloudServer.ClientInstanceId
		cloudServerConf.clientPrivateKey = cloudServer.ClientPrivateKey		
	}
	
	def createIIBConfig = {Connections conns, NodeChildren nodeConns ->
		// IIBConfig		
		def iib = nodeConns.IIB
		IIBConfig iibConfig = new IIBConfig()
		conns.iibConfig = iibConfig
		iibConfig.host = iib.Host
		iibConfig.intNode = iib.IntegrationNode.@name
		iibConfig.connectionType = iib.ConnectionType
		iibConfig.adminPort = iib.AdminPort.text() as Integer
		iibConfig.getIibUser().userName = iib.IIBUser.@name.text()
		iibConfig.getIibUser().password = iib.IIBUser.@password.text()
	}		
		
	def createMonitoringProfileConfig = {Monitoring mon,  NodeChildren monitoring -> 
		// Profile
		def profile = monitoring.Profile
		Profile profileConf = new Profile(profile.Directory.text(), profile.ApplyIfUnchanged.toBoolean())
		profileConf.timeToWait = profile.'TimeToWait'.text() as Integer
		mon.profile = profileConf
	}
	
	def createIntServer = {Monitoring mon, NodeChild intSvr ->
		IntegrationServer intSvrConfig = new IntegrationServer(intSvr.@name.text(), intSvr.@includeName.toBoolean(), intSvr.@includeSchemaName.toBoolean())
		mon.getIntegrationServers().add(intSvrConfig)
		intSvr.MsgFlow?.each {msgflow ->
			if (!msgflow?.@name.text().isEmpty()) {
				// println 'msg flow: '+msgflow.@name.text()
				createMsgFlow(intSvrConfig, msgflow)
			}
		}
		intSvr.SubFlow?.each {subflow ->
			if (!subflow?.@name.text().isEmpty()) {
				// println 'sub flow: '+subflow.@name.text()
				createSubFlow(intSvrConfig, subflow)
			}
		}
		// Applications
		intSvr.Applications?.Application?.each { app ->
			if (!app.@name.text().isEmpty()) {
				// println 'app name: '+app.@name.text()
				Application appConfig = new Application(app.@name.text(), app.@includeSchemaName.toBoolean())
				intSvrConfig.applications.add(appConfig)
				createApplication(appConfig, app)
			}
		}
		// SharedLibraries
		intSvr.SharedLibraries?.Library?.each { lib ->
			if (!lib.@name.text().isEmpty()) {
				// println 'shared lib name: '+lib.@name.text()
				SharedLibrary shlConfig = new SharedLibrary(lib.@name.text(), lib.@includeSchemaName.toBoolean())
				intSvrConfig.sharedLibs.add(shlConfig)
				createSharedLib(shlConfig, lib)
			}
		}
//		// Static Libraries
//		intSvr.StaticLibraries?.Library?.each { lib ->
//			if (!lib.@name.text().isEmpty()) {
//				// println 'static lib name: '+lib.@name.text()
//				StaticLibrary stlConfig = new StaticLibrary(lib.@name.text(), lib.@includeSchemaName.toBoolean())
//				intSvrConfig.staticLibs.add(stlConfig)
//				createStaticLib(stlConfig, lib)
//			}
//		}
		// Rest APIs
		intSvr.RestAPIs?.RestAPI?.each { api ->
			if (!api.@name.text().isEmpty()) {
				// println 'rest api name: '+api.@name.text()
				RestApi apiConfig = new RestApi(api.@name.text(), api.@includeSchemaName.toBoolean())
				intSvrConfig.restAPIs.add(apiConfig)
				createRestApi(apiConfig, api)
			}
		}
		// Services (SOAP)
		intSvr.Services?.Service?.each { service ->
			if (!service.@name.text().isEmpty()) {
				// println 'rest api name: '+api.@name.text()
				Service serviceConfig = new Service(service.@name.text(), service.@includeSchemaName.toBoolean())
				intSvrConfig.services.add(serviceConfig)
				createService(serviceConfig, service)
			}
		}
	}

	def  createApplication = {Application appConfig, NodeChild app ->
		def gci = app.GlobalCorrelationId
		if (gci) {
			createGlobalCorrelationId(appConfig, gci)
		}
		def adq = app.ApplicationDataQuery
		if (adq) {		
			createApplicationDataQuery(appConfig, adq)
		} 
		app.MsgFlow?.each {msgflow ->
			if (!msgflow.@name.text().isEmpty()) {
				// println 'msg flow: '+msgflow.@name.text()
				createMsgFlow(appConfig, msgflow)
			}
		}
		app.SubFlow?.each {subflow ->
			if (!subflow.@name.text().isEmpty()) {
				// println 'sub flow: '+subflow.@name.text()
				createSubFlow(appConfig, subflow)

			}
		}
	}

	def  createSharedLib = {SharedLibrary libConfig, NodeChild lib ->
		lib.SubFlow?.each {subflow ->
			if (!subflow.@name.text().isEmpty()) {
				// println 'sub flow: '+subflow.@name.text()
				createSubFlow(libConfig, subflow)
			}
		}
	}

//	def  createStaticLib = {StaticLibrary libConfig, NodeChild lib ->
//		lib.MsgFlow?.each {msgflow ->
//			if (!msgflow.@name.text().isEmpty()) {
//				// println 'msg flow: '+msgflow.@name.text()
//				createMsgFlow(libConfig, msgflow)
//			}
//		}
//		lib.SubFlow?.each {subflow ->
//			if (!subflow.@name.text().isEmpty()) {
//				// println 'sub flow: '+subflow.@name.text()
//				createSubFlow(libConfig, subflow)
//			}
//		}
//	}

	def  createRestApi = {RestApi apiConfig, NodeChild api ->
		def gci = api.GlobalCorrelationId
		if (gci) {
			createGlobalCorrelationId(apiConfig, gci)
		}
		def adq = api.ApplicationDataQuery
		if (adq) {		
			createApplicationDataQuery(apiConfig, adq)
		} 
		api.MsgFlow?.each {msgflow ->
			if (!msgflow.@name.text().isEmpty()) {
				// println 'msg flow: '+msgflow.@name.text()
				createMsgFlow(apiConfig, msgflow)
			}
		}
		api.SubFlow?.each {subflow ->
			if (!subflow.@name.text().isEmpty()) {
				// println 'sub flow: '+subflow.@name.text()
				createSubFlow(apiConfig, subflow)
			}
		}
	}
	
	def  createService = {Service serviceConfig, NodeChild service ->
		def gci = service.GlobalCorrelationId
		if (gci) {
			createGlobalCorrelationId(serviceConfig, gci)
		}
		def adq = service.ApplicationDataQuery
		if (adq) {		
			createApplicationDataQuery(serviceConfig, adq)
		} 
		service.MsgFlow?.each {msgflow ->
			if (!msgflow.@name.text().isEmpty()) {
				// println 'msg flow: '+msgflow.@name.text()
				createMsgFlow(serviceConfig, msgflow)
			}
		}
		service.SubFlow?.each {subflow ->
			if (!subflow.@name.text().isEmpty()) {
				// println 'sub flow: '+subflow.@name.text()
				createSubFlow(serviceConfig, subflow)
			}
		}
	}

	def createMsgFlow = {def parent, NodeChild msgflow ->
		def attrmap = msgflow.attributes()
		boolean includePayloadPresent = attrmap.containsKey("includePayload")
		boolean includePayload = includePayloadPresent ? msgflow.@includePayload.toBoolean() : true // Make absence default to true
		boolean labelNodeStartsGroupPresent = attrmap.containsKey("labelNodeStartsGroup")
		boolean labelNodeStartsGroup = labelNodeStartsGroupPresent ? msgflow.@labelNodeStartsGroup.toBoolean() : false // Make absence default to false
		MsgFlow mf = new MsgFlow(msgflow.@name.text(), includePayload, labelNodeStartsGroup)

		def gci = msgflow.GlobalCorrelationId
		if (gci) {
			createGlobalCorrelationId(mf, gci)
		}
		def adq = msgflow.ApplicationDataQuery
		if (adq) {
			createApplicationDataQuery(mf, adq)
		}
		// println 'created msg flow: '+mf.name
		parent.msgFlows.add(mf)
	}

	def createSubFlow = {def parent, NodeChild subflow ->
		def attrmap = subflow.attributes()
		boolean includePayloadPresent = attrmap.containsKey("includePayload")
		boolean includePayload = includePayloadPresent ? subflow.@includePayload.toBoolean() : true // Make absence default to true 
		boolean labelNodeStartsGroupPresent = attrmap.containsKey("labelNodeStartsGroup")
		boolean labelNodeStartsGroup = labelNodeStartsGroupPresent ? subflow.@labelNodeStartsGroup.toBoolean() : false // Make absence default to false
		SubFlow sf = new SubFlow(subflow.@name.text(), includePayload, labelNodeStartsGroup)
		// println 'created sub flow: '+sf.name
		parent.subFlows.add(sf)
	}
	
	def createGlobalCorrelationId = {def parent, NodeChildren globalCorrelId ->
		GlobalCorrelationId globalCorrelationId = new GlobalCorrelationId()
		parent.globalCorrelationId = globalCorrelationId
		globalCorrelId.Node.each {n ->
			Node node = new Node(n.@type.text(), n.@xpath.text())
			def namespaces = n.Namespaces
			if (namespaces) {
				Namespaces nspaces = new Namespaces()
				namespaces.Namespace.each {ns ->
					ns.each  {it ->
						Namespace namespace = new Namespace(it.@prefix.text(), it.@uri.text())
						nspaces.addNamespace(namespace)
					}
				}
				node.namespaces = nspaces
			}
			globalCorrelationId.nodes.add(node)
		}
	}

	
	def createApplicationDataQuery = {def parent, NodeChildren appDataQuery ->
		ApplicationDataQuery applicationDataQuery = new ApplicationDataQuery()		
		def queryElement = appDataQuery.Query
		if (!queryElement.isEmpty()) {
			queryElement.each { qe ->
				Query query = new Query(qe.@type.text())
				def contentElement = qe.Content
				Content content = new Content(contentElement.@name.text(), contentElement.@xpath.text())
				def namespacesElement = contentElement.Namespaces
				Namespaces namespaces = new Namespaces()
				if (!namespacesElement.isEmpty()) {
					def namespaceElement = namespacesElement.Namespace
					namespacesElement.Namespace.each { ns ->
						Namespace namespace = new Namespace(ns.@prefix.text(), ns.@uri.text())
						namespaces.addNamespace(namespace)
					}				
				}
				content.namespaces = namespaces
				query.content = content
				applicationDataQuery.addQuery(query)
			}
		}
		parent.applicationDataQuery = applicationDataQuery
	}
}
