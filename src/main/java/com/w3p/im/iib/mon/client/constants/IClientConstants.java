package com.w3p.im.iib.mon.client.constants;


public interface IClientConstants {
	
	// IBM Integration Server version
	String IBM_SERVER_VERSION = "IIB-10";
	String IBM_SERVER_IMAGE = "iib.png";


	// This client's version
	String CLIENT_VERSION = "version";
	
	// Run Mode - PROD or DEV
	String PROD = "PROD";
	String DEV = "DEV";
	
	// nJAMS server type
	String CLOUD_SERVER = "CLOUD";
	String ON_PREM_SERVER = "JMS";
	
	// Event Sources
	String QUEUE = "queue://";
	String JMS_TOPIC = "JMS_TOPIC";
	String MQTT_TOPIC = "MQTT_TOPIC";
	
	String STEP_TYPE = "stepType";
	String SUBPROCESS = "subprocess";

	String ROUTE_TO_LABEL_NODE = "ComIbmRouteToLabelNode";
	String OUTPUT_NODE = "OutputNode";
	String SUB_FLOW_NODE = "SubFlowNode";
	String LABEL_NODE = "ComIbmLabelNode";
	String MQ_OUTPUT_NODE = "ComIbmMQOutputNode";
	String PUB_SUB_NODE = "ComIbmPSServiceNode";	
	
	String GROUP_END_MODEL_ID = "groupend";
	String GROUP_END_NAME = "GroupEnd"; // can't be empty
	String GROUP_END_TYPE = "groupEndType";

	String GROUP_START_MODEL_ID = "groupstart";
	String GROUP_START_NAME = "GroupStart"; // can't be empty
	String GROUP_START_TYPE = "groupStartType";
	String GROUP_TYPE = "groupType";

	String END_ACTIVITY_TYPE = "endType";
	String END_ACTIVITY_NAME = "End";
	String END_ACTIVITY_MODEL_ID = "end";	

	String START_ACTIVITY_TYPE = "startType";
	String START_ACTIVITY_NAME = "Start";
	String START_ACTIVITY_MODEL_ID = "start";
	
	String GROUP_NAME_PREFIX = "G_"; // Prefix to the Label node's name

	String GT = ">"; // Separates Path elements
	String UNDERSCORE = "_";
	String SPACE = " ";
	String FWD_SLASH = "/";
	String BACK_SLASH = "\\";
	
	public final static String DOT = ".";
	String REGEX_DOT = "\\.";
	String DOT_PNG = ".png";
	
	String IMAGES = "images";
	
	// For passing startup config values
	String LABEL_NODE_STARTS_GROUP = "labelNodeStartsGroup";
	String INCLUDE_SCHEMA_NAME = "includeSchemaName";
	String INCLUDE_SERVER_NAME = "includeServerName";
	String INCLUDE_PAYLOAD = "includePayload";
	
	// General constants
	String DOT_XML = ".xml";
	String UTF_8 = "UTF-8";
	String USER_DIR = "user.dir";
	
	// Monitoring Profiles
	String MON_PROF ="_MonitoringProfile"; // IIB name suffix
	
	// FOR IIB 10: IIB's Topic pattern formats for monitoring events published to MQ and MQTT
	String EVENT_TOPIC_PATTERN = "$SYS/Broker/{0}/Monitoring/{1}/{2}";
	String EVENT_TOPIC_PATTERN_MQTT = "IBM/IntegrationBus/{0}/Monitoring/{1}/{2}";
	
	// Subflow uri for a Shared Lib differes between IIB and ACE
	String SUBFLOW_URI_SHARED_LIB = "sharedlibraries"; // In ACE it's 'shared-libraries'

		
	// Used by the I/O utilities that read and write Files and Queues
	/**
	 * Size of buffer used when reading files. Desfulat = 4096
	 */
	String READBUFFER_SIZE = "readBufSize"; // files-reading performance - size of buffer for each 'chunk' of file

	// Values for File and Queue i/o requests
	/**
	 * Use JMS User properties as a basis for creating the key in IContents
	 * Returns Boolean
	 */
	String NAME_USING_MSG_PROPERTIES = "selectUsingMsgProperties"; // WAS useMsgPropertieForName

	/**
	 * Pattern used for selecting files and naming outputs (files and entries in IContents)
	 */
	String DEFAULT_NAME_PATTERN = "*.*";
	
	// JMS Message constants
	/**
	 * Create message as a byte-array
	 */
	String BYTES = "bytes";
	
	/**
	 * Create message as text
	 */
	String TEXT = "text";
	
	/**
	 * Read from a queue - destroys the message.
	 */
	String GET = "get";
	
	
	/**
	 * Read from a queue - message stays on the queue
	 */
	String BROWSE = "browse";

	/**
	 * Should the a correlation id be created when writiing a message.
	 */
	String GEN_CORREL_ID = "genCorrelId";
	
}
