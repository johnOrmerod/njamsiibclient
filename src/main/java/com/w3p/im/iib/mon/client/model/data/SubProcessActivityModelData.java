package com.w3p.im.iib.mon.client.model.data;

import com.im.njams.sdk.common.Path;

public class SubProcessActivityModelData extends ActivityModelData {

	private static final long serialVersionUID = 1L;
	
	private ProcessModelData subProcess;
	private String subProcessName;
	private PathData subProcessPath;

	public SubProcessActivityModelData(ProcessModelData processModelData) {
		super(processModelData);
	}

    public SubProcessActivityModelData(ProcessModelData processModelData, ProcessModelData subProcess) {
		super(processModelData);
		this.subProcess = subProcess;
	}

	public SubProcessActivityModelData(ProcessModelData processModel, String id, String name, String type) {
	    super(processModel, id, name, type);
	}

	public ProcessModelData getSubProcess() {
		return subProcess;
	}

	public void setSubProcess(ProcessModelData subProcess) {
		this.subProcess = subProcess;
	}

	public String getSubProcessName() {
		return subProcessName;
	}

	public void setSubProcessName(String subProcessName) {
		this.subProcessName = subProcessName;
	}

	public Path getSubProcessPath() {
		return new Path(subProcessPath.getParts());
	}

	public void setSubProcessPath(String subProcessPathAsString) {
		this.subProcessPath = new PathData(subProcessPathAsString);
	}
}
