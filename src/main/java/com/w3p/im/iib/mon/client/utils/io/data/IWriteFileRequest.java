package com.w3p.im.iib.mon.client.utils.io.data;

public interface IWriteFileRequest extends IFileRequest {
	
	public IWriteFileRequest setDestination(String dest);
	
	public String getDestination();
	
	public IWriteFileRequest setCreateFolderIfNotExists(boolean createFolderIfNotExists);
	
	public boolean isCreateFolderIfNotExists();
	
	public IWriteFileRequest setClearFolderIfNotEmpty(boolean clearFolderIfNotEmpty);
	
	public boolean isClearFolderIfNotEmpty();
	
	public IWriteFileRequest setReplaceFilesIfExist(boolean replaceIfExists);
	
	public boolean isReplaceFilesIfExist();
	
}
