package com.w3p.im.iib.mon.client.utils.io.data;

public class QueueRequest extends Request implements IQueueRequest {

//	private String dest;
	private ConnectionData connData;
	
	
	public QueueRequest() {
	}

	@Override
	public IQueueRequest setConnectionData(ConnectionData connData) {
		this.connData = connData;
		return this;
	}

	@Override
	public ConnectionData getConnectionData() {
		return connData;
	}

//	@Override
//	public void setDestination(String dest) {
//		this.dest = dest;
//	}
//
//	@Override
//	public String getDestination() {
//		return dest;
//	}
}
