package com.w3p.im.iib.mon.client.utils.io.data;

public class ReadFileRequest extends FileRequest implements IReadFileRequest {
	
	private static final int DEFAULT_READ_BUF_SIZE = 4096;
	
	private String folder; 
	private int readBufSize;
	
		
	public ReadFileRequest() {
	}

	@Override
	public String getSource() {
		return folder;
	}

	@Override
	public ReadFileRequest setSource(String source) {
		this.folder = source;
		return this;
	}

	public ReadFileRequest setReadBufSize(int readBufSize) {
			this.readBufSize = readBufSize;
			return this;
		}

	public int getReadBufSize() {
		if (readBufSize == 0) {
			return DEFAULT_READ_BUF_SIZE;
		}
		return readBufSize;
	}

}
