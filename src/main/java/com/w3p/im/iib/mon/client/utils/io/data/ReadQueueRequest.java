package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.BROWSE;

import java.util.Optional;
	
public class ReadQueueRequest extends QueueRequest implements IReadQueueRequest {
	
	private String mode;
	private String msgSelector;
	
		
	public ReadQueueRequest() {
	}

	public String getMode() {
		if (mode == null) {
			mode = BROWSE; // Default to BROWSE
		}
		return mode;
	}

	public IReadQueueRequest setMode(String mode) {
		this.mode = mode;
		return this;
	}

	public Optional<String> getMsgSelector() {
		return msgSelector == null ? Optional.ofNullable(msgSelector) :  
			msgSelector.isEmpty() ? Optional.empty() : Optional.of(msgSelector);
	}

	
	public IReadQueueRequest setMsgSelector(String msgSelector) {
		this.msgSelector = msgSelector;
		return this;
	}
}
