package com.w3p.im.iib.mon.client.utils.io.exceptions;

public class IOUtilsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IOUtilsException() {
		super();
	}

	public IOUtilsException(String message) {
		super(message);
	}

	public IOUtilsException(Throwable cause) {
		super(cause);
	}

	public IOUtilsException(String message, Throwable cause) {
		super(message, cause);
	}
}
