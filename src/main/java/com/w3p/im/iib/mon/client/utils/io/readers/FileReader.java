package com.w3p.im.iib.mon.client.utils.io.readers;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;


public class FileReader implements IFileReader, Closeable {
	private static final Logger log = LoggerFactory.getLogger(FileReader.class);
	
	private FilesToRead filesToRead;
	private int readBufferSize;


	public FileReader() {
	}

	@Override
	public void init(IReadFileRequest req, Response resp) {
		filesToRead = new FindFilesToRead().find(req, resp);
		readBufferSize = req.getReadBufSize();
	}
	
	@Override
	public IContent readAll(IReadFileRequest req, Response resp) {
		FileContent fileContent = new FileContent();	
		
		for (Path filePath : filesToRead.getFiles()) {
			// Read the file into a String
			String content = getFileContentsAsString(filePath, readBufferSize, resp);
			String fileName = filePath.toFile().getName(); 
			fileContent.addContent(fileName, content);
			resp.setFilesRead(resp.getFilesRead() + 1);
		}
		
		return fileContent;
	}

	private String getFileContentsAsString(Path filePath, int readBufferSize, Response resp) {		
		// ByteBuffer: see https://www.javacodegeeks.com/2012/12/the-java-bytebuffer-a-crash-course.html
		ByteArrayOutputStream baos = null;
		ByteBuffer buf = null;
		String fileContent = null;
		try (SeekableByteChannel sbc = Files.newByteChannel(filePath, StandardOpenOption.READ)) {
			buf = ByteBuffer.allocate(readBufferSize);
			baos = new ByteArrayOutputStream();
			while (sbc.read(buf) > 0) {
				buf.flip();
				int bufRemain = buf.remaining();
				if (bufRemain == readBufferSize) {
					baos.write(buf.array());
				} else {
					byte[] bytes = new byte[bufRemain];
					buf.duplicate().get(bytes);
					baos.write(bytes);
				}
				buf.clear();
			}
	
			fileContent = baos.toString(StandardCharsets.UTF_8.toString());			
	
		} catch (UnsupportedEncodingException e) {
			String reason = String.format("Error converting contents of inputfile '%s' from byte-array.", filePath.getFileName().toString());			
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e);
		} catch (IOException e) {
			String reason = String.format("Error reading contents of inputfile '%s'. Reason is: '%s'.",	filePath.getFileName().toString(), e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
		} catch (Exception e) {
			String reason = String.format("Unhandled error reading contents of inputfile '%s'. Reason is: '%s'.", filePath.getFileName().toString(), e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
		}
		
		return fileContent;
	}

	private void deleteAfterRead(Response resp, Path filePath, String fileName) {
		try {				
			Files.delete(filePath);
			resp.setFilesDeleted(resp.getFilesDeleted() + 1);
		} catch (IOException e) {
			String reason = String.format("Error deleting the inputfile '%s' from input folder. Reason: '%s'. See log.", fileName);
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e);
		}
	}

	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub		
	}
}
