package com.w3p.im.iib.mon.client.utils.io.readers;

import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public interface IFileReader extends IReader {
	
	void init(IReadFileRequest req, Response resp);
	
	IContent readAll(IReadFileRequest req, Response resp);

}
