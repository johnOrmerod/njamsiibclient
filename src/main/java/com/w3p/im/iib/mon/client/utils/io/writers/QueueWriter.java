package com.w3p.im.iib.mon.client.utils.io.writers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.BYTES;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.io.Closeable;
import java.nio.charset.Charset;
import java.util.UUID;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.client.utils.io.data.IWriteQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;
import com.w3p.im.iib.mon.client.utils.io.jms.JmsConnector;
import com.w3p.im.iib.mon.client.utils.io.jms.JmsFactoryFactoryWrapper;


public class QueueWriter extends JmsConnector implements IQueueWriter, Closeable {	
	
	private static final Logger log = LoggerFactory.getLogger(QueueWriter.class);
	
	protected MessageProducer producer;
	private IWriteQueueRequest req;


	public QueueWriter() { 
	}

	@Override
	public void init(IWriteQueueRequest req, Response resp) {
		try {
			super.init(req.getConnectionData());
			this.req = req;
			producer = jmsSession.createProducer(jmsDest);			
		} catch (JMSException e) {
			String reason = String.format("Error configuring the JMS environment. Reason: '%s'.", e.toString());
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e);
		}
	}	

	// For jUnit testing using jms mocks
	protected void init(IWriteQueueRequest req, Response resp, JmsFactoryFactoryWrapper jffw) {
		try {
			super.init(req.getConnectionData(), jffw);
			this.req = req;
			producer = jmsSession.createProducer(jmsDest);
		} catch (JMSException e) {
			String reason = String.format("Error configuring the JMS environment. Reason: '%s'.", e.toString());
			resp.setMessage(Response.ERROR, reason);
			log.error(reason, e.toString(), e);
		}
	}

	@Override
	public void write(String name, String msgContent, Response resp)  {
		try {
			Message message = null;
			if (req.getMsgType().equals(BYTES)) {
				message = createBytesMessage(msgContent);				
			} else {
				message = createTextMessage(msgContent);
			}			
			message = addMessageExtras(message);			
			producer.send(message);
			resp.setFilesWritten(resp.getFilesWritten() + 1);
		} catch (JMSException e) {
			String reason = String.format("Error creating a JMS Text Message for '%s'. Reason: '%s'.", name, e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		} catch (Exception e) {
			// Catch any un-handled exception, such as UnmappableCharacterException from 'foreign' XML files
			String reason = String.format("Unhandled error creating the JMS Text Message for file '%s'. Reason: '%s'.", name, e.toString());
			log.error(reason, e);
			resp.setMessage(Response.ERROR, reason);
			resp.setFilesInError(resp.getFilesInError() + 1);
		}
	}

	protected BytesMessage createBytesMessage(String msgContent) throws JMSException {
		BytesMessage message = jmsSession.createBytesMessage();
		byte[] msgAsBytes = msgContent.getBytes(Charset.forName(UTF_8));
		message.writeBytes(msgAsBytes);
		return message;
	}

	protected TextMessage createTextMessage(String msgContent) throws JMSException {
		TextMessage message = jmsSession.createTextMessage(); // throws JMSRuntimeException		
		// UTF-8 This is needed to ensure that special characters. such as 'TM' superscript are handled.
		message.setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
		message.setText(msgContent);
		return message;
	}
	
	protected Message addMessageExtras(Message message) throws JMSException {
		// Set a correlationId if required
		if (req.getOption(GEN_CORREL_ID).isPresent()) { 
			if ((Boolean)req.getOption(GEN_CORREL_ID).get()) {
				message.setJMSCorrelationIDAsBytes(UUID.randomUUID().toString().getBytes());
			} 
		}
		
		// set any message properties
		for (String name : req.getMsgProperties().keySet()) {
			message.setStringProperty(name, req.getMsgProperty(name));
		}
		
		return message;
	}

	public void close() {
		try {
			if (jmsSession != null) {jmsSession.close();} 
			if (jmsConnection != null) {jmsConnection.close();}
		} catch (JMSException e) {
			// FIXME report in Response
		}		
	}
	
	

}
