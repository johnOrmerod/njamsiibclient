package com.w3p.im.iib.mon.client.utils.serialize;

import java.io.File;

import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;

public class FlowToProcessModelCacheDeserializer extends AbstractSerializer{
	

	public FlowToProcessModelCache importFlowToProcessModelCache(String inputFolder) {
		FlowToProcessModelCache cache = null;
		try {
			File serialized = new File(inputFolder, "FlowToProcessModel.cache");
			cache = (FlowToProcessModelCache)read(serialized);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cache;
	}
	
}
