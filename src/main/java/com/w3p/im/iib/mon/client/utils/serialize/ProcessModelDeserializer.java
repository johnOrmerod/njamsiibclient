package com.w3p.im.iib.mon.client.utils.serialize;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_PAYLOAD;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.INCLUDE_SCHEMA_NAME;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.LABEL_NODE_STARTS_GROUP;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.im.njams.sdk.Njams;
import com.im.njams.sdk.common.IdUtil;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;
import com.im.njams.sdk.model.TransitionModel;
import com.w3p.im.iib.mon.client.model.data.ActivityModelData;
import com.w3p.im.iib.mon.client.model.data.GroupModelData;
import com.w3p.im.iib.mon.client.model.data.ProcessModelData;
import com.w3p.im.iib.mon.client.model.data.SubProcessActivityModelData;
import com.w3p.im.iib.mon.client.model.data.TransitionModelData;

public class ProcessModelDeserializer extends AbstractSerializer {
	

	public ProcessModelDeserializer() {

	}
	
	public void importAllProcessModels(Njams njamsClient, String inputFolder) {
		try {
			for (Path path : Files.newDirectoryStream(Paths.get(inputFolder))) {
				if (path.toFile().isFile()) {
					ProcessModelData pmd = (ProcessModelData)read(path.toFile());
					ProcessModel pm = createProcessModel(njamsClient, pmd);
					njamsClient.addProcessModel(pm);					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private ProcessModel createProcessModel(Njams njamsClient, ProcessModelData pmd) {
		ProcessModel pm = new ProcessModel(pmd.getPath(), njamsClient);
		pm.setStarter(pmd.isStarter());
		pm.setSvg(pmd.getSvg());
		
		pmd.getPropertyNames().forEach(name -> pm.setProperty(name, pmd.getProperty(name)));
		
		pmd.getActivityModels().forEach(amd -> {
			if (SubProcessActivityModelData.class.isAssignableFrom(amd.getClass())) {
				pm.addActivity(createSubProcessActivityModel(pm, (SubProcessActivityModelData) amd));
			} else if (GroupModelData.class.isAssignableFrom(amd.getClass())) {
				pm.addActivity(createGroupModel(pm, (GroupModelData)amd));
			}  else {
				pm.addActivity(createActivityModel(pm, amd));
			}
		});
		
		pmd.getActivityModels().forEach(amd -> {
			// create the incoming outgoing connections for all AMDs driven by am.getId()
			ActivityModel am = pm.getActivity(amd.getId());
			amd.getPredecessors().forEach(p -> 
				am.addIncomingTransition(createTransitionModel(pmd, pm, p, amd))); 
			amd.getSuccessors().forEach(s -> 
				am.addOutgoingTransition(createTransitionModel(pmd, pm, amd, s)));
		});
		
		return pm;
	}


	private ActivityModel createActivityModel(ProcessModel pm, ActivityModelData amd) {
		ActivityModel am = new ActivityModel(pm, amd.getId(), amd.getName(), amd.getType());
		if (amd.getParent() != null) {
			String parentId = amd.getParent().getId();
			GroupModel gm = (GroupModel)pm.getActivity(parentId);
			am.setParent(gm);
		}
		am.setConfig(amd.getConfig());
		am.setMapping(amd.getMapping());
		am.setStarter(amd.isStarter());
		am.setX(amd.getX());
		am.setY(amd.getY());


		return am;
	}
	
	private SubProcessActivityModel createSubProcessActivityModel(ProcessModel pm, SubProcessActivityModelData spamd) {
		SubProcessActivityModel spam = pm.createSubProcess(spamd.getId(), spamd.getName(), spamd.getType());  //new ActivityModel(pm, amd.getId(), amd.getName(), amd.getType());
		spam.setConfig(spamd.getConfig());
		spam.setMapping(spamd.getMapping());
		spam.setStarter(spamd.isStarter());
		spam.setX(spamd.getX());
		spam.setY(spamd.getY());
//		spam.setSubProcess(subProcess); // Can't be set by the topology producer
		spam.setSubProcess(spamd.getSubProcessName(), spamd.getSubProcessPath());

		return spam;
	}

	private GroupModel createGroupModel(ProcessModel pm, GroupModelData gmd) {
		if (gmd == null) {
			return null;
		}
		GroupModel gm = new GroupModel(pm, gmd.getId(), gmd.getName(), gmd.getType());
		gm.setConfig(gmd.getConfig());
		gm.setHeight(gmd.getHeight());
		gm.setMapping(gmd.getMapping());
		gm.setStarter(gmd.isStarter());
		gm.setWidth(gmd.getWidth());
		gm.setX(gmd.getX());
		gm.setY(gmd.getY());
		
		return gm;
	}
	
	private TransitionModel createTransitionModel(ProcessModelData pmd, ProcessModel pm, ActivityModelData from, ActivityModelData to) {
		String fromActivityModelId = from.getId();
		String toActivityModelId = to.getId();
		String transitionModelId = IdUtil.getTransitionModelId(fromActivityModelId, toActivityModelId);
		TransitionModel tm = pm.getTransition(transitionModelId);  
		if (tm == null) {
			TransitionModelData tmd = pmd.getTransitionModel(transitionModelId);
			tm = new TransitionModel(pm, transitionModelId, transitionModelId);
			tm.setFromActivity(pm.getActivity(fromActivityModelId));
			tm.setToActivity(pm.getActivity(toActivityModelId));
			tm.setConfig(tmd.getConfig());
			if (tmd.getParent() != null) {
				String parentId = tmd.getParent().getId();
				GroupModel gm = (GroupModel)pm.getActivity(parentId);
				tm.setParent(gm);
			}
			pm.addTransition(tm);
		} 
		return tm;
	}
	
}
