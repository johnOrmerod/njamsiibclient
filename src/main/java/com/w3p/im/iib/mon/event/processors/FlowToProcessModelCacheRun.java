package com.w3p.im.iib.mon.event.processors;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.REGEX_DOT;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.common.Path;
import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.SubProcessActivityModel;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.SubflowMonitoringEvent;


public class FlowToProcessModelCacheRun implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger logger = LoggerFactory.getLogger(FlowToProcessModelCacheRun.class);
	
	private final Hashtable<Path, ProcessModel> allProcessModels = new Hashtable<>();	
	private final Hashtable<String, ProcessModel> flowToProcessModel = new Hashtable<>(); 
	private final Hashtable<String, Path> flowToPath = new Hashtable<>();   // msg/subFlow | ProcessModel name : PM Path
	private final Hashtable<String, String> flowToName = new Hashtable<>(); // msg/sub Flow name : PM Name


	/**
	 * Used for <b>subflows</b> where the ProcessModel is not available at creation time.
	 * <br>It will found on first use at runtime and cached.
	 * @param flowName
	 * @param processPathParts
	 */
	public void addPathForFlow(String flowName, List<String> processPathParts) {
		flowToPath.put(flowName, new Path(processPathParts));
		flowToName.put(flowName, processPathParts.get(processPathParts.size()-1));
	}

	/**
	 * Used for <b>msgflows</b> where the ProcessModel is available at creation time.
	 * @param flowName is either: Msgflow or Msgflow.Subflow
	 * @param pm
	 */
	public void addPathForFlow(String flowName, ProcessModel pm) {
		flowToProcessModel.put(flowName, pm);
	}

	/**
	 * For <b>subfkows</b>, this will return null on first use - then cache it using #addProcessModel() 
	 * @param searchFlowName
	 * @return
	 */
	public ProcessModel getProcessModelForFlow(String searchFlowName) {
		return flowToProcessModel.get(searchFlowName);
	}
	
	private synchronized void addProcessModel(String flowName, ProcessModel pm) {
		flowToProcessModel.put(flowName, pm);
		flowToPath.computeIfAbsent(flowName, k -> pm.getPath());
	}
	
	public Path getPathForFlow(String flowName) {
		return flowToPath.get(flowName);
	}
	
	public String getProcessModelNameForFlow(String flowName) {
		return flowToName.get(flowName);
	}

	/**
	 * Replace any ProcessModels with a null value with the process model from all PMs in nJAMS client.<br>
	 * This is limited to a two-part key: msgflow.subflow and subflow.subflow.
	 *  
	 * @param allPMs
	 * @param clientPath
	 */
	public void completeTheCache(Collection<ProcessModel> allPMs, Path clientPath) {
		List<String> clientPathParts = clientPath.getParts();		
		for (ProcessModel pm : allPMs) {
			allProcessModels.put(pm.getPath(), pm);
		}
		
		Set<String> cachedFlowNames = flowToPath.keySet(); //flowToProcessModel.keySet();
		for (String cachedFlowName : cachedFlowNames) { // Resolve all null PMs in the cache
			Path relativePath = flowToPath.get(cachedFlowName);
			Path fullPath = relativePath; //clientPath.add(relativePath);
			ProcessModel cachedPM = allProcessModels.get(fullPath); 
			if (cachedPM == null) {
				List<String> fullPathParts = new ArrayList<>(clientPathParts);
				fullPathParts.addAll(relativePath.getParts());
				fullPath = new Path(fullPathParts);
				ProcessModel targetPM = allProcessModels.get(fullPath);
				if (targetPM != null) {
					addProcessModel(cachedFlowName, targetPM); //flowToProcessModel.put(cachedFlowName, targetPM);
					addPathForFlow(cachedFlowName, targetPM);
				}
			} else {
				addPathForFlow(cachedFlowName, cachedPM);
			}
		}
	}
	
	public ProcessModel findProcessModel(MonitoringEvent monEvent) { 
		
		String parentFlowName = monEvent.getFlowParentName();  // This is the msg flow name
		String flowName = monEvent.isFromSubFlow() ? monEvent.getSubflowName() : monEvent.getBaseMessageFlow();  //the subflow name is the subflow-name for the node
		String searchFlowName = null;
		if (monEvent instanceof SubflowMonitoringEvent) {
			flowName = monEvent.getLocalNodeLabel();
			if (monEvent.isFromSubFlow()) {
				searchFlowName = parentFlowName+DOT+monEvent.getNodeLabel(); //parentFlowName+DOT+monEvent.getLocalNodeLabel();    //monEvent.getSubflowName();			
			} else {
				searchFlowName = flowName; // includes any schema name
			}
		} else {
			if (monEvent.isFromSubFlow()) {
				searchFlowName = parentFlowName+DOT+monEvent.getSubflowName();    //monEvent.getSubflowName();			
			} else {
				searchFlowName = flowName; // includes any schema name
			}
		}
		
		ProcessModel processModel =  getProcessModelForFlow(searchFlowName);
		if (processModel != null) {
			return processModel;
		}

		synchronized (searchFlowName) {
			String[] searchFlowNameParts = searchFlowName.split(REGEX_DOT);
			logger.warn("ProcessModel not found for subflow '{}' with parent path '{}' in the FlowToProcessModelCache. ", searchFlowName, parentFlowName);	
			String[] parentFlowNameParts = parentFlowName.split(REGEX_DOT); //parentFlowNameParts = parentFlowName.split(REGEX_DOT);;
			int parentFlowNamePartsSize = parentFlowNameParts.length -1; // Remove 1 to use as index
			
			ProcessModel subflowPM = null;
			
			for (int parentPart = 1; parentPart <= parentFlowNamePartsSize; parentPart++) { //   parentFlowNameParts.length; parentPart++) {
				StringBuilder sbParentFlowName = new StringBuilder(parentFlowNameParts[0]);
				for (int i = 1; i <= parentPart; i++) {
					sbParentFlowName.append(DOT).append(parentFlowNameParts[i]);					
				}
				ProcessModel parentPM = getProcessModelForFlow(sbParentFlowName.toString()); // This was created in the previous iteration  
				
				if (parentPM != null) {
					// The current event is > 1 level - we have to find the PM for the parent's parent'd parent...
					// Process 1 element of the parentFlowName at a time until the right process model is found
					String targetFlowName = searchFlowNameParts[parentPart + 1]; //StringUtils.substringAfterLast(searchFlowName, DOT); // FIXME this is for jump = 1 
					StringBuilder sbSearchFlowName = new StringBuilder(searchFlowNameParts[0]);
					for (int i = 1; i <= parentPart + 1; i++) {
						sbSearchFlowName.append(DOT).append(searchFlowNameParts[i]);					
					}
					subflowPM = findProcessModelForSubflow(targetFlowName, sbSearchFlowName.toString(), parentPM);
					if (subflowPM != null) {
						logger.info("ProcessModel found for subflow '{}' with parent path '{}' and added to the FlowToProcessModelCache. ", searchFlowName, parentFlowName);
					} else {
						logger.error("ProcessModel not found - after a search - for subflow '{}' with parent path '{}' in the FlowToProcessModelCache. ", searchFlowName, parentFlowName);
					}
				} else {
					logger.error("ProcessModel not found for subflow '{}' with parent path '{}' in the FlowToProcessModelCache. ", searchFlowName, parentFlowName);
					return null; // TODO add a throw
				}
			}
				return subflowPM;
		}
	}

	private ProcessModel findProcessModelForSubflow(String flowName, String searchFlowName,	ProcessModel parentPM) {
		List<ActivityModel> parentActivites = parentPM.getActivityModels(); // Activity = IIB Node
		for (ActivityModel am : parentActivites) {
			if (am instanceof SubProcessActivityModel) {
				if (am.getName().equals(flowName)) {   // NOTE this is the aubflow name in the Toolkit - might not be the name of the actuakl subflow 
					Path subProcessPath = ((SubProcessActivityModel) am).getSubProcessPath();
					ProcessModel matchedPM = allProcessModels.get(subProcessPath);
					addProcessModel(searchFlowName, matchedPM);
					return matchedPM;
				}
			}
		}
		return null;
	}

	public void setSubprocessModels() {		// This sets the actual sub-proc in the proc model, so that cloud server works
		for (String cachedFlowName : flowToProcessModel.keySet()) { // Cached PMs
			ProcessModel cachedPM = flowToProcessModel.get(cachedFlowName); 
			if (cachedPM != null) {
				for (ActivityModel cachedAM : cachedPM.getActivityModels()) {
					if (cachedAM instanceof SubProcessActivityModel) {
						// Get the process model using Path
						// 	e.g >W3P_NJAMS_DEMO>appTesting>JOT420-16>HTTPInputApplication>SF_HTTPInput>
						Path subProcessPath = ((SubProcessActivityModel)cachedAM).getSubProcessPath();
						ProcessModel sfpm = allProcessModels.get(subProcessPath);
						if (sfpm != null) {
							((SubProcessActivityModel)cachedAM).setSubProcess(sfpm);
						}
					}
				}
			} 				
		}
	}

	public boolean isEmpty() {
		return flowToPath.isEmpty();
	}
}
