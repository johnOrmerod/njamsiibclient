package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class NJAMSTopologyCreationException extends InternalException {


	private static final long serialVersionUID = 1L;

	public NJAMSTopologyCreationException() {
		super();
	}

	public NJAMSTopologyCreationException(String msg, Object... values) {
		super(msg, values);
	}
}
