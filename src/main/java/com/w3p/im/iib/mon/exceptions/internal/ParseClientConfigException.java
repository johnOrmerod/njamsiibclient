package com.w3p.im.iib.mon.exceptions.internal;

import com.w3p.im.iib.mon.exceptions.InternalException;

public class ParseClientConfigException extends InternalException {


	private static final long serialVersionUID = 1L;

	public ParseClientConfigException() {
		super();
	}

	public ParseClientConfigException(String msg, Object... values) {
		super(msg, values);
	}
}
