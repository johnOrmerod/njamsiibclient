package com.w3p.im.iib.mon.exceptions.monitoring;

import com.w3p.im.iib.mon.exceptions.ApplicationException;

public class MonitoringEventParserException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public MonitoringEventParserException() {
		super();
	}

	public MonitoringEventParserException(String msg, Object... values) {
		super(msg, values);
	}
}
