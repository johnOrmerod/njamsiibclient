package com.w3p.im.iib.mon.exceptions.monitoring;

import com.w3p.im.iib.mon.exceptions.ApplicationException;

public class MonitoringException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	
	public MonitoringException() {
	}

	public MonitoringException(String msg, Object... values) {
		super(msg, values);
	}
}
