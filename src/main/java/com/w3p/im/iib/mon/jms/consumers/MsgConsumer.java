package com.w3p.im.iib.mon.jms.consumers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.*;

import java.io.UnsupportedEncodingException;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.data.IMessageConsumer;
import com.w3p.im.iib.mon.jms.service.JMSConnector;



public abstract class MsgConsumer extends JMSConnector implements IMessageConsumer {
	private static final Logger logger = LoggerFactory.getLogger(MsgConsumer.class);
	
	protected String messageAsString;
	protected MessageConsumer jmsConsumer;	

	
	@Override
	public void consumeMsgsFromDestination(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException {
		super.context = context;
		super.connection = connection;
		super.isTransacted = isTransacted;
		consumeMsgsFromDestination(dest, isTransacted);
	}

	/**
	 * @param jndiDestName
	 * @param transacted
	 * @throws JMSException 
	 */
	protected void consumeMsgsFromDestination(String dest, boolean isTransacted) throws JMSException {
		logger.trace("start");
		logger.debug("consumeMsgsFromDestination() for jmsDest '{}'", dest);	
		// A Destination is thread-safe, though we are using it at the instance level
		createSession(connection, isTransacted); // Create transacted receiver session
		Destination inDest = getDestination(dest); // Get the input destination (queue or topic)
		jmsConsumer = createJmsConsumer(connection, inDest); // Create a JMS MessageConsumer (queue or topic).
		logger.debug("#consumeMsgsFromDestination() Event Processor started for jmsDest '{}'", inDest);			
		logger.trace("end");
	}

	
	/**
	 * Used to terminate a dynamic topic subscription
	 * @throws JMSException 
	 */
	@Override
	public void closeSession() throws JMSException {
		logger.trace("start");
		session.close();
		logger.trace("end");
	}


	/**
	 * @param inMsg
	 * @return
	 * @throws MessagingException 
	 */
	public String extractMessageAsString(Message inMsg) throws MessagingException {
		String messageString = "";
		try {
			if (inMsg instanceof TextMessage) {
				logger.trace("message is a text msg");
				messageString = ((TextMessage) inMsg).getText();				
			} else if (inMsg instanceof BytesMessage) {
				logger.trace("message is a bytes msg");
				BytesMessage bytesMsg = (BytesMessage) inMsg;
				bytesMsg.reset(); // TODO use for debug with hot method replace
				int msgLength = new Long(bytesMsg.getBodyLength()).intValue();
				byte[] msgBytes = new byte[msgLength];
				bytesMsg.readBytes(msgBytes);
				try {
					messageString = new String(msgBytes,UTF_8);
				} catch (UnsupportedEncodingException e) {
					logger.error("Error processing the JMS message. Cause: '{}'.", e.toString(), e);
					throw new MessagingException("Error processing the JMS message. Cause: '{}'.", e.toString(), e);
				}				
			} else {
				String msgType = inMsg.getJMSType();
				throw new MessagingException("Error - unexpected message Type received. Msg type: '{}'.", msgType);
			}
		} catch (JMSException e) {
			try {
				// FIXME write to error queue
				// Undo any messages sent as part of the processing
				rollback(); 
			} catch (JMSException e1) {
				logger.error("Exception in 'rollback'. No action to take. Cause: '{}'.", e.toString(), e);
			}			
			throw new MessagingException("JMS exception. Cause: ''{0}''.", e.toString(), e);
		} finally {
			if (isTransacted) {
				try {
					commit();
				} catch (JMSException e) {
					logger.error("Exception in 'finally' block when committing the JMS message. No action to take. Cause: '{}'.", e.toString(), e);
				}
			}
		}
		
		return messageString;
	}

}