package com.w3p.im.iib.mon.jms.data;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.naming.Context;


public interface IMessageConsumer extends MessageListener {	

	/**
	 * @param context
	 * @param connection
	 * @param dest
	 * @param isTransacted
	 * @throws Exception
	 */
	void consumeMsgsFromDestination(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException;
	
	void setMessagingService(IMessagingServiceProducer messagingService);
	
	void closeSession() throws JMSException;

}
