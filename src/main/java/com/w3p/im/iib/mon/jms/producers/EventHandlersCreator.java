package com.w3p.im.iib.mon.jms.producers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.EVENT_TOPIC_PATTERN;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.EVENT_TOPIC_PATTERN_MQTT;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.event.processors.MonitorEventMessagesHandler;
import com.w3p.im.iib.mon.exceptions.internal.CreateMonitoringEventHandlerException;
import com.w3p.im.iib.mon.monitor.data.EventHandlingRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequest;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;

public class EventHandlersCreator {
	private static final Logger logger = LoggerFactory.getLogger(EventHandlersCreator.class);
	
	protected EventHandlingRequest request;	
	
	
	public EventHandlersCreator(EventHandlingRequest request) {
		this.request = request;
	}
	
	/**
	 * For getting events from a queue
	 * @throws CreateMonitoringEventHandlerException
	 */
	public void createEventsHandlers() throws CreateMonitoringEventHandlerException {
		logger.trace("#createEventHandlers: entry");
		MonitorEventsRequestForQueues mer = createMonitorEventsRequest(request);
		createEventMessagesHandler(mer);		
		logger.trace("#createEventHandlers: exit");
	}

	protected MonitorEventsRequestForQueues createMonitorEventsRequest(EventHandlingRequest request) throws CreateMonitoringEventHandlerException {
		MonitorEventsRequestForQueues mer = new MonitorEventsRequestForQueues(request.getNjamsClients(), request.getIntNodeName()); //, request.getIntSvrName()); //request.getNjamsClient(), request.getIntNodeName(), request.getIntSvrName());
		mer.setMqJmsConnectionProperties(request.getMqJmsConnectionProperties())
			.setEventsSource(request.getEventsSource());
		mer.setIibEventsProblemsQueue(request.getIibEventsProblemsQueue())
			.setFlowToProcessModelCaches(request.getFlowToProcessModelCaches())
			.setConsumerThreads(request.getConsumerThreads());

		return mer;
	}

	protected void createEventMessagesHandler(MonitorEventsRequestForQueues mer) throws CreateMonitoringEventHandlerException {
		String eventsSource = mer.getEventsSource();
		MonitorEventMessagesHandler monitorEventMessagesHandler = new MonitorEventMessagesHandler(mer);
		monitorEventMessagesHandler.handleMessagesForEventsSource(eventsSource);
	}

//	private String formatTopicString(MonitorEventsRequestForTopics mer) {
//		String topicString = null;
//		if (mer.isUseMQTT()) {
//			topicString = MessageFormat.format(EVENT_TOPIC_PATTERN_MQTT, mer.getIntegrationNodeName(), mer.getIntegrationServerName(), mer.getFlowName());
//		} else if (mer.isUseTopic()) {
//			topicString = MessageFormat.format(EVENT_TOPIC_PATTERN_IIB, mer.getIntegrationNodeName(), mer.getIntegrationServerName(), mer.getFlowName());
//		}		
//		logger.info("#createEventMessagesHandler(): Events source = '{}'.", topicString);
//		return topicString;
//	}
}
