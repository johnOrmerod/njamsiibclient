package com.w3p.im.iib.mon.jms.producers;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.api.config.proxy.ExecutionGroupProxy;
import com.w3p.im.iib.mon.client.config.data.Application;
import com.w3p.im.iib.mon.client.config.data.IDeployableObject;
import com.w3p.im.iib.mon.client.config.data.IntegrationServer;
import com.w3p.im.iib.mon.client.config.data.Library;
import com.w3p.im.iib.mon.client.config.data.MsgFlow;
import com.w3p.im.iib.mon.client.config.data.RestApi;
import com.w3p.im.iib.mon.client.config.data.Service;
import com.w3p.im.iib.mon.client.config.data.SharedLibrary;
import com.w3p.im.iib.mon.client.config.data.StaticLibrary;
import com.w3p.im.iib.mon.client.config.data.SubFlow;
import com.w3p.im.iib.mon.monitor.data.MonitoredIntegrationNode;
import com.w3p.im.iib.mon.monitor.data.MonitoredIntegrationServer;
import com.w3p.im.iib.mon.monitor.data.MonitoredMessageFlow;
import com.w3p.im.iib.mon.monitor.data.MonitoredObject;
import com.w3p.im.iib.mon.monitor.data.MonitoredStaticLibrary;
import com.w3p.im.iib.mon.monitor.data.MonitoredSubFlow;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeRequest;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeResponse;

public class MonitoringScopeCreator {

	private static final Logger logger = LoggerFactory.getLogger(MonitoringScopeCreator.class);

	public  MonitoringScopeResponse createScope(MonitoringScopeRequest request) {

		MonitoringScopeResponse response = null;
		try {
			MonitoredObject<MonitoredObject<?>> monitoringScope = determineScopeForMonitoring(request);
			response = new MonitoringScopeResponse();
			response.setMonitoringScope(monitoringScope);
		} catch (Exception e) {
			if (response != null) {
				logger.error(response.getMessage(), e);
			} else {
				logger.error("resonse is null, exception is '{}'", e.toString(), e);
			}
			setResponseError(response, e);			
		}
		return response;	
	}


	/**
	 * 'protected' to allow JUnit testing of method
	 * @return
	 */
	protected MonitoredObject<MonitoredObject<?>> determineScopeForMonitoring(MonitoringScopeRequest request) {

		MonitoredIntegrationNode monIntNode = new MonitoredIntegrationNode(request.getIntegrationNodeName());
		IntegrationServer intSvr = request.getIntSvr();
		String intSvrName = intSvr.getName();
		MonitoredObject<?> mis = monIntNode.getMonitoredChildObjectByName(ExecutionGroupProxy.class, intSvrName);
		if (mis == null) {
			mis = new MonitoredIntegrationServer(intSvrName);
		}
		monIntNode.addMonitoredChildObject(mis);
		// Find Applications to monitor
		for (Application app : intSvr.getApplications()) {
			findTypesToMonitor(monIntNode, intSvrName, app, 
					"com.w3p.api.config.proxy.ApplicationProxy", "com.w3p.im.iib.mon.monitor.data.MonitoredApplication");
		}
		// Find any RestAPIs with flows to monitor
		for (RestApi restApi : intSvr.getRestAPIs()) {
			findTypesToMonitor(monIntNode, intSvrName, restApi, 
					"com.w3p.api.config.proxy.RestApiProxy", "com.w3p.im.iib.mon.monitor.data.MonitoredRestApi");
		}
		// Find any Services with flows to monitor
		for (Service service : intSvr.getServices()) {
			findTypesToMonitor(monIntNode, intSvrName, service, 
					"com.w3p.api.config.proxy.ServiceProxy", "com.w3p.im.iib.mon.monitor.data.MonitoredService");
		}
		// Find Shared Libraries with flows to monitor
		for (SharedLibrary shLib : intSvr.getSharedLibs()) {
			findTypesToMonitor(monIntNode, intSvrName, shLib,
					"com.w3p.api.config.proxy.SharedLibraryProxy", "com.w3p.im.iib.mon.monitor.data.MonitoredSharedLibrary");
		}

		return monIntNode;		
	}

	private void findTypesToMonitor(MonitoredIntegrationNode monIntNode, String intSvrName, IDeployableObject dep, String childProxyName, String monitoredTypeName) {
		findTypesToMonitor(monIntNode, intSvrName, null, dep, childProxyName, monitoredTypeName);
	}
	@SuppressWarnings("unchecked")
	private void findTypesToMonitor(MonitoredIntegrationNode monIntNode, String intSvrName, MonitoredObject<MonitoredObject<?>> parentMonType, IDeployableObject dep, String childProxyName, String monitoredTypeName) {
		/* 'parentCo' is only used for a Static Lib, which is a child of its App / RestApi / service */
		// Get available IIB Apps or Libraries or RestAPis: type is determined by 'childProxyName'
		// The flows to be monitored are in the lists of msg snd sbub flows 
		Class<?> childProxy = null;
		Class<?> monitoredType = null;
		try {
			childProxy = Class.forName(childProxyName);
			monitoredType = Class.forName(monitoredTypeName);
		} catch (ClassNotFoundException e) {
			logger.error(e.toString(), e); // FIXME Logging error not good enough
		}
		String configObjectName = dep.getName();
		
		// Only add Types to an existing MonitoredIntegrationServer
		MonitoredIntegrationServer mis = (MonitoredIntegrationServer) monIntNode.getMonitoredChildObjectByName(ExecutionGroupProxy.class, intSvrName);
		
		MonitoredObject<MonitoredObject<?>> monType = null;
		if (dep.getClass().isAssignableFrom(StaticLibrary.class)) {
			monType = (MonitoredObject<MonitoredObject<?>>) parentMonType.getMonitoredChildObjectByName(childProxy, configObjectName);
		} else {		
			monType = (MonitoredObject<MonitoredObject<?>>) mis.getMonitoredChildObjectByName(childProxy, configObjectName);
		}
		if (monType == null) {
			try {
				monType = (MonitoredObject<MonitoredObject<?>>) monitoredType.getDeclaredConstructor(String.class).newInstance(configObjectName);
				monType.setIncludeSchemaName(dep.isIncludeSchemaName());
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				logger.error(e.toString(), e); // FIXME Logging error not good enough
			}
		}
		
		mis.addMonitoredChildObject(monType);
		
		for (MsgFlow msgFlow : dep.getMsgFlows()) {
			String flowName = msgFlow.getName();
			if (!dep.isIncludeSchemaName() && StringUtils.contains(flowName, ".")) {
				flowName =  StringUtils.substringAfterLast(flowName, ".");
			} 
			MonitoredMessageFlow mmf = new MonitoredMessageFlow(flowName);
			mmf.setIncludeSchemaName(dep.isIncludeSchemaName()); // Value has to be from parent (e.g Application) so that all msgflows have same value 
			mmf.setIncludePayload(msgFlow.isIncludePayload());
			mmf.setLabelNodeStartsGroup(msgFlow.isLabelNodeStartsGroup());
			monType.addMonitoredChildObject(mmf);
			if (dep.getClass().isAssignableFrom(StaticLibrary.class)) { //run instanceof StaticLibrary) { 
				mis.addMonitoredChildObject(monType); // FIXME move this before these loops - monType is not changed
			}
		}

		// Repeat for SubFlows TODO There must be a way to avoid this almost duplicated code?
		for (SubFlow subFlow : dep.getSubFlows()) {	
			String flowName = subFlow.getName();
			if (!dep.isIncludeSchemaName() && StringUtils.contains(flowName, ".")) {
				flowName =  StringUtils.substringAfterLast(flowName, ".");
			}
			MonitoredSubFlow msf = new MonitoredSubFlow(flowName);
			msf.setIncludeSchemaName(dep.isIncludeSchemaName());
			msf.setIncludePayload(subFlow.isIncludePayload());
			msf.setLabelNodeStartsGroup(subFlow.isLabelNodeStartsGroup());
			monType.addMonitoredChildObject(msf);
			if (!dep.getClass().isAssignableFrom(StaticLibrary.class)) {
				mis.addMonitoredChildObject(monType);
			}
		}
		
		// Include any Static Libs - exclude for Library types - would be better to implement as some sort of Library subclass that omits this search 
		if (!(dep instanceof Library)) {
			for (StaticLibrary staticLib : dep.getStaticLibs()) {
				String libName = staticLib.getName(); // This always returns 'name = *' to ensure that all static libs for this runnable are included
				if (!dep.isIncludeSchemaName() && StringUtils.contains(libName, ".")) {
					libName =  StringUtils.substringAfterLast(libName, ".");
				}
				MonitoredStaticLibrary msl = new MonitoredStaticLibrary(libName);
				msl.setIncludeSchemaName(dep.isIncludeSchemaName());
				monType.addMonitoredChildObject(msl);
				// Forces a msgflow and a subflow to be monitored with name = '*'
				findTypesToMonitor(monIntNode, intSvrName, monType, staticLib,
						"com.w3p.api.config.proxy.StaticLibraryProxy", "com.w3p.im.iib.mon.monitor.data.MonitoredStaticLibrary");
			}
		}
	}

	private void setResponseError(MonitoringScopeResponse response, Exception e) {
		response.setSuccess(false);
		response.setRetCode(-1);
		response.setMessage( "Error when establishing the scope for Topology to be monitored.\n  reason: "+e.toString());
	}
}
