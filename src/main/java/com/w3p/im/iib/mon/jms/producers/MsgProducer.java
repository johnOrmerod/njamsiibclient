package com.w3p.im.iib.mon.jms.producers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.nio.charset.Charset;
import java.util.Map;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;
import javax.naming.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.jms.data.IMessageProducer;
import com.w3p.im.iib.mon.jms.service.JMSConnector;

public class MsgProducer extends JMSConnector implements IMessageProducer {
	private static final Logger logger = LoggerFactory.getLogger(MsgProducer.class);
	
	protected MessageProducer jmsProducer;

	public MsgProducer() {
	}
	
	@Override
	public void createMessageProducer(Context context, Connection connection, String dest, boolean isTransacted) throws JMSException {
		super.context = context;
		super.connection = connection;
		super.isTransacted = isTransacted;
		createMessageProducer(dest, isTransacted);
	}

	protected void createMessageProducer(String dest, boolean isTransacted) throws JMSException {
		logger.trace("start");
		logger.debug("createProduce() for jmsDest '{}'", dest);	
		// A Destination is thread-safe, though we are using it at the instance level
		createSession(connection, isTransacted); // Create transacted receiver session
		Destination outDest = getDestination(dest); // Get the output destination (queue or topic)
		jmsProducer = createJmsProducer(connection, outDest); // Create a JMS MessageProducer (queue or topic).
		logger.debug("#createProducer() Message Producer started for Dest '{}'", dest);			
		logger.trace("end");
	}	
	
	@Override
	public BytesMessage createBytesMessage(String msgContent) throws JMSException {
		BytesMessage message = session.createBytesMessage();
		byte[] msgAsBytes = msgContent.getBytes(Charset.forName(UTF_8));
		message.writeBytes(msgAsBytes);
		return message;
	}
	
	@Override
	public TextMessage createTextMessage(String msgContent) throws JMSException {
		TextMessage message = session.createTextMessage(); // throws JMSRuntimeException		
		// UTF-8 This is needed to ensure that special characters. such as 'TM' superscript are handled.
		message.setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
		message.setText(msgContent);
		return message;
	}
	
	@Override
	public Message setMessageProperties(Message message, Map<String, String> msgProperties) throws JMSException {
		for (String name : msgProperties.keySet()) {
			message.setStringProperty(name, msgProperties.get(name));
		}		
		return message;
	}

	@Override
	public void closeSession() throws JMSException {
		// TODO Auto-generated method stub
		
	}

}
