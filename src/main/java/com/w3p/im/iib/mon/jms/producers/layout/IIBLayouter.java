package com.w3p.im.iib.mon.jms.producers.layout;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.model.ActivityModel;
import com.im.njams.sdk.model.GroupModel;
import com.im.njams.sdk.model.ProcessModel;
import com.im.njams.sdk.model.TransitionModel;
import com.im.njams.sdk.model.layout.ProcessModelLayouter;

public class IIBLayouter implements ProcessModelLayouter {
	
   private static final Logger logger = LoggerFactory.getLogger(IIBLayouter.class);
	
   	private static final String END_ACTIVITY_NAME = "End";
   	
    private final int ACTIVITY_HORIZONTAL_OFFSET;
    private final int ACTIVITY_VERTICAL_OFFSET;
    private final int START_HORIZONTAL_OFFSET;
    private final int START_VERTICAL_OFFSET;
    
    private final List<GroupModel> groupModels = new ArrayList<>();
    // fromNode:toNode  to prevent layout loops, when a latrer node has a connection back to a preceding noide in the connection path
    private final List<String> transitions = new ArrayList<>();   

    public IIBLayouter() {
    	this(150, 100, 0, 0);
	}

    public IIBLayouter(int activityHorizontalOffset, int activityVerticalOffset, int startHorizontalOffset, int startVerticalOffset) {
    	ACTIVITY_HORIZONTAL_OFFSET = activityHorizontalOffset;
    	ACTIVITY_VERTICAL_OFFSET = activityVerticalOffset;
    	START_HORIZONTAL_OFFSET = startHorizontalOffset;
    	START_VERTICAL_OFFSET = startVerticalOffset;
 	}

    
	/**
     * Layouts the given ProcessModel
     *
     * @param processModel processModel
     */
    @Override
    public void layout(ProcessModel processModel) {
    	try {
    		transitions.clear();
    		List<ActivityModel> startActivityies = processModel.getStartActivities();

    		if (startActivityies != null && startActivityies.size() > 1) {
    			logger.warn("{}: SimpleProcessModelLayouter does not support "
    					+ "ProcessModels with more than one start activity", processModel.getPath());
    		}

    		if (startActivityies != null && !startActivityies.isEmpty()) {
    			ActivityModel startActivity = startActivityies.get(0);
    			setPositionOfStartActivity(startActivity);
    			List<ActivityModel> successors = startActivity.getSuccessors();
    			if (successors == null) {
    				return;
    			}
    			positionSuccessors(startActivity, 0);
    		}

    		reformatDiagramLayout(processModel); // Uncovers non-group nodes hidden by depth-first layout

    	} catch (Exception e) {
    		logger.error(e.toString(), e);    		
    	}
    }

    private void positionSuccessors(ActivityModel activity, int groupModelOffset) {
        int yoffset = groupModelOffset == 0 ? 0 : groupModelOffset;
        // depth-first algorithm
        List<ActivityModel> successors = activity.getSuccessors();
        for (ActivityModel element : successors) {
            if (activity instanceof GroupModel) {
                element.setX(activity.getX() + ((GroupModel) activity).getWidth() + ACTIVITY_HORIZONTAL_OFFSET / 2);
                element.setY(activity.getY() + yoffset);
                yoffset += ((GroupModel) activity).getHeight() + ACTIVITY_VERTICAL_OFFSET;
            } else {
            	String transition = activity.getName()+":"+element.getName();
            	if (!transitions.contains(transition)) { 
            		element.setX(activity.getX() + ACTIVITY_HORIZONTAL_OFFSET);
            		element.setY(activity.getY() + yoffset);
            		yoffset += ACTIVITY_VERTICAL_OFFSET;
            		transitions.add(transition);                
             	} else {
            		logger.warn("Transition '{}' has already been processed. Stopping the layout for node '{}' to prevent an endless loop in the layout.", transition, activity.getName());
            		return; // Prevent an endless loop in the layout
            	}
            }
            // next step is a group
            if (element instanceof GroupModel && !((GroupModel) element).getStartActivities().isEmpty()) {
                ((GroupModel) element).setWidth(0);
                ((GroupModel) element).setHeight(0);
                if (((GroupModel) element).getStartActivities().size() > 1) {
                    logger.warn("{}: SimpleProcessModelLayouter does not support "
                            + "GroupModels with more than one start activity", element.getId());
                }
                ActivityModel groupStarter = ((GroupModel) element).getStartActivities().get(0);
                groupStarter.setX(element.getX() + ACTIVITY_HORIZONTAL_OFFSET / 2);
                groupStarter.setY(element.getY() + ACTIVITY_VERTICAL_OFFSET / 2);
                positionSuccessors(groupStarter, groupModelOffset);
            }
            resizeParent((ActivityModel) element);
        }

        for (ActivityModel element : successors) {
            positionSuccessors(element, groupModelOffset);
        }

    }

    private void resizeParent(ActivityModel element) {
        if (element.getParent() == null) {
            // nothing to do
            return;
        }
        GroupModel parent = (GroupModel) element.getParent();

        if (element instanceof GroupModel) {
            parent.setWidth(((GroupModel) element).getWidth() + ACTIVITY_HORIZONTAL_OFFSET);
            parent.setHeight(((GroupModel) element).getHeight() + ACTIVITY_VERTICAL_OFFSET);
        } else {
            if (element.getX() + ACTIVITY_HORIZONTAL_OFFSET > (parent.getX() + parent.getWidth())) {
                parent.setWidth(element.getX() - parent.getX() + ACTIVITY_HORIZONTAL_OFFSET);
             }
            if (element.getY() + ACTIVITY_VERTICAL_OFFSET > (parent.getY() + parent.getHeight())) {
                parent.setHeight(element.getY() - parent.getY() + ACTIVITY_VERTICAL_OFFSET);
             }
        }

        // resize grandparents, etc
        resizeParent(parent);
    }

    private void setPositionOfStartActivity(ActivityModel activity) {
        activity.setX(START_HORIZONTAL_OFFSET);
        activity.setY(START_VERTICAL_OFFSET);
    }
    
	protected void reformatDiagramLayout(ProcessModel pm) {
		List<Integer> yValues = new ArrayList<>();
		boolean duplicateCoordinatesMightExist = true;
		while (duplicateCoordinatesMightExist) {
			boolean foundDuplicate = false;
			Set<String> coordinates = new HashSet<>();
			List<ActivityModel> activities = pm.getActivityModels();
			ActivityModel endActivity = null;
			int maxX = 0;
	
			for (ActivityModel am : activities) {
				if (!yValues.contains(am.getY())) {
					yValues.add(am.getY()); // find out the y values used
				}	
				if (am.getName().equals(END_ACTIVITY_NAME)) {
					endActivity = am;
				}
				String coordinate = String.valueOf(am.getX())+","+String.valueOf(am.getY());
				if (coordinates.contains(coordinate)) {
					am.setY(am.getY()+ACTIVITY_VERTICAL_OFFSET);  // Duplicate coordinate - move the node down
					foundDuplicate = true;
				} else if (am instanceof GroupModel) {
					groupModels.add((GroupModel)am);
				} else {
					coordinates.add(coordinate);
				}
				
				if (am instanceof GroupModel) {
					if (am.getX() + ((GroupModel)am).getWidth() > maxX) {
						maxX = ((GroupModel)am).getWidth();
					}
				}
				else if (am.getX() > maxX) {
					maxX = am.getX();
				}
				
				if (endActivity != null && endActivity.getX() < maxX) {
					endActivity.setX(maxX+ACTIVITY_HORIZONTAL_OFFSET); // TODO increase Y value?
				}
			}
			
			// Do any groups overlay nodes for a given 'y' value. If they do, then move the Group down
			for (int i = groupModels.size()-1; i >= 0; i--) {
				GroupModel gm = groupModels.get(i);
				if (yValues.contains(gm.getY())) {
					// We need to move the group down 'i' times vertical-offset
					moveGroupAndChildrenDown(i, gm);
				}
			}
			
			if (!foundDuplicate) {
				duplicateCoordinatesMightExist = false;
			}
		}

		// Move any target nodes where connections overlay each other
		boolean overlaidConnectionsMightExist = true;
		while (overlaidConnectionsMightExist) {
			List<ActivityModel> nodesWithMultipleSources  = new ArrayList<>();
			List<ActivityModel> toNodes = new ArrayList<>();
			List<TransitionModel> transitions = pm.getTransitionModels();
			for (TransitionModel tm : transitions) {
				ActivityModel amTo = pm.getActivity(tm.getToActivity().getId());
				if (amTo != null) { // Somehow when a Group is present, amTo can be null
					if (toNodes.contains(amTo)) {
						if (!nodesWithMultipleSources.contains(amTo)) {
							nodesWithMultipleSources.add(amTo);
						}
					} else {
						toNodes.add(amTo);
					}
				}
			}
			
			// Process nodesWithMultipleSources to check for their sources having same y coord
			List<ActivityModel> nodesToMoveDown = new ArrayList<>();
			Set<Integer> yValuesFrom = new HashSet<>();
			for (ActivityModel nodeWithMultipleSources : nodesWithMultipleSources) {
				for (TransitionModel tm : transitions) {
					if (tm.getToActivity().getName().equals(nodeWithMultipleSources.getName())) {
						// Found one of the transitions to this node
						ActivityModel amFrom = pm.getActivity(tm.getFromActivity().getId());
						if (amFrom != null) { // Will be null if tm.from is a  Group
							if (nodeWithMultipleSources.getY() == amFrom.getY()) {
								if (!yValuesFrom.add(amFrom.getY())) {
									nodesToMoveDown.add(nodeWithMultipleSources);	// More than one fromNode has a y-value = that of nodeWithMultipleSources
								}						
							}
						}
					}
				}
			}
			
			// Move nodes down - but only when the moved node doesn't overlay an existing node
			int i = 0;
			for (ActivityModel nodeToMoveDown : nodesToMoveDown) {
				boolean willProduceNewOverlaidNode = true;
				while (willProduceNewOverlaidNode) {
					i++;					
					willProduceNewOverlaidNode = willProduceNewOverlaidNode(pm, nodeToMoveDown, i*ACTIVITY_VERTICAL_OFFSET);
				}
				nodeToMoveDown.setY(nodeToMoveDown.getY() + i*ACTIVITY_VERTICAL_OFFSET);
				yValues.add(nodeToMoveDown.getY());
			}			
			overlaidConnectionsMightExist = !nodesToMoveDown.isEmpty();
		}
		
		// Now move 'End' activity vertically to 'y=0'
		for (ActivityModel am : pm.getActivityModels()) {
			if (am.getName().equals(END_ACTIVITY_NAME)) {
				// find max Y value
				int maxY = 0;
				for (int yValue : yValues) {
					if (yValue > maxY) {
						maxY = yValue;
					}
				}
				if (maxY != 0) {
					am.setY(maxY - (ACTIVITY_VERTICAL_OFFSET/2));
				}
			}
		}
	}

	protected void moveGroupAndChildrenDown(int i, GroupModel gm) {
		int verticalOffset = (i+1) * ACTIVITY_VERTICAL_OFFSET;
		gm.setY(gm.getY()+ verticalOffset);
		// now move down all Group nodes
		gm.getChildActivities();
		for (ActivityModel childActivityModel : gm.getChildActivities()) {
			childActivityModel.setY(childActivityModel.getY() + verticalOffset);						
		}
	}


	private boolean willProduceNewOverlaidNode(ProcessModel pm, ActivityModel nodeToMoveDown, Integer yAdj) {
		Integer newY = nodeToMoveDown.getY() + yAdj;
		String newCoords = String.valueOf(nodeToMoveDown.getX())+","+newY.toString();
		for (ActivityModel am : pm.getActivityModels()) {
			String coordinate = String.valueOf(am.getX())+","+String.valueOf(am.getY());
			if (coordinate.equals(newCoords)) {
				return true;
			}
		}		
		return false;
	}

}
