package com.w3p.im.iib.mon.jms.service;

import com.w3p.im.iib.mon.jms.data.IMessageConsumer;
import com.w3p.im.iib.mon.jms.data.IMessagingServiceConsumer;

public class MessagingServiceConsumer extends MessagingService implements IMessagingServiceConsumer {

	protected IMessageConsumer msgConsumer;
	protected MessagingServiceFactory messagingServiceFactory;


	public MessagingServiceConsumer(MqJmsConnectionProperties connectionProperties) {
		super(connectionProperties);
	}

	public synchronized void addMessageConsumer(String dest, IMessageConsumer msgConsumer) throws Exception {
		addMessageConsumer(dest, msgConsumer, false);
	}

	public synchronized void addMessageConsumer(String dest, IMessageConsumer msgConsumer, boolean isTransacted) throws Exception {
		this.msgConsumer = msgConsumer;
		createConnection(dest);
		msgConsumer.consumeMsgsFromDestination(context, connection, dest, isTransacted);
	}

	public synchronized IMessageConsumer getMessageConsumer() {
		return msgConsumer;
	}
	
	public synchronized void setMessagingServiceFactory(MessagingServiceFactory messagingServiceFactory) {
		this.messagingServiceFactory = messagingServiceFactory;
	}

	public synchronized MessagingServiceFactory getMessagingServiceFactory() {
		return messagingServiceFactory;
	}
}
