package com.w3p.im.iib.mon.monitor.data;

import java.util.HashMap;
import java.util.Map;

public class IncludeSchemaName {
	
	private final Map<String, Boolean> flowMap = new HashMap<>(); // k=Flow name, v=includeSchemaName
	
	public IncludeSchemaName add(String flowName, boolean includeSchemaName) {
		flowMap.put(flowName, includeSchemaName);
		return this;
	}
	
	public boolean isIncludeSchemaName (String flowName) {
		return (flowMap.get(flowName)) == null ? false : flowMap.get(flowName);
	}

	@Override
	public String toString() {
		return "IncludeSchemaName [flowMap=" + flowMap + "]";
	}
}
