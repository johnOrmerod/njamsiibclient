package com.w3p.im.iib.mon.monitor.data;

import com.w3p.api.config.proxy.BrokerProxy;


public class IntNodeConnectionObject {
	
	private String intNodeName; // for Local connection
	private String ipaddress; // or host name
	private int port;
	private String qmgr;
	private BrokerProxy intNode;
	private boolean local;
	

	/**
	 * Broker (Integration Node) name
	 * @return
	 */
	public String getIntNodeName() {
		return intNodeName;
	}
	public void setIntNodeName(String intNodeName) {
		this.intNodeName = intNodeName;
	}
	/**
	 * ip address of remote Broker (empty String = use MQ Java Bindings)
	 * @return
	 */
	public String getIpaddress() {
		return ipaddress;
	}
	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}
	/**
	 * Port on which the SVRCONN channel is listening (a valid port number, or the empty string to use MQ Java Bindings)
	 * @return
	 */
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * Queue Manager on which the broker is running (or empty String to use the default queue manager)
	 * @return
	 */
	public String getQmgr() {
		return qmgr;
	}
	public void setQmgr(String qmgr) {
		this.qmgr = qmgr;
	}

	/**
	 * The Broker itself
	 * @return
	 */
	public BrokerProxy getIntNode() {
		return intNode;
	}

	public void setIntNode(BrokerProxy intNode) {
		this.intNode = intNode;
//		this.iibBrokerProxy = intNode.getBrokerProxy(); FIXME Need to determine what this should be  
	}

	public boolean isLocal() {
		return local;
	}

	public void setLocal(boolean local) {
		this.local = local;
	}

	@Override
	public String toString() {
		return "intNodeName=" + intNodeName + ", ipaddress=" + ipaddress + ", is local? " + local
				+ ipaddress + ", port=" + port + ", qmgr=" + qmgr + ", intnode=" + intNode;
	}

	
}
