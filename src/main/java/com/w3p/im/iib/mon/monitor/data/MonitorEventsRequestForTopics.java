package com.w3p.im.iib.mon.monitor.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.JMS_TOPIC;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.MQTT_TOPIC;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.QUEUE;

import java.util.Map;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.jms.producers.FlowToProcessModelCache;
import com.w3p.im.iib.mon.jms.service.MqJmsConnectionProperties;
import com.w3p.im.iib.mon.mqtt.service.MqttConnectionProperties;

public class MonitorEventsRequestForTopics extends MonitorEventsRequest {

	private final Njams njamsClient;
	private FlowToProcessModelCache flowToProcessModelCache;
	
	private final String integrationServerName;
	private final String parentName;
	 
	private MqttConnectionProperties mqttConnectionProperties;
	
	
	public MonitorEventsRequestForTopics(Njams njamsClient, String integrationNodeName, String integrationServerName, String parentName) {
		super(integrationNodeName);
		this.njamsClient = njamsClient;
		this.integrationServerName = integrationServerName;
		this.parentName = parentName; // USed by ACE
	}
	
	public Njams getNjamsClient() {
		return njamsClient;
	}

	public FlowToProcessModelCache getFlowToProcessModelCache() {
		return flowToProcessModelCache;
	}

	public MonitorEventsRequestForTopics setFlowToProcessModelCache(FlowToProcessModelCache flowToProcessModelCache) {
		this.flowToProcessModelCache = flowToProcessModelCache;
		return this;
	}
	
	public String getIntegrationServerName() {
		return integrationServerName;
	}

	public String getParentName() {
		return parentName;
	}

	public MqttConnectionProperties getMqttConnectionProperties() {
		return mqttConnectionProperties;
	}

	public MonitorEventsRequestForTopics setMqttConnectionProperties(MqttConnectionProperties mqttConnectionProperties) {
		this.mqttConnectionProperties = mqttConnectionProperties;
		return this;
	}

//	public String getEventsSource() {
//		return eventsSource;
//	}
//
//	public MonitorEventsRequest setEventsSource(String eventsSource) {
//		this.eventsSource = eventsSource;
//		return this;
//	}

//	public boolean isUseMQTT() {
//		return eventsSource.equals(MQTT_TOPIC);
//	}
//
//	public boolean isUseTopic() {
//		return eventsSource.equals(JMS_TOPIC);
//	}
//
//	public boolean isUseQueue() {
//		return eventsSource.startsWith(QUEUE);
//	}

//	public boolean isTracingEnabled() {
//		return tracingEnabled;
//	}
//
//	public MonitorEventsRequest setTracingEnabled(boolean tracingEnabled) {
//		this.tracingEnabled = tracingEnabled;
//		return this;
//	}

//	public String getIibEventsProblemsQueue() {
//		return iibEventsProblemsQueue;
//	}
//
//
//	public MonitorEventsRequest setIibEventsProblemsQueue(String iibEventsProblemsQueue) {
//		this.iibEventsProblemsQueue = iibEventsProblemsQueue;
//		return this;
//	}

//	public int getConsumerThreads() {
//		return consumerThreads;
//	}
//
//
//	public MonitorEventsRequest setConsumerThreads(int consumerThreads) {
//		this.consumerThreads = consumerThreads;
//		return this;
//	}

	@Override
	public String toString() {
		return "MonitorEventsRequestForTopics [integrationServerName=" + integrationServerName
				+ ", integrationNodeName=" + integrationNodeName + ", eventsSource=" + eventsSource + "]";
	}

}
