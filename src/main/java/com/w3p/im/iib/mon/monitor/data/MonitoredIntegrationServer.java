package com.w3p.im.iib.mon.monitor.data;

public class MonitoredIntegrationServer  extends MonitoredObject<MonitoredObject<?>> {	

	public MonitoredIntegrationServer(String name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "Monitored IntegrationServer [name=" + name + "]";
	}
}
