package com.w3p.im.iib.mon.monitor.data;

public class MonitoredMessageFlow extends MonitoredObject<MonitoredObject<?>> {

	public MonitoredMessageFlow(String name) {
		super(name);
	}
	
	@Override
	public String toString() {
		return "Monitored Message Flow [name=" + name + "]";
	}
}
