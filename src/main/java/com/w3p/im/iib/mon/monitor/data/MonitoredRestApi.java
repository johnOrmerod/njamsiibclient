package com.w3p.im.iib.mon.monitor.data;

public class MonitoredRestApi extends MonitoredObject<MonitoredObject<?>> {
	
	public MonitoredRestApi(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Monitored RestApi [name=" + name + "]";
	}
}
