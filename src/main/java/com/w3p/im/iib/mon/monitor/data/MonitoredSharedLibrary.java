package com.w3p.im.iib.mon.monitor.data;

public class MonitoredSharedLibrary extends MonitoredObject<MonitoredObject<?>> {
	
	public MonitoredSharedLibrary(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return "Monitored Shared Library [name=" + name + "]";
	}
}
