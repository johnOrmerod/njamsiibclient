package com.w3p.im.iib.mon.monitor.data;

import java.util.List;

public class MonitoringEventSummary {
	
	public MonitoringEventSummary(String brokerName, String executionGroup, String messageFlow) {
		this.integrationNode = brokerName;
		this.integrationServer = executionGroup;
		this.messageFlow = messageFlow;
	}
	private String integrationNode;
	private String integrationServer;
	private String messageFlow;
	private String start;
	private String end;
//	private long duration;
	private String correlationId;
	private List<NodeData> executionPath;
	
	
	public String getIntegrationNode() {
		return integrationNode;
	}
	public void setIntegrationNode(String integrationNode) {
		this.integrationNode = integrationNode;
	}
	public String getIntegrationServer() {
		return integrationServer;
	}
	public void setIntegrationServer(String integrationServer) {
		this.integrationServer = integrationServer;
	}
	public String getMessageFlow() {
		return messageFlow;
	}
	public void setMessageFlow(String messageFlow) {
		this.messageFlow = messageFlow;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
//	public long getDuration() {
//		return duration;
//	}
//	public void setDuration(long duration) {
//		this.duration = duration;
//	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public List<NodeData> getExecutionPath() {
		return executionPath;
	}
	public void setExecutionPath(List<NodeData> executionPath) {
		this.executionPath = executionPath;
	}
	@Override
	public String toString() {
		return  integrationNode + "."+integrationServer+"."+messageFlow;
	}

	
}
