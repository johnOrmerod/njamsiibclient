package com.w3p.im.iib.mon.monitor.data;

public class NodeData {
	
	private String nodeLabel;
	private String payloadOut;
	private String applicationData;
	
	public NodeData(String nodeLabel) {
		super();
		this.nodeLabel = nodeLabel;
	}
	public String getNodeLabel() {
		return nodeLabel;
	}
	public void setNodeLabel(String nodeLabel) {
		this.nodeLabel = nodeLabel;
	}
	public String getPayloadOut() {
		return payloadOut;
	}
	public void setPayloadOut(String payloadOut) {
		this.payloadOut = payloadOut;
	}
	public String getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	} 

}
