package com.w3p.im.iib.mon.monitor.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.MON_PROF;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.DOT_XML;

import com.w3p.api.config.proxy.BrokerProxy;
import com.w3p.api.config.proxy.MessageFlowProxy;

import com.w3p.im.iib.mon.topology.model.MessageFlow;

public class ProcessMonitoringProfileRequest {
	
	private int timeToWait; // Wait-time in seconds for sync calls to remote broker for applying Monitoring Profiles   
	private boolean applyProfileIfUnchanged; // 'true' = generate profile and apply it even if it has not changed (compared to current file)
	private BrokerProxy intNodeProxy;
	private final MessageFlowProxy messageFlowProxy;
	private final MessageFlow messageFlowModel;
	private String intNodeName;
	private String monProfileDir;
	
	private final String profileAsXml;
	private final String monitoringProfile;
	private final String monitoringProfileFile;
	private final String integrationServer;
	private final String application;
	private final String messageFlow;
	
	public ProcessMonitoringProfileRequest(String profileAsXml, String integrationServer, String application, MessageFlow messageFlowModel) {
		this.profileAsXml = profileAsXml;
		this.integrationServer = integrationServer;
		this.application = application;
		this.messageFlowModel = messageFlowModel;
		this.messageFlow = messageFlowModel.getNameWithSchema();
		this.messageFlowProxy = messageFlowModel.getMessageFlowProxy();
		this.monitoringProfile = messageFlowModel.isIncludeSchemaName() ? this.messageFlow+MON_PROF : messageFlowModel.getName()+MON_PROF;
		this.monitoringProfileFile = monitoringProfile+DOT_XML;
	}
	public int getTimeToWait() {
		return timeToWait;
	}
	public ProcessMonitoringProfileRequest setTimeToWait(int timeToWait) {
		this.timeToWait = timeToWait;
		return this;
	}
	public boolean isApplyProfileIfUnchanged() {
		return applyProfileIfUnchanged;
	}
	public ProcessMonitoringProfileRequest setApplyProfileIfUnchanged(boolean applyProfileIfUnchanged) {
		this.applyProfileIfUnchanged = applyProfileIfUnchanged;
		return this;
	}
	public String getProfileAsXml() {
		return profileAsXml;
	}
	public String getMonitoringProfile() {
		return monitoringProfile;
	}
	public String getMonitoringProfileFile() {
		return monitoringProfileFile;
	}
	public String getIntegrationServer() {
		return integrationServer;
	}
	public String getApplication() {
		return application;
	}
	public String getIntNodeName() {
		return intNodeName;
	}
	public ProcessMonitoringProfileRequest setIntNodeName(String intNodeName) {
		this.intNodeName = intNodeName;
		return this;
	}
	public String getMonProfileDir() {
		return monProfileDir;
	}
	public ProcessMonitoringProfileRequest setMonProfileDir(String monProfileDir) {
		this.monProfileDir = monProfileDir;
		return this;
	}
	public BrokerProxy getIntNodeProxy() {
		return intNodeProxy;
	}
	public ProcessMonitoringProfileRequest setIntNodeProxy(BrokerProxy intNodeProxy) {
		this.intNodeProxy = intNodeProxy;
		return this;
	}
	public MessageFlowProxy getMessageFlowProxy() {
		return messageFlowProxy;
	}
	public MessageFlow getMessageFlowModel() {
		return messageFlowModel;
	}
}
