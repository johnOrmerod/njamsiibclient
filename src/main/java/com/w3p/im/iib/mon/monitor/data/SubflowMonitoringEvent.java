package com.w3p.im.iib.mon.monitor.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.model.SubProcessActivityModel;


public class SubflowMonitoringEvent extends MonitoringEvent {
	private static final Logger logger = LoggerFactory.getLogger(SubflowMonitoringEvent.class);
	
	// At entry to the subflow, this class is a proxy for the first event in the subflow (current event) 
	
	public SubProcessActivityModel getSubProcessActivityModel() {
		String subProcessActivityModelId = getLocalNodeLabelAsId();		
		SubProcessActivityModel subProcessActivityModel = processModel.getSubProcess(subProcessActivityModelId);
		return subProcessActivityModel;
	}
}
