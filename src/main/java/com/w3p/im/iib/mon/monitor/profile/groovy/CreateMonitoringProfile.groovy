package com.w3p.im.iib.mon.monitor.profile.groovy

import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import java.util.List

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.w3p.im.iib.mon.client.config.data.ApplicationDataQuery
import com.w3p.im.iib.mon.client.config.data.Content
import com.w3p.im.iib.mon.client.config.data.DataTypeToQueryText
import com.w3p.im.iib.mon.client.config.data.GlobalCorrelationId
import com.w3p.im.iib.mon.topology.model.Application
import com.w3p.im.iib.mon.topology.model.Flow
import com.w3p.im.iib.mon.topology.model.FlowNode
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection
import com.w3p.im.iib.mon.topology.model.IntegrationServer
import com.w3p.im.iib.mon.topology.model.MessageFlow
import com.w3p.im.iib.mon.topology.model.SubFlow
import groovy.xml.MarkupBuilder
import groovy.xml.StreamingMarkupBuilder

class CreateMonitoringProfile {
	final Logger logger = LoggerFactory.getLogger(CreateMonitoringProfile.class)
//	logger.trace("")
// For use of 'delegate' see: https://stackoverflow.com/questions/10880220/how-to-recursively-add-a-child-inside-streamingmarkupbuilder
	/*
	 * When input is JSON, for xpath use: '$Root/JSON/Data/xpath for data' [JSON is converted to XML]
	 * When input id XML,  for xpath use: '$Root/XMLNSC/xpath-for-data'
	 * When input is SOAP, start xpath after <Body>
	 */
	def globalCorrelationId
	def nodeTypeToCorrelationIdsMap	 // Can be null
	def monitoredTerminalTypes = ['catch', 'failure', 'fault', 'noMessage', 'noMatch', 'timeout', 'unknown', 'alternate', 'control', 'eod' ]
	def eventSourceAddressList = []
	  
//	Sample
//		 = [
//		'ComIbmMQInputNode':'$Root/MQMD/MsgId',
//		'ComIbmSOAPInputNode':'$Root/HTTPInputHeader/MsgId' or '$LocalEnvironment/Destination/HTTP/RequestIdentifier',
//		'ComIbmWSInputNode':'$Root/HTTPInputHeader/MsgId'
//		]
	def nodeTypeToNamespacesMap // Can be null
//	 Sample
//		 = [
//		'ComIbmMQInputNode':[],
//		'ComIbmSOAPInputNode':[],
//		'ComIbmWSInputNode':[]
//		]
	/*
	 * applicationDataQuery
	 */
	def applicationDataQuery
	def applicationDataQueryMap
//	Sample
//	 = [
//		'ComIbmMQGetNode':[
//			'MsgId':'$Root/MQMD/MsgId'],
//		'ComIbmMQOutputNode':[
//			'MsgId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId'],
//		'ComIbmMQReply':[
//			'MsgId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/msgId',
//			'CorrelId':'$LocalEnvironment/WrittenDestination/MQ/DestinationData[1]/correlId']
//		]
		
	boolean includePayloadBody = true
		
	String createForFlow (IntegrationServer is, MessageFlow mf, GlobalCorrelationId gci, ApplicationDataQuery adq) {
		logger.debug("CreateMonitoringProfile # createForFlow() - enter")
		applicationDataQuery = adq
		nodeTypeToCorrelationIdsMap = gci?.nodeTypeToCorrelationIds?.nodeTypeToCorrelationIds
		nodeTypeToNamespacesMap = gci?.nodeTypeToNamespaces?.nodeTypeToNamespacesMap
		
		String builtProfile = null
		try	{
			def builder = new StreamingMarkupBuilder()
			builder.encoding = UTF_8
			def profile = {
				mkp.declareNamespace(p: "http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0.3/monitoring/profile")
				'p:monitoringProfile' ('p:version':'2.0') {
					processNodes.delegate = delegate
					processNodes(mf, null, is)
				}
			}

			builtProfile = builder.bind(profile)
			return builtProfile
		} catch (all) {
			all.printStackTrace()
			throw all
		}
		logger.trace("CreateMonitoringProfile # createForFlow() - exit")
	}

	// Define main Closure
	def processNodes = { Flow flow, String sfName, IntegrationServer is ->
		includePayloadBody = flow.isIncludePayload()
		flow.flowNodes.each { node ->
			createEventsource.delegate = delegate
			if (node.type != 'InputNode' && node.type.endsWith('InputNode') || node.type == 'ComIbmSOAPAsyncResponseNode' || node.type == 'ComIbmTimeoutNotificationNode') {
				
				createEventsource(node, sfName, 'start', nodeTypeToCorrelationIdsMap?."$node.type")
				createEventsource(node, sfName, 'end', null)
				node.flowNodeTargetConnections.each {FlowNodeConnection fntc ->
					if (fntc.sourceOutTerminal.toLowerCase().contains('catch')) {
						createEventsource(node, sfName, 'catch', null)
					} else if (fntc.sourceOutTerminal.toLowerCase().contains('failure')) {
						createEventsource(node, sfName, 'failure', null)
					} else if (fntc.sourceOutTerminal.toLowerCase().contains('fault')) {
						createEventsource(node, sfName, 'fault', null)
					} else if (fntc.sourceOutTerminal.toLowerCase().contains('timeout')) {
						createEventsource(node, sfName, 'timeout', null)
					}
				}
			} else if (node.type == 'SubFlowNode') {
				SubFlow sf = is.getActualSubflow(node) 
				// is.findSubflow(node.getSubflowFileName(flow.isIncludeSchemaName())) //is.findSubflowUsingURIProperty(node, flow.isIncludeSchemaName()) 
				if (sf) {
					sf.setNameInFlow(node.name)  //TODO includes the schema name //TODO have a null check & find out how to log errors in Groovy
					String nodeName = node.name
					if (flow.class.isAssignableFrom(SubFlow.class)) {
						//					nodeName = flow.getNameInFlow() +'.'+ node.name   // append parent's name (as defined in its subflow node), if parent is a subflow.
						nodeName = sfName +'.'+ node.name
					}
					processNodes(sf, nodeName, is) // node.name is the name used in the msgflow
				} else {
					logger.error("Subflow '{}' in flow '{}' was not found - probably not included in the startup config file", flow.name, node.name); 
				}
			} else if (node.type == 'ComIbmLabelNode') {
				createEventsource(node, sfName, 'out', null)
			} else if (node.type.startsWith("ComIbmMQ") && node.type != "ComIbmMQHeaderNode") {
				createEventsource(node, sfName, 'in', null)
				createEventsource(node, sfName, 'out', null)
			} else if (node.type != 'OutputNode' && node.type != 'InputNode' ) {
				node.flowNodeTargetConnections.each {FlowNodeConnection fntc ->
					if (fntc.sourceOutTerminal.toLowerCase() in monitoredTerminalTypes) {
						createEventsource(node, sfName, fntc.sourceOutTerminal.toLowerCase(), null)
					} 						
				}
				node.flowNodeSourceConnections.each{FlowNodeConnection fnsc ->
					createEventsource(node, sfName, fnsc.targetInTerminal.toLowerCase(),  null) // Will mostly be 'in'
				}				
			}
		}
	}

	// Define inner Closure
	def createEventsource = { FlowNode node, String sfName, String terminalName, globalCorrelId ->
		String eventSourceAddress = null
		String eventName = null
		String uow = null
		String sourceAddress = sfName == null ? node.name : sfName+'.'+node.name
		if (terminalName == 'in') {
			eventSourceAddress = sourceAddress+'.terminal.in'
			eventName = node.name+'.InTerminal'
			uow = 'independent'
		} else if (terminalName == 'out') {
			eventSourceAddress = sourceAddress+'.terminal.out'
			eventName = node.name+'.OutTerminal'
			uow = 'independent'
		} else if (terminalName == 'start' || terminalName == 'end' || terminalName == 'rollback') {
			eventSourceAddress = sourceAddress+'.transaction.'+terminalName.capitalize()
			eventName = node.name+'.Transaction'+terminalName.capitalize()
			uow = terminalName == 'start' || terminalName == 'rollback' ? 'independent' : 'none'
		} else if (terminalName == 'catch') {
			eventSourceAddress = sourceAddress+'.terminal.catch'
			eventName = node.name+'.CatchTerminal'
			uow = 'independent'
		} else if (terminalName == 'failure') {
			eventSourceAddress = sourceAddress+'.terminal.failure'
			eventName = node.name+'.FailureTerminal'
			uow = 'independent'
		} else if (terminalName == 'fault') {
			eventSourceAddress = sourceAddress+'.terminal.fault'
			eventName = node.name+'.FaultTerminal'
			uow = 'independent'
		} else if (terminalName == 'timeout') {
			eventSourceAddress = sourceAddress+'.terminal.timeout'
			if (node.name == "HTTP Input") {
				eventName = node.name+'.HTTP TimeoutTerminal'
			} else {
				eventName = node.name+'.TimeoutTerminal'
			}
			uow = 'independent'
		} else if (terminalName == 'noMessage') {
			eventSourceAddress = sourceAddress+'.terminal.noMessage'
			eventName = node.name+'.No MessageTerminal'
			uow = 'independent'
		} else if (terminalName == 'noMatch') {
			eventSourceAddress = sourceAddress+'.terminal.noMatch'
			eventName = node.name+'.No matchTerminal'
			uow = 'independent'
		} else if (terminalName == 'unknown') {							// Aggregate Control node
			eventSourceAddress = sourceAddress+'.terminal.unknown'
			eventName = node.name+'.UnknownTerminal'
			uow = 'independent'
		} else if (terminalName == 'alternate') { 						// Java Compute node
			eventSourceAddress = sourceAddress+'.terminal.alternate'
			eventName = node.name+'.AlternateTerminal'
			uow = 'independent'
		} else if (terminalName == 'control') { 						// Collector or Aggregate Reply node - input terminal
			eventSourceAddress = sourceAddress+'.terminal.control'
			eventName = node.name+'.ControlTerminal'
			uow = 'independent'
		} else if (terminalName == 'eod') { 						// File output node - 'EOD' input terminal
			eventSourceAddress = sourceAddress+'.terminal.EOD'
			eventName = node.name+'.Finish FileTerminal'
			uow = 'independent'
		} else { 
			// User-defined input terminal for Collector node OR it's for an output terminal whose name we don't yet handle
			eventSourceAddress = sourceAddress.concat('.terminal.').concat(terminalName)
			eventName = node.name.concat('.').concat(terminalName).concat('Terminal')
			uow = 'independent'
			logger.warn("*** An in terminal with name '{}' for node '{}', has a name other than 'In'. It was used to create a Monitoring Event."
				+ "OR it is an error caused by an out termial that is not handled  ", terminalName, node.name);
		}
		
		if (!(eventSourceAddress in eventSourceAddressList)) {
			eventSourceAddressList.add(eventSourceAddress)
	
		'p:eventSource' ('p:eventSourceAddress':eventSourceAddress, 'p:enabled':'true') {
			'p:eventPointDataQuery' {
				'p:eventIdentity' {
					'p:eventName' ('p:literal':eventName)
				}
				'p:eventCorrelation' {
					'p:localTransactionId' ('p:sourceOfId':'automatic')
					'p:parentTransactionId' ('p:sourceOfId':'automatic')
					if (globalCorrelId) {
						if (nodeTypeToCorrelationIdsMap?."$node.type") {
							'p:globalTransactionId' ('p:queryText':nodeTypeToCorrelationIdsMap."$node.type", 'p:sourceOfId':'query') {
								nodeTypeToNamespacesMap?.each {nodeType, nsList -> 
									nsList.each {ns ->
										'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
									}
								}
							}
						} else {
							'p:globalTransactionId' ('p:queryText':globalCorrelId, 'p:sourceOfId':'query')
						}
					} else {
						'p:globalTransactionId' ('p:sourceOfId':'automatic')
					}
				}
				'p:eventFilter' ('p:queryText':'true()')
				'p:eventUOW' ('p:unitOfWork':uow)
			}
			'p:applicationDataQuery' {
				if (terminalName == 'Start' && (node.type == 'ComIbmMQInputNode' || node.type == 'ComIbmWSInputNode') || node.type == 'ComIbmSOAPInputNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalName == 'Out' && (node.type == 'ComIbmMQInputNode' || node.type == 'ComIbmWSInputNode' || node.type == 'ComIbmSOAPInputNode')) {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				}  else if (terminalName == 'Out' && node.type != 'ComIbmMQGetNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {	
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces.namespaces.each { ns ->
									'p:prefixMapping' ('prefix':ns.prefix, 'URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalName == 'In' && node.type == 'ComIbmMQOutputNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalName == 'In' && node.type == 'ComIbmMQGetNode') {
					Content content = applicationDataQuery?.getQueryContent("$node.type")
					if (content) {						
						'p:simpleContent' ('p:dataType':'string', 'p:name':content.name) {
							'p:valueQuery' ('p:queryText':content.xpath) {
								content.namespaces?.namespaces.each { ns ->
									'p:prefixMapping' ('p:prefix':ns.prefix, 'p:URI':ns.uri)
								}
							}
						}
					}
				} else if (terminalName == 'Catch') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalName == 'Rollback') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalName == 'Failure') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalName == 'Fault') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				} else if (terminalName == 'noMessage') {
					'p:complexContent' {
						'p:payloadQuery' ('p:queryText':'$ExceptionList')
					}
				}
			}
			if (includePayloadBody && (terminalName != 'Out' || node.type == 'ComIbmLabelNode')) { // Exclude payload  for: 'out' teminals and Label nodes
				'p:bitstreamDataQuery' ('p:bitstreamContent':'body', 'p:encoding':'base64Binary')
			}
		}
		}
	}
}
