package com.w3p.im.iib.mon.mqtt.service;

import com.w3p.im.iib.mon.client.config.data.User;

public class MqttConnectionProperties {
	
	private String host;
	private int port;
	private int qos;	
	private int keepAliveInterval;
	private String clientId;
	private User user;
	
	
	public MqttConnectionProperties() {
		// TODO Auto-generated constructor stub
	}


	public String getHost() {
		return host;
	}
	public MqttConnectionProperties setHost(String host) {
		this.host = host;
		return this;
	}
	
	public int getPort() {
		return port;
	}
	public MqttConnectionProperties setPort(int port) {
		this.port = port;
		return this;
	}
	
	public int getQos() {
		return qos;
	}
	public MqttConnectionProperties setQos(int qos) {
		this.qos = qos;
		return this;
	}
	
	public int getKeepAliveInterval() {
		return keepAliveInterval;
	}
	public MqttConnectionProperties setKeepAliveInterval(int keepAliveInterval) {
		this.keepAliveInterval = keepAliveInterval;
		return this;
	}
	
	public String getClientId() {
		return clientId;
	}
	public MqttConnectionProperties setClientId(String clientId) {
		this.clientId = clientId;
		return this;
	}
	
	public User getUser() {
		if (user == null) {
			user = new User();
		}
		return user;
	}
	public MqttConnectionProperties setUser(User user) {
		this.user = user;
		return this;
	}

	@Override
	public String toString() {
		return "MqttConnectionProperties [host=" + host + ", port=" + port + ", clientId=" + clientId + "]";
	}
}
