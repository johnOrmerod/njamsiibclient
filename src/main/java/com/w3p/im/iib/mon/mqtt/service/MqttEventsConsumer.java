package com.w3p.im.iib.mon.mqtt.service;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.w3p.im.iib.mon.event.processors.MessageEvents;
import com.w3p.im.iib.mon.event.processors.MonitorEventsProcessor;
import com.w3p.im.iib.mon.exceptions.monitoring.MonitoringEventParserException;
import com.w3p.im.iib.mon.exceptions.monitoring.MonitoringException;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForTopics;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.groovy.ParseMonitoringEvent;

public class MqttEventsConsumer implements MqttCallbackExtended {	
	private static final Logger logger = LoggerFactory.getLogger(MqttEventsConsumer.class);
	
	private final String topic;
	private final MqttClient client;
	private final MqttConnectOptions mqttConnectionOptions;
	private MessageEvents messageEvents = new MessageEvents();
	
	private final MonitorEventsProcessor monitorEventsProcessor;
	private final int qos; // Quality of Service: 0, 1, 2
	private String currentLocalTransactionId = "";
	
	
	public MqttEventsConsumer(MqttClient client, MqttConnectOptions mqttConnectionOptions, MonitorEventsRequestForTopics request, String topic) {
		this.topic = topic;
		this.client = client;
		this.mqttConnectionOptions = mqttConnectionOptions;
		this.qos = request.getMqttConnectionProperties().getQos();

		monitorEventsProcessor = new MonitorEventsProcessor(null, null, request); // FIXME njamsClient and ftpmc set ot null - added for reading events from queue TEMP
	}

	/**
	 * 
	 * This callback is invoked when a message is received on a subscribed topic.
	 * 
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) {
		try {
			String messageAsString = new String(message.getPayload());
			logger.trace("Topic: {},  Message: {}", topic, messageAsString);
			MonitoringEvent event = processMessage(messageAsString);
			if (event.getLocalTransactionId().equals(currentLocalTransactionId)) {
				messageEvents.addEvent(event);
			}
			if (messageEvents.haveAllEventsBeenReceived()) {
				// We have read the last message of the set & no more event messages for subsequent msgflows - process the event set
				logger.debug("readEventsMessages(): read last msg of event set - processing the set");
				messageEvents.processEvents();
				monitorEventsProcessor.sendLogMessageToNjams(messageEvents);
				// Start over again				
				messageEvents.clearEvents();
			}
		} catch (MonitoringEventParserException | MonitoringException e) {
			// Error parsing a monitoring event, or processing the event set.The event set will never be complete and messageEvents will be cleared on next change of localTxnId
			logger.error(e.toString(), e);
		} catch (Exception e) {
			// Could be anything - don't throw an exception to avoid breaking this MQTT cient
			logger.error(e.toString(), e);
		}
	}
	

	/**
	 * <b>NOTE</b>: This is the same code as MonitorEventsConsumer#(processMessage)
	 * @param messageAsString
	 * @return MonitoringEvent
	 * @throws MonitoringEventParserException
	 */
	private MonitoringEvent processMessage(String messageAsString) throws MonitoringEventParserException {
		logger.trace("#processMessage() - Message received: \n  Text = {}", messageAsString);
		try {
			MonitoringEvent	event = new ParseMonitoringEvent().parse(messageAsString);
			if (!currentLocalTransactionId.equals(event.getLocalTransactionId())) {
				currentLocalTransactionId = event.getLocalTransactionId();
			}
			return event;
		} catch (Exception e) {
			throw new  MonitoringEventParserException("Error parsing a Monitoring Event. The message set will be skipped. Cause: ''[0]''.\n  Message = ''{1}''. ",
					e.toString(), messageAsString);
		}
	}
	
	/**
	 * 
	 * This callback is invoked upon losing the MQTT connection.
	 * 
	 */
	@Override
	public void connectionLost(Throwable e) {
		logger.error("Connection lost! Cause: {}", e.toString(), e);
		// code to reconnect to the broker would go here if desired
		boolean connected = false;
		int retry = 0;
		while (!connected && retry < 10) {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				// Can't do much about it
			}			
			connected = reconnectToBroker(client, mqttConnectionOptions);			
			retry++;
		}
				
		if (connected) {
			// Re-subscribe as subscription has been lost along with unprocessed messages
			try {
				client.subscribe(topic, qos);
				// Reset the processing variables
				messageEvents.clearEvents();
				currentLocalTransactionId = "";
			} catch (MqttException ex) {
				logger.error(ex.toString(), ex);
			} catch (Exception ex) {
				logger.error(ex.toString(), ex);
			}
		}
	}
	
	protected boolean reconnectToBroker(MqttClient client, MqttConnectOptions connectionOptions) {
		// Connect to Broker
		boolean connected = false;
		try {
			if (!client.isConnected()) { 
				IMqttToken token = client.connectWithResult(connectionOptions);
//			if (token.getException() != null) {
//				return "Response is: exception = "+token.getException().toString() ; 
//			}
				logger.debug("IMqttToken: {}", token.getResponse().toString());
			}
			connected = client.isConnected(); //token.getSessionPresent();
		} catch (MqttException e) {
			logger.error(e.toString(), e);
		}catch (Exception e) {
			logger.error(e.toString(), e);
		}
		return connected;
	}
	
	/**
	 * Called when the connection to the server is completed successfully.<br>
	 * @param reconnect: 'true' = the connection was the result of automatic reconnect.
	 * @param serverURI: the server URI to which the connection was made.
	 * 
	 */
	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		logger.debug("connectComplete: reconnect: {}, serverURI: {}.", reconnect, serverURI);	
	}
	/**
	 * 
	 * This callback is invoked when a message published by this client is successfully received by the broker.
	 * 
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {
		// Not used - this class only subscribes
	}

}
