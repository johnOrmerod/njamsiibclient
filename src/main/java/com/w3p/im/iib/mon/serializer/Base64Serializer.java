package com.w3p.im.iib.mon.serializer;

import java.nio.charset.Charset;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.serializer.Serializer;


public class Base64Serializer<T> implements Serializer<T> {
	
	Logger log = LoggerFactory.getLogger(Base64Serializer.class);


	@Override
	public String serialize(final T value) {
		log.debug("#~#~#~#~ Base64Serializer called to decode: {}", value);
		String result = "";
		if (value != null) {
			if (value instanceof String) {
				if (((String) value).contains("<wmb:applicationData xmlns=\"\">")) {
					// Detect whether the input is XML and return as-is
					result = (String) value;
				} else {
					result = new String(Base64.getDecoder().decode(((String)value).getBytes()), Charset.defaultCharset());
				}
			}
		}
		log.debug("~#~#~#~# Base64Serializer decoded input to: {}", result);
		return result;
	}
}
