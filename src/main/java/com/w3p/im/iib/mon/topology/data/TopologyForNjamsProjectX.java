package com.w3p.im.iib.mon.topology.data;

import java.util.Map;

public class TopologyForNjamsProjectX {
	
	private final boolean success;
	private final String message;
	private final Map<String, String> flowProcessModelCache;
	

	public TopologyForNjamsProjectX(String message, Map<String, String> flowProcessModelCache, boolean success) {
		super();
		this.message = message;
		this.flowProcessModelCache = flowProcessModelCache;
		this.success = success;
	}

	public String getMessage() {
		if (message == null) {
			message.equals("");
		}
		return message;
	}

	public boolean isSuccess() {
		return success;
	}

	public Map<String, String> getFlowProcessModelCache() {
		return flowProcessModelCache;
	}
}
