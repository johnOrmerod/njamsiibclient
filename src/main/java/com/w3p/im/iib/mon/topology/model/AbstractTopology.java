package com.w3p.im.iib.mon.topology.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractTopology {

	protected final static String DOT = ".";
	protected final List<MessageFlow> messageFlows = new ArrayList<>();	
	protected final List<SubFlow> subFlows = new ArrayList<>();
	protected final List<StaticLibrary> staticLibraries = new ArrayList<>();
	protected final Map<String, MessageFlow> messageFlowMap = new HashMap<>();
	
	protected String name;
	protected boolean includeSchemaName;
	public String getName() {
		return name;
	}

	public <T extends AbstractTopology> void addTopologyItem(T topologyItem) {		
		if (topologyItem instanceof MessageFlow) {
			messageFlows.add((MessageFlow) topologyItem);
			messageFlowMap.put(topologyItem.getName(), (MessageFlow) topologyItem);
		} else if (topologyItem instanceof SubFlow) {
			subFlows.add((SubFlow) topologyItem);
		} else if (topologyItem instanceof StaticLibrary) {
			staticLibraries.add((StaticLibrary) topologyItem);
		} 
	}

	public List<MessageFlow> getMessageFlows() {
		return messageFlows;
	}
	
	public MessageFlow getMessageFlow(String name) {
		return messageFlowMap.get(name);
	}
	
	public List<SubFlow> getSubFlows() {
		return subFlows;
	}
	
	public List<StaticLibrary> getStaticLibraries() {
		return staticLibraries;
	}

	/**
	 * For jUnit only
	 * @param messageFlows 
	 */
	protected void setMessageFlows(List<MessageFlow> messageFlows) {
		this.messageFlows.clear();
		this.messageFlows.addAll(messageFlows);
	}
	
	public abstract boolean isRunning();

	public void setIncludeSchemaName(boolean includeSchemaNames) {
		this.includeSchemaName = includeSchemaNames;
	}

	public boolean isIncludeSchemaName() {
		return includeSchemaName;
	}
}
