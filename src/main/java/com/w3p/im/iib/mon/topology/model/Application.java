package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.ApplicationProxy;


public class Application extends Deployable { //AbstractTopology {
	
	
	public Application(ApplicationProxy ap) {
		super(ap);
	}

	@Override
	public String toString() {
		return "Application [name=" + name + "]";
	}
}
