package com.w3p.im.iib.mon.topology.model;

import com.w3p.api.config.proxy.DeployableProxy;
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;


public class Deployable extends AbstractTopology {
	
	private DeployableProxy dp;	
	
	
	public Deployable() {
		super();
	}

	public Deployable(DeployableProxy dp) {
		this.dp = dp;
		try {
			super.name = dp.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace();
		}
	}

	@Override
	public boolean isRunning() {
		try {
			return dp.isRunning();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			e.printStackTrace();
			return false;
		}
	}
}
