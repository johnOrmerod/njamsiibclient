package com.w3p.im.iib.mon.topology.model;

import java.util.List;

import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException;
import com.w3p.api.config.proxy.SharedLibraryProxy;


public class SharedLibrary extends AbstractTopology {
	
	private final SharedLibraryProxy slp;	
	

	public SharedLibrary(SharedLibraryProxy slp) {
		this.slp = slp;
		try {
			super.name = slp.getName();
		} catch (ConfigManagerProxyPropertyNotInitializedException e) {
			super.name = null;
			e.printStackTrace();
		}
	}

	public SharedLibraryProxy getSharedLibraryProxy() {
		return slp;
	}
	
	
	@Override
	public <T extends AbstractTopology> void addTopologyItem(T topologyItem) {
		if (topologyItem instanceof MessageFlow) {
			messageFlows.add((MessageFlow) topologyItem);
		} else if (topologyItem instanceof SubFlow) {
			subFlows.add((SubFlow) topologyItem);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.w3p.im.iib.mon.topology.model.AbstractTopology#isRunning()
	 * @return = true ->  // isRunning() is not defined for a Library
	 */
	@Override
	public boolean isRunning() {
		return true;
	}

	public List<MessageFlow> getMessageFlows() {
		return messageFlows;  // Always empty - needed for code that tries to get message flows for generic
	}
	public List<SubFlow> getSubFlows() {
		return subFlows;
	}
	protected void setSubFlows(List<SubFlow> subFlows) {
		this.subFlows.clear();
		this.subFlows.addAll(subFlows);
	}
	
	@Override
	public String toString() {
		return "Shared Library [name=" + name + "]";
	}

}
