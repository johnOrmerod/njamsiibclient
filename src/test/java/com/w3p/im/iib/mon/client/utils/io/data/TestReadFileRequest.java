package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.DEFAULT_NAME_PATTERN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestReadFileRequest {
	
	private static final String content = "content";
	private static final String namePattern = "pattern_%s.txt";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateRequestWithNamePattern() {
		IReadFileRequest req = new ReadFileRequest();
		req.setContent(content)
		.setNamePattern(namePattern)
		.addOption("optionA", Boolean.TRUE);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
	}
	
	@Test
	public void testCanCreateRequestWithDefaultNamePattern() {
		IReadFileRequest req = new ReadFileRequest();
		req.setContent(content)
		.addOption("optionA", Integer.MAX_VALUE);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(DEFAULT_NAME_PATTERN));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Integer)req.getOption("optionA").get() == Integer.MAX_VALUE);
	}
	
	@Test
	public void testCanCreateRequestWithReadBufferSize() {
		IReadFileRequest req = new ReadFileRequest();
		req.setContent(content)
		.setNamePattern(namePattern)
		.addOption("optionA", Boolean.TRUE);
		req.setSource("inputFile")
		.setReadBufSize(2048);		

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
		assertEquals("inputFile", req.getSource());
		assertEquals(2048, req.getReadBufSize());
	}
	
	@Test
	public void testCanCreateRequestWithDefaultReadBufferSize() {
		IReadFileRequest req = new ReadFileRequest();
		req.setContent(content)
		.setNamePattern(namePattern)
		.addOption("optionA", Boolean.TRUE);
		req.setSource("inputFile");		

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
		assertEquals("inputFile", req.getSource());
		assertEquals(4096, req.getReadBufSize());
	}
}
