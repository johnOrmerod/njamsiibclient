package com.w3p.im.iib.mon.client.utils.io.data;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.BROWSE;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.DEFAULT_NAME_PATTERN;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.GET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class TestReadQueueRequest {
	
	private static final String content = "content";
	private static final String namePattern = "pattern_%s.txt";
	
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test	
	public void testCanCreateRequestForBrowseModeAsDefault() {
		IReadQueueRequest req = new ReadQueueRequest();
		req.setContent(content)
		.setNamePattern(namePattern)
		.addOption("optionA", Boolean.TRUE);
		req.setMsgSelector("team =  'Spurs'");
		
		ConnectionData connData = new ConnectionData();
		req.setConnectionData(connData);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(namePattern));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Boolean)req.getOption("optionA").get());
		assertEquals(BROWSE, req.getMode());
		assertEquals("team =  'Spurs'", req.getMsgSelector().get());
		assertNotNull(connData);
	}
	
	@Test
	public void testCanCreateRequestForGetMode() {
		IReadQueueRequest req = new ReadQueueRequest();
		req.setContent(content)
		.addOption("optionA", Integer.MAX_VALUE);
		req.setMode(GET)
		.setMsgSelector("team =  'Spurs'");
		
		ConnectionData connData = new ConnectionData();
		req.setConnectionData(connData);

		assertNotNull(req);
		assertTrue(req.getContent().equals(content));
		assertTrue(req.getNamePattern().equals(DEFAULT_NAME_PATTERN));
		assertTrue(req.getOption("optionA").isPresent());
		assertTrue((Integer)req.getOption("optionA").get() == Integer.MAX_VALUE);
		assertEquals(GET, req.getMode());
		assertEquals("team =  'Spurs'", req.getMsgSelector().get());
		assertNotNull(connData);
	}
}
