package com.w3p.im.iib.mon.client.utils.io.jms;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

public class CreateMocksForJMS {

	public CreateMocksForJMS() {
		// TODO Auto-generated constructor stub
	}
	
	public MockedJmsObjects createForJMSConnection() throws JMSException {
		MockedJmsObjects mjo = new MockedJmsObjects();
		JmsConnectionFactory connFac = mock(JmsConnectionFactory.class);				
		JmsFactoryFactory ff = mock(JmsFactoryFactory.class);
		when(ff.createConnectionFactory()).thenReturn(connFac);

		JmsFactoryFactoryWrapper jffw = mock(JmsFactoryFactoryWrapper.class);
		when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenReturn(ff);

		/*		JMSContext context = mock(JMSContext.class);
		when(connFac.createContext(JMSContext.AUTO_ACKNOWLEDGE)).thenReturn(context); */
		
		Connection jmsConnection = mock(Connection.class);
		when(connFac.createConnection()).thenReturn(jmsConnection);
		
		boolean transacted = false; // FIXME pass as parameter 
		Session jmsSession = mock(Session.class);
		when(jmsConnection.createSession(transacted, Session.AUTO_ACKNOWLEDGE)).thenReturn(jmsSession);		

		Queue jmsQueue = mock(Queue.class);
		when(jmsSession.createQueue(anyString())).thenReturn(jmsQueue);
		
		TextMessage message = mock(TextMessage.class);
		when(jmsSession.createTextMessage()).thenReturn(message);

//		Destination jmsDest = mock(Destination.class);	
		
		MessageProducer producer = mock(MessageProducer.class);
		when(jmsSession.createProducer(jmsQueue)).thenReturn(producer);
		
		mjo.setJmsConnectionFactory(connFac);
		mjo.setJmsConnnection(jmsConnection);
		mjo.setJmsDestination(jmsQueue);
		mjo.setJmsSession(jmsSession);
		mjo.setJmsFactoryFactory(ff);
		mjo.setJmsFactoryFactoryWrapper(jffw);
		mjo.setProducer(producer);
		mjo.setJmsQueue(jmsQueue);
		mjo.setMessage(message);
		
		return mjo;
	}


}
