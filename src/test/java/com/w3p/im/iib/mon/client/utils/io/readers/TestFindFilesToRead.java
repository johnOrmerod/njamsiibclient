package com.w3p.im.iib.mon.client.utils.io.readers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.w3p.im.iib.mon.client.utils.io.data.IReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.ReadFileRequest;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public class TestFindFilesToRead {
	
	private static final Path inputFolderPath = Paths.get("src","test","resources", "inputFolder");

	@ClassRule
	public static TemporaryFolder tempInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder emptyInputFolder = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder fileInputFolder = new TemporaryFolder();

	@BeforeClass
	public static void copyTestFiles() throws IOException {
		// Copy test files to a jUnit temp folder		
		DirectoryStream<Path> filesList = Files.newDirectoryStream(inputFolderPath); // List the source files
		for (Path inFilePath : filesList) {
			String fileName = inFilePath.toFile().getName();
			File tempFile = tempInputFolder.newFile(fileName);
			Path tempFilePath = Paths.get(tempFile.getAbsolutePath());
			Files.copy(inFilePath, tempFilePath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
		}
		
		// Create a file in fileInputFolder for testing 'folder not folder'
		fileInputFolder.newFile("InputFolder");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFindFilesToRead() {
		FindFilesToRead finder = new FindFilesToRead();
		assertNotNull(finder);
	}

	@Test
	public void testCanFindFilesToRead() {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.setSource(inputFolder);
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp); // FIXME rename to FileToBeRead
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(10, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanFindFilesToReadByNamePattern() {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.setSource(inputFolder);
		req.setNamePattern("03_HTTP*.xml");
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp);
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(1, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanHandleFilesToReadAreExcludedByNamePattern() {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputFolder = tempInputFolder.getRoot().getAbsolutePath();
		req.setSource(inputFolder);
		req.setNamePattern("*.pdf");
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp);
		
		assertNotNull(ftbc);
		assertTrue(resp.isSuccess());
		assertFalse(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
	}
	
	@Test
	public void testCanHandleNoInputFilesToRead() {	
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputFolder = emptyInputFolder.getRoot().getAbsolutePath();
		req.setSource(inputFolder);
		req.setNamePattern("*.pdf");
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp);

		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals(String.format("WARN: Input folder '%s' is empty. No files to read.\r\n", inputFolder)));
	}
	
	@Test
	public void testCanHandleInputFolderNotExist() {
		Response resp = new Response();		
		IReadFileRequest req = new ReadFileRequest();
		
		Path inputFolderPath = Paths.get("src","test","resources", "notFound"); 
		String inputFolder = inputFolderPath.toFile().getAbsolutePath();
		req.setSource(inputFolder);
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp);
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals(String.format("ERROR: Input folder '%s' does not exist.\r\n", inputFolder)));

	}
	

	@Test
	public void testCanHandleInputFolderNotFolder() {
		Response resp = new Response();
		IReadFileRequest req = new ReadFileRequest();
		String inputRoot = fileInputFolder.getRoot().getAbsolutePath();
		String inputFolder = new File(inputRoot, "InputFolder").getAbsolutePath();
		req.setSource(inputFolder);
		FindFilesToRead finder = new FindFilesToRead();		
		FilesToRead ftbc = finder.find(req, resp);
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		assertEquals(0, ftbc.getFiles().size());
		assertTrue(resp.getAllMessages().get().equals(String.format("ERROR: Input folder '%s' is not a directory.\r\n", inputFolder)));
		
	}

	@Ignore("need a mock")
	@Test
	public void testCanHandleThrownIOException() {
		System.out.println("Can't find an easy way to mock 'Files.newDirectoryStream(inputFolder)' and throw an IOException"); 
	}
}
