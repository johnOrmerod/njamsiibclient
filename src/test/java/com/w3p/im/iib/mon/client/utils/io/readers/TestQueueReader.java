package com.w3p.im.iib.mon.client.utils.io.readers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.NAME_USING_MSG_PROPERTIES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.client.utils.io.data.IReadQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.RequestTestsHelper;
import com.w3p.im.iib.mon.client.utils.io.data.Response;

public class TestQueueReader {
	
	private RequestTestsHelper helper = new RequestTestsHelper();
//	private CreateMocksForJMS mocksForJMS = new CreateMocksForJMS();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testCanCreateMessagesReader() {
		IQueueReader mr = new QueueReader();
		assertNotNull(mr);
	}
	
	// Integration test - uses MQ
	@Test
	public void intTestCanReadMessagesUsingMsgPropertiesAndForID() {
		Response resp = new Response();
		IReadQueueRequest req = helper.createRequestDataInForIB10QMGR();
		req.addOption(NAME_USING_MSG_PROPERTIES, Boolean.TRUE);
		req.setMsgSelector("team = 'Spurs'");
		req.setNamePattern("%s.txt");
		
		QueueReader qr = new QueueReader();
		qr.init(req, resp);
		assertTrue(resp.isSuccess());
		IContent contents = qr.readAll(req, resp);
		assertTrue(resp.isSuccess());
		assertNotNull(qr); 
		assertNotNull(qr.getJmsDest());
		assertEquals("queue:///NJAMS.UTILS.TEST.IN?WMQConstants.WMQ_PERSISTENCE+=+-1", qr.getJmsDest().toString());
		
		String msg = contents.getContent("team-Spurs.txt");
		assertNotNull(msg);
	}
	
	// Integration test - uses MQ
	@Test
	public void intTestCanReadMessagesNotUsingMsgPropertiesThoughForID() {
		Response resp = new Response();
		IReadQueueRequest req = helper.createRequestDataInForIB10QMGR();
		req.addOption(NAME_USING_MSG_PROPERTIES, Boolean.TRUE);
		req.setNamePattern("%s.txt");
		
		QueueReader qr = new QueueReader();
		qr.init(req, resp);
		assertTrue(resp.isSuccess());
		IContent contents = qr.readAll(req, resp);
		assertTrue(resp.isSuccess());
		assertNotNull(qr); 
		assertNotNull(qr.getJmsDest());
		assertEquals("queue:///NJAMS.UTILS.TEST.IN?WMQConstants.WMQ_PERSISTENCE+=+-1", qr.getJmsDest().toString());
		
		String msg = contents.getContent("team-Spurs.txt");
		assertNotNull(msg);
	}
}
