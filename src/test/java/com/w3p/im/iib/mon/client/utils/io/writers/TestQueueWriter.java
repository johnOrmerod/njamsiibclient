package com.w3p.im.iib.mon.client.utils.io.writers;

import static com.w3p.im.iib.mon.client.constants.IClientConstants.GEN_CORREL_ID;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.TEXT;
import static com.w3p.im.iib.mon.client.constants.IClientConstants.UTF_8;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;
import com.w3p.im.iib.mon.client.utils.io.data.ConnectionData;
import com.w3p.im.iib.mon.client.utils.io.data.IWriteQueueRequest;
import com.w3p.im.iib.mon.client.utils.io.data.RequestTestsHelper;
import com.w3p.im.iib.mon.client.utils.io.data.Response;
import com.w3p.im.iib.mon.client.utils.io.jms.CreateMocksForJMS;
import com.w3p.im.iib.mon.client.utils.io.jms.JmsFactoryFactoryWrapper;
import com.w3p.im.iib.mon.client.utils.io.jms.MockedJmsObjects;

public class TestQueueWriter {
	
	private RequestTestsHelper helper = new RequestTestsHelper();
	private CreateMocksForJMS mocksForJMS = new CreateMocksForJMS();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testCanCreateMessagesWriter() {
		QueueWriter qw = new QueueWriter();
		assertNotNull(qw);
	}
	
	@Test
	public void testCanInitMessagesWriter() {
		QueueWriter qw = new QueueWriter();
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		qw.init(req, resp);

		assertTrue(resp.isSuccess());
		assertNotNull(qw); 
		assertNotNull(qw.producer);
		assertNotNull(qw.getJmsDest());
		assertEquals("queue:///NJAMS.UTILS.TEST.OUT?WMQConstants.WMQ_PERSISTENCE+=+-1", qw.getJmsDest().toString());		
	}
	
	@Test
	public void testCanCreateMessage() throws UnsupportedEncodingException {
		Response resp = new Response();
		String msgContent = "This is a test message";
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.addOption(GEN_CORREL_ID, Boolean.TRUE);
		req.addMsgProperty("team", "Spurs");
		
		try (QueueWriter qw = new QueueWriter()) {
			qw.init(req, resp);	
			assertTrue(resp.isSuccess());
			// Create a Text msg
			Message msg = qw.createTextMessage(msgContent);			
			assertTrue(resp.isSuccess());
			assertTrue(msg instanceof TextMessage);			
			String msgText = ((TextMessage)msg).getText();
			assertEquals(msgContent, msgText);
			
			qw.addMessageExtras(msg); 
			assertNotNull(msg.getJMSCorrelationID());
			assertEquals("Spurs", msg.getStringProperty("team"));
			// Create a byte-array message
			msg = qw.createBytesMessage(msgContent);
			assertTrue(resp.isSuccess());
			assertTrue(msg instanceof BytesMessage);
			
			// Get bytes msg as text
			BytesMessage bytesMsg =(BytesMessage)msg; 
			bytesMsg.reset();
			int msgLength = new Long(bytesMsg.getBodyLength()).intValue();
			byte[] msgBytes = new byte[msgLength];
			bytesMsg.readBytes(msgBytes);
			msgText = new String(msgBytes,UTF_8);		
			assertEquals(msgContent, msgText);
			
			qw.addMessageExtras(bytesMsg);
			assertNotNull(msg.getJMSCorrelationID());
			assertEquals("Spurs", msg.getStringProperty("team"));
			
		} catch (JMSException e) {
			fail(String.format("Unplanned JMSExcption occured: %s", e.toString()));
		}
	}

	// Integration test - uses MQ
	@Test
	public void intTestCanWriteMessageToQueue() {		
		Response resp = new Response();
		resp.setFilesToBeRead(1);
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.addOption(GEN_CORREL_ID, Boolean.TRUE);
		req.addMsgProperty("team", "Spurs");
		
		try (QueueWriter qw = new QueueWriter()) {
			qw.init(req, resp);
			assertTrue(resp.isSuccess());
			// This will create a byte-array message, by default. Use IWriteQueueRequest.TEXT for text format.
			String msgContent = "This is a test message";
			qw.write("myData", msgContent, resp); // FIXME not sure if a 'name' value is useful - use in error messages

			assertTrue(resp.isSuccess());
			assertEquals(1,resp.getFilesToBeRead());
			assertEquals(1,resp.getFilesWritten());
			assertEquals(0,resp.getFilesInError());
			assertEquals(0,resp.getFilesDeleted());
			assertFalse(resp.getAllMessages().isPresent());
		}
	}

	@Test
	public void testMockCanInitMessagesWriter() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		Connection jmsConnection = mjo.getJmsConnnection();
		Session jmsSession = mjo.getJmsSession();
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw); 
			//TODO check response
			assertNotNull(qw);
			assertNotNull(qw.producer);
			assertNotNull(qw.getJmsDest());
			assertEquals("queue:///NJAMS.UTILS.TEST.OUT", qw.getJmsDest().toString());	
		}
		
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(jff).createConnectionFactory();
		
		verify(connFac, times(4)).setStringProperty(anyString(), anyString());
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(connFac, times(1)).setIntProperty(WMQConstants.WMQ_PORT, req.getConnectionData().getPort());
		verify(connFac, times(1)).setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, false);
		
		verify(jmsConnection).createSession(false, Session.AUTO_ACKNOWLEDGE);
		verify(jmsSession).createProducer(mjo.getJmsDestination());
		verify(jmsSession).createQueue("queue:///NJAMS.UTILS.TEST.OUT?WMQConstants.WMQ_PERSISTENCE = -1");		
		verify(jmsSession).close();			
	}
	
	@Test
	public void testMockCanWriteMessage() throws JMSException {		
		Response resp = new Response();
		resp.setFilesToBeRead(1);
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.setMsgType(TEXT);
		req.addOption(GEN_CORREL_ID, Boolean.TRUE); // Make sure this code is used		
		req.addMsgProperty("team", "Spurs");
		
		ConnectionData connData = req.getConnectionData();
		
		// Mocking JMS connection
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
	
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Session jmsSession = mjo.getJmsSession();
		Connection jmsConnection = mjo.getJmsConnnection();
		
		TextMessage message = mjo.getMessage();
		when(jmsSession.createTextMessage()).thenReturn(message);
	
		String fileName = "testFile";
		String msgContent = "This is a test message";
		try (QueueWriter qw = new QueueWriter()) {
			qw.init(req, resp, jffw);
			assertTrue(resp.isSuccess());
			qw.write(fileName, msgContent, resp);
		}
		assertTrue(resp.isSuccess());
		assertEquals(1,resp.getFilesToBeRead());
		assertEquals(1,resp.getFilesWritten());
		assertEquals(0,resp.getFilesInError());
		assertEquals(0,resp.getFilesDeleted());
		assertFalse(resp.getAllMessages().isPresent());
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsSession).createQueue(anyString());
		verify(jmsSession).createTextMessage();
		verify(message).setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);
		verify(message).setText(msgContent);		
		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		verify(message).setStringProperty("team", "Spurs");
		verify(jmsSession).close(); // Check that the auto-close worked.
		verify(jmsConnection).close();
	}

	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingFactoryFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		
		when(jffw.getFactory(WMQConstants.WMQ_PROVIDER)).thenThrow(new JMSException("Error creating the JmsFactoryFactory."));		

		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
		}
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Error creating the JmsFactoryFactory.'.\r\n"));

		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsFactoryFactory jff = mjo.getJmsFactoryFactory();
		
		when(jff.createConnectionFactory()).thenThrow(new JMSException("Error creating the JmsConnectionFactory."));		
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			//TODO check response
			assertNotNull(qw);
		}
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Error creating the JmsConnectionFactory.'.\r\n"));

		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(jff).createConnectionFactory();
	}

	@Test
	public void testMockCanHandleJMSExceptionWhenConfiguringConnectionFactory() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = req.getConnectionData();
		
		// To handle a 'when' on a method that returns 'null' (e.g a setter), see: https://stackoverflow.com/questions/29537574/mockito-error-is-not-applicable-for-the-arguments-void
		doThrow(new JMSException("Error occurred when setting an Integer property for the Connection Factory.")).
			when(connFac).setIntProperty(WMQConstants.WMQ_PORT, connData.getPort());
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
		}
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Error occurred when setting an Integer property for the Connection Factory.'.\r\n"));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_PORT, connData.getPort());			
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSConnection() throws JMSException {

		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();

		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = req.getConnectionData();
		
		Connection jmsConnection = mjo.getJmsConnnection();
		
		doThrow(new JMSException("Internal error occurred when creating the JMS Connection.")).
		when(connFac).createConnection(); 

		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
		}			

		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Internal error occurred when creating the JMS Connection.'.\r\n"));

		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(connFac).createConnection();
		verify(jmsConnection, times(0)).close(); // Connection should be null

	}

	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSSession() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = req.getConnectionData();
		
		Connection jmsConnection = mjo.getJmsConnnection();
		Session jmsSession = mjo.getJmsSession();

		doThrow(new JMSException("Internal error occurred when creating the JMS session.")).
			when(jmsConnection).createSession(false, Session.AUTO_ACKNOWLEDGE); 
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
		}
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Internal error occurred when creating the JMS session.'.\r\n"));

		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsConnection).createSession(false, Session.AUTO_ACKNOWLEDGE);
		verify(jmsSession,times(0)).close(); // Check that the auto-close worked - but not for jmsSession.
		verify(jmsConnection).close();
	}

	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingJMSDestination() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();

		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		ConnectionData connData = req.getConnectionData();
		
		Connection jmsConnection = mjo.getJmsConnnection();
		Session jmsSession = mjo.getJmsSession();
		
		doThrow(new JMSException("Internal error occurred when creating the Queue object.")).
			when(jmsSession).createQueue(anyString());
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);		
		}
		
		assertFalse(resp.isSuccess());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error configuring the JMS environment. "
				+ "Reason: 'javax.jms.JMSException: Internal error occurred when creating the Queue object.'.\r\n"));

		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsConnection).createSession(false, Session.AUTO_ACKNOWLEDGE);
		verify(jmsSession).createQueue(anyString());
		verify(jmsSession).close(); // Check that the auto-close worked.
		verify(jmsConnection).close();
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenCreatingTextMessage() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();

		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.setMsgType(TEXT);
		ConnectionData connData = req.getConnectionData();		
		
		Connection jmsConnection = mjo.getJmsConnnection();
		Session jmsSession = mjo.getJmsSession();
		
		when(jmsSession.createTextMessage()).
			thenThrow(new JMSException("Internal error occurred when creating the message."));		
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
			assertTrue(resp.isSuccess());
			qw.write("name", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());		 
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error creating a JMS Text Message for 'name'. "
				+ "Reason: 'javax.jms.JMSException: Internal error occurred when creating the message.'.\r\n"));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsSession).createQueue(anyString());
		verify(jmsSession).createTextMessage();
		verify(jmsSession).close(); // Check that the auto-close worked.
		verify(jmsConnection).close();		
	}
	
	@Test
	public void testMockCanHandleJMSExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();
		TextMessage message = mjo.getMessage();
		
		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.setMsgType(TEXT);
		req.addOption(GEN_CORREL_ID, Boolean.TRUE); // Make sure this is set	
		ConnectionData connData = req.getConnectionData();
		
		Session jmsSession = mjo.getJmsSession();
		Connection jmsConnection = mjo.getJmsConnnection();
		
		doThrow(new JMSException("Error occurred when setting the correlationId for the message.")).
			when(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
			assertTrue(resp.isSuccess());
			qw.write("name", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Error creating a JMS Text Message for 'name'. "
				+ "Reason: 'javax.jms.JMSException: Error occurred when setting the correlationId for the message.'.\r\n"));
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsSession).createQueue(anyString());
		verify(jmsSession).createTextMessage();
		verify(message).setText(anyString());
		verify(message).setJMSCorrelationIDAsBytes(anyString().getBytes());
		verify(jmsSession).close(); // Check that the auto-close worked.
		verify(jmsConnection).close();
	}
	
	@Test
	public void testMockCanHandleUnhandledExceptionWhenSettingMessageProperties() throws JMSException {
		
		MockedJmsObjects mjo = mocksForJMS.createForJMSConnection();
		JmsFactoryFactoryWrapper jffw = mjo.getJmsFactoryFactoryWrapper();
		JmsConnectionFactory connFac = mjo.getJmsConnectionFactory();

		Queue jmsQueue = mjo.getJmsQueue();
		when(jmsQueue.toString()).thenReturn("queue:///NJAMS.UTILS.TEST.OUT");
		
		Response resp = new Response();
		IWriteQueueRequest req = helper.createRequestDataOutForIB10QMGR();
		req.setMsgType(TEXT);
		req.addOption(GEN_CORREL_ID, Boolean.TRUE); // Make sure this code is used		
		ConnectionData connData = req.getConnectionData();
		
		Session jmsSession = mjo.getJmsSession();
		Connection jmsConnection = mjo.getJmsConnnection();
		
		TextMessage message = mock(TextMessage.class);		
		when(jmsSession.createTextMessage()).thenReturn(message);
		
		doThrow(new RuntimeException("Something stupid happened!")).
			when(message).setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208); 
		
		try(QueueWriter qw = new QueueWriter()) { 
			qw.init(req, resp, jffw);
			assertNotNull(qw);
			assertTrue(resp.isSuccess());
			qw.write("name", "<?xml >", resp);
		}
		
		assertFalse(resp.isSuccess());
		assertEquals(1, resp.getFilesInError());
		assertTrue(resp.getAllMessages().isPresent());
		String allMessages = resp.getAllMessages().get();
		assertTrue(allMessages.equals("ERROR: Unhandled error creating the JMS Text Message for file 'name'. "
				+ "Reason: 'java.lang.RuntimeException: Something stupid happened!'.\r\n"));
		
		// Follow the code path
		
		// Follow the code path
		verify(jffw).getFactory(WMQConstants.WMQ_PROVIDER);
		verify(connFac).setStringProperty(WMQConstants.WMQ_HOST_NAME, connData.getHost());
		verify(connFac).setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		verify(jmsSession).createQueue(anyString());
		verify(jmsSession).createTextMessage();
		verify(message).setIntProperty(WMQConstants.JMS_IBM_CHARACTER_SET, 1208);		
		verify(jmsSession).close(); // Check that the auto-close worked.
		verify(jmsConnection).close();
	}

}
