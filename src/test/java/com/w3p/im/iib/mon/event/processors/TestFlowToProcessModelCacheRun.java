package com.w3p.im.iib.mon.event.processors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.im.njams.sdk.common.Path;
import com.im.njams.sdk.model.ProcessModel;

public class TestFlowToProcessModelCacheRun {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateFlowToProcessModelCache() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		assertNotNull(cache);		
	}

	@Test
	public void testEmptyPathCacheReturnsNull() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		Path nullPath = cache.getPathForFlow("flowName");
		assertNull(nullPath);
	}
	
	@Test
	public void testPathCacheIsEmpty() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		assertTrue(cache.isEmpty());
	}
	
	@Test
	public void testPathCacheNotEmpty() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		assertFalse(cache.isEmpty());
	}
	
	@Test
	public void testPathNotFoundReturnsNull() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		Path notFound = cache.getPathForFlow("flow1");
		assertNull(notFound);
	}
	
	
	@Test
	public void testCanAddPathForFlow() {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		Path flow1Path = cache.getPathForFlow("Flow1");
		assertNotNull(flow1Path);
		assertEquals(2, flow1Path.getParts().size());
		assertEquals("App1", flow1Path.getParts().get(0));
		assertEquals("Flow1", flow1Path.getParts().get(1));
		assertEquals(">App1>Flow1>", flow1Path.toString());
		// Subflow
		cache.addPathForFlow("Flow1.Subflow1", Arrays.asList("App1", "Subflow1")); // FIXME The nodeLabel will always contain [App|Lib|api].MsgFlow.[Subflow..] need to crearte key wityh App|Lib
		Path subflow1Path = cache.getPathForFlow("Flow1.Subflow1");
		assertNotNull(subflow1Path);
		assertEquals(2, subflow1Path.getParts().size());
		assertEquals("App1", subflow1Path.getParts().get(0));
		assertEquals("Subflow1", subflow1Path.getParts().get(1));
		assertEquals(">App1>Subflow1>", subflow1Path.toString());
	}
	
	@Test
	public void testCanSerialiseTheCache() throws IOException, ClassNotFoundException {
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
		Path flow1Path = cache.getPathForFlow("Flow1");
		assertNotNull(flow1Path);
		assertEquals(2, flow1Path.getParts().size());
		assertEquals("App1", flow1Path.getParts().get(0));
		assertEquals("Flow1", flow1Path.getParts().get(1));
		assertEquals(">App1>Flow1>", flow1Path.toString());
		// Subflow
		cache.addPathForFlow("Flow1.Subflow1", Arrays.asList("App1", "Subflow1")); // FIXME The nodeLabel will always contain [App|Lib|api].MsgFlow.[Subflow..] need to crearte key wityh App|Lib
		Path subflow1Path = cache.getPathForFlow("Flow1.Subflow1");
		assertNotNull(subflow1Path);
		assertEquals(2, subflow1Path.getParts().size());
		assertEquals("App1", subflow1Path.getParts().get(0));
		assertEquals("Subflow1", subflow1Path.getParts().get(1));
		assertEquals(">App1>Subflow1>", subflow1Path.toString());
		
		FileOutputStream fileOutputStream
	      = new FileOutputStream("cache.txt");
	    ObjectOutputStream objectOutputStream 
	      = new ObjectOutputStream(fileOutputStream);
	    objectOutputStream.writeObject(cache);
	    objectOutputStream.flush();
	    objectOutputStream.close();
	    
	    FileInputStream fileInputStream
	      = new FileInputStream("cache.txt");
	    ObjectInputStream objectInputStream
	      = new ObjectInputStream(fileInputStream);
	    FlowToProcessModelCacheRun cache2 = (FlowToProcessModelCacheRun) objectInputStream.readObject();
	    objectInputStream.close(); 
	 
	    Path subflow1Path2 = cache2.getPathForFlow("Flow1.Subflow1");
		assertNotNull(subflow1Path2);
		assertEquals(2, subflow1Path2.getParts().size());
		assertEquals("App1", subflow1Path2.getParts().get(0));
		assertEquals("Subflow1", subflow1Path2.getParts().get(1));
		assertEquals(">App1>Subflow1>", subflow1Path2.toString());
		
	}
	
	@Test
	public void testCanGetProcessModelNameAndIdForFlow() {
		// Msgflow -> ProcessModel
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
//		String  pmName = cache.getProcessModelNameForFlow("Flow1");
//		assertNotNull(pmName);
//		assertEquals("Flow1", pmName);

		// Subflow -> SubProcessModel
		cache.addPathForFlow("Subflow1", Arrays.asList("Lib1", "SubflowL"));
//		String subProcessNameLib = cache.getProcessModelNameForFlow("Subflow1");
//		assertNotNull(subProcessNameLib);
//		assertEquals("SubflowL", subProcessNameLib);
		
		cache.addPathForFlow("Flow1.SubflowX", Arrays.asList("App1", "Subflow1"));
//		String subProcessName = cache.getProcessModelNameForFlow("Flow1.SubflowX");
//		assertNotNull(subProcessName);
//		assertEquals("Subflow1", subProcessName);
	}
	
	@Test
	public void testCanAddProcessModelWithSubflow() {
		
		ProcessModel pm = mock(ProcessModel.class);
//		when(pm.)
		
		
		
		// Msgflow -> ProcessModel
		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
//		String  pmName = cache.getProcessModelNameForFlow("Flow1");
//		assertNotNull(pmName);
//		assertEquals("Flow1", pmName);

		// Subflow -> SubProcessModel
		cache.addPathForFlow("Subflow1", Arrays.asList("Lib1", "SubflowL"));
//		String subProcessNameLib = cache.getProcessModelNameForFlow("Subflow1");
//		assertNotNull(subProcessNameLib);
//		assertEquals("SubflowL", subProcessNameLib);
		
		cache.addPathForFlow("Flow1.SubflowX", Arrays.asList("App1", "Subflow1"));
//		String subProcessName = cache.getProcessModelNameForFlow("Flow1.SubflowX");
//		assertNotNull(subProcessName);
//		assertEquals("Subflow1", subProcessName);
	}
	

//	@Test
//	public void testCanClearContents() {
//		FlowToProcessModelCacheRun cache = new FlowToProcessModelCacheRun();
//		cache.addPathForFlow("Flow1", Arrays.asList("App1", "Flow1"));
//		String  pmName = cache.getProcessModelNameForFlow("Flow1");
//		assertEquals("Flow1", pmName);
//		
//		cache.clear();
//		assertNull(cache.getPathForFlow("Flow1"));
//		assertNull(cache.getProcessModelNameForFlow("Flow1"));		
//	}
}
