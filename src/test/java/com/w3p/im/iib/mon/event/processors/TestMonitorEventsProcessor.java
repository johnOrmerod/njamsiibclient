package com.w3p.im.iib.mon.event.processors;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.im.njams.sdk.Njams;
import com.w3p.im.iib.mon.exceptions.monitoring.MonitoringException;
import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues;
import com.w3p.im.iib.mon.monitor.data.MonitoringEvent;
import com.w3p.im.iib.mon.monitor.data.groovy.ParseMonitoringEvent;

public class TestMonitorEventsProcessor {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	private static final String START_ACTIVITY_TYPE = "start";
	private static final String START_ACTIVITY_NAME = "Start";
	
	private final String event1 = "<wmb:event xmlns:wmb=\"http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event\">  <wmb:eventPointData>   <wmb:eventData wmb:productVersion=\"10008\" wmb:eventSchemaVersion=\"6.1.0.3\" wmb:eventSourceAddress=\"HTTP Input.transaction.Start\">    <wmb:eventIdentity wmb:eventName=\"HTTP Input.TransactionStart\"/>    <wmb:eventSequence wmb:creationTime=\"2018-05-15T19:36:25.301642Z\" wmb:counter=\"1\"/>    <wmb:eventCorrelation wmb:localTransactionId=\"9c8c4528-5e8a-46dc-9d12-eb0bf3dc4075-2\" wmb:parentTransactionId=\"\" wmb:globalTransactionId=\"\"/>   </wmb:eventData>   <wmb:messageFlowData>    <wmb:broker wmb:name=\"PETS_AT_HOME\" wmb:UUID=\"47ea0eeb-dc5d-47c5-a565-0bb5ec322503\"/>    <wmb:executionGroup wmb:name=\"appTesting\" wmb:UUID=\"8b5a1201-f6dd-4536-97ad-3c0102bb9e24\"/>    <wmb:messageFlow wmb:uniqueFlowName=\"PETS_AT_HOME.appTesting.HTTPInputApplication.HTTPInputMessageFlow\" wmb:name=\"HTTPInputMessageFlow\" wmb:UUID=\"80a76821-ec12-42c7-b920-905f84e2d9d4\" wmb:threadId=\"15300\"/>    <wmb:node wmb:nodeLabel=\"HTTP Input\" wmb:nodeType=\"ComIbmWSInputNode\"/>   </wmb:messageFlowData>  </wmb:eventPointData>  <wmb:bitstreamData>   <wmb:bitstream wmb:encoding=\"base64Binary\">ew0KIklucHV0RmllbGQxIjoiSGVsbG8iLA0KIklucHV0RmllbGQyIjoibkpBTVMgeSINCn0=</wmb:bitstream>  </wmb:bitstreamData> </wmb:event>";
	private final String event2 = "<wmb:event xmlns:wmb=\"http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event\">  <wmb:eventPointData>   <wmb:eventData wmb:productVersion=\"10008\" wmb:eventSchemaVersion=\"6.1.0.3\" wmb:eventSourceAddress=\"Mapping.terminal.in\">    <wmb:eventIdentity wmb:eventName=\"Mapping.InTerminal\"/>    <wmb:eventSequence wmb:creationTime=\"2018-05-15T19:36:25.303990Z\" wmb:counter=\"2\"/>    <wmb:eventCorrelation wmb:localTransactionId=\"9c8c4528-5e8a-46dc-9d12-eb0bf3dc4075-2\" wmb:parentTransactionId=\"\" wmb:globalTransactionId=\"\"/>   </wmb:eventData>   <wmb:messageFlowData>    <wmb:broker wmb:name=\"PETS_AT_HOME\" wmb:UUID=\"47ea0eeb-dc5d-47c5-a565-0bb5ec322503\"/>    <wmb:executionGroup wmb:name=\"appTesting\" wmb:UUID=\"8b5a1201-f6dd-4536-97ad-3c0102bb9e24\"/>    <wmb:messageFlow wmb:uniqueFlowName=\"PETS_AT_HOME.appTesting.HTTPInputApplication.HTTPInputMessageFlow\" wmb:name=\"HTTPInputMessageFlow\" wmb:UUID=\"80a76821-ec12-42c7-b920-905f84e2d9d4\" wmb:threadId=\"15300\"/>    <wmb:node wmb:nodeLabel=\"Mapping\" wmb:nodeType=\"ComIbmMSLMappingNode\" wmb:terminal=\"in\"/>   </wmb:messageFlowData>  </wmb:eventPointData>  <wmb:bitstreamData>   <wmb:bitstream wmb:encoding=\"base64Binary\">ew0KIklucHV0RmllbGQxIjoiSGVsbG8iLA0KIklucHV0RmllbGQyIjoibkpBTVMgeSINCn0=</wmb:bitstream>  </wmb:bitstreamData> </wmb:event>";
	private final String event3 = "<wmb:event xmlns:wmb=\"http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event\">  <wmb:eventPointData>   <wmb:eventData wmb:productVersion=\"10008\" wmb:eventSchemaVersion=\"6.1.0.3\" wmb:eventSourceAddress=\"HTTP Input.transaction.End\">    <wmb:eventIdentity wmb:eventName=\"HTTP Input.TransactionEnd\"/>    <wmb:eventSequence wmb:creationTime=\"2018-05-15T19:36:25.830027Z\" wmb:counter=\"3\"/>    <wmb:eventCorrelation wmb:localTransactionId=\"9c8c4528-5e8a-46dc-9d12-eb0bf3dc4075-2\" wmb:parentTransactionId=\"\" wmb:globalTransactionId=\"\"/>   </wmb:eventData>   <wmb:messageFlowData>    <wmb:broker wmb:name=\"PETS_AT_HOME\" wmb:UUID=\"47ea0eeb-dc5d-47c5-a565-0bb5ec322503\"/>    <wmb:executionGroup wmb:name=\"appTesting\" wmb:UUID=\"8b5a1201-f6dd-4536-97ad-3c0102bb9e24\"/>    <wmb:messageFlow wmb:uniqueFlowName=\"PETS_AT_HOME.appTesting.HTTPInputApplication.HTTPInputMessageFlow\" wmb:name=\"HTTPInputMessageFlow\" wmb:UUID=\"80a76821-ec12-42c7-b920-905f84e2d9d4\" wmb:threadId=\"15300\"/>    <wmb:node wmb:nodeLabel=\"HTTP Input\" wmb:nodeType=\"ComIbmWSInputNode\"/>   </wmb:messageFlowData>  </wmb:eventPointData>  <wmb:bitstreamData>   <wmb:bitstream wmb:encoding=\"base64Binary\">ew0KIklucHV0RmllbGQxIjoiSGVsbG8iLA0KIklucHV0RmllbGQyIjoibkpBTVMgeSINCn0=</wmb:bitstream>  </wmb:bitstreamData> </wmb:event>";
	private final List<String> events = new ArrayList();
	private final FlowToProcessModelCacheRun flowToProcessModelCache = new FlowToProcessModelCacheRun();
	
	private Njams njamsClient = null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setup() throws Exception {
		events.add(event1);
		events.add(event2);
		events.add(event3);
	}

	@After
	public void tearDown() throws Exception {
		events.clear();
		njamsClient = null;
	}

	@Test
	public void testSendEventToNjams() throws MonitoringException {
//		MessageEvents events = createMessageEvents();
//		MonitorEventsRequestForQueues mer = createMonitorEventsRequest();
//		njamsClient = new Njams(null, "", "", null);
//		NjamsProject project = njamsClient.createProject();
//		ProcessModel pm = project.createProcessDefinition("pathToMsgFlow");
//		ActivityModel start = pm.addStartActivity(START_ACTIVITY_NAME, START_ACTIVITY_TYPE);
//		ActivityModel activityA = start.transitionTo("nodeA", "mqInputNode");
		
		
//		MonitorEventsProcessor mep = new MonitorEventsProcessor(mer);
//		mep.sendLogMessageToNjams(events);
	}

	private MessageEvents createMessageEvents() throws MonitoringException {
		MessageEvents msgEvents = new MessageEvents();		
		for (String msgEvent : events) {
			MonitoringEvent monEvent = new ParseMonitoringEvent().parse(msgEvent);
			msgEvents.addEvent(monEvent);
//			flowToProcessModelCache.put(monEvent.getMessageFlow(), "pathToMsgFlow"); 
		}
		msgEvents.processEvents();
		return msgEvents;
	}
}
