package com.w3p.im.iib.mon.jms.producers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.monitor.data.MonitoredObject;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeRequest;
import com.w3p.im.iib.mon.topology.data.MonitoringScopeResponse;


public class TestTopologyProducer {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
//	@Test
//	public void testCanHandleNullIntNode() {
//		BrokerProxy bp = null;
//		TopologyCreator topology = new TopologyCreator();
//		assertNotNull("Flow topology is null", topology);
//		TopologyResponse resp = topology.init(bp);
//		assertFalse("Should have failed with IN = null", resp.isSuccess());
//	}
	
//	@Test
//	public void testCanHandleDisconnectedIntNode() throws ConfigManagerProxyLoggedException {
//		BrokerProxy bp = BrokerProxy.getLocalInstance(integrationNodeName);
//		bp.disconnect(); //TODO This has stopped failing!??
//		TopologyCreator topology = new TopologyCreator();
//		assertNotNull("Flow topology is null", topology);
//		TopologyResponse resp = topology.init(bp);
//		assertFalse("Should have failed with IntNode not connected", resp.isSuccess());		
//	}

	@Test
	public void testCanInitTopologyProducer() {
		MonitoringScopeCreator producer = new MonitoringScopeCreator();
		assertNotNull("TopologyProcuder is null", producer);
		MonitoringScopeResponse resp = producer.createScope(null);
		assertTrue(resp.getMessage(), resp.isSuccess());
	}

	@Test
	public void testDetermineScopeForMonitoring() {
		MonitoringScopeCreator topology = new MonitoringScopeCreator();
		// public MonitoringScopeRequest(BrokerProxy integrationNode, IntegrationServer intSvr) {
		MonitoredObject<MonitoredObject<?>> mo = topology.determineScopeForMonitoring(new MonitoringScopeRequest(null, null));
		assertNotNull(mo);
	}

	@Test
	public void testIsRunning() {
//		fail("Not yet implemented");
	}

}
