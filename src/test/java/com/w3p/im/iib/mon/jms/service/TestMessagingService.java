package com.w3p.im.iib.mon.jms.service;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.w3p.im.iib.mon.exceptions.internal.MessagingException;
import com.w3p.im.iib.mon.jms.data.IMessagingService;

public class TestMessagingService {
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanRunMQ() throws MessagingException {
		MqJmsConnectionProperties connProps = new MqJmsConnectionProperties();
		connProps.setMqJmsProviderUrl("JMSBINDCF").setMqJmsConnectionFactory("com.sun.jndi.fscontext.RefFSContextFactory").setMqJmsInitialContextFactory("file:/C:/mqm/JNDI-Directory");
		IMessagingService ms = MessagingServiceFactory.createMessagingServiceForConsumer("LocalBroker", connProps );
		assertTrue(ms != null);
		ms.createConnection("topic");
		// Keep it alive for 220 seconds = time for 11 or 12 flow stats to be collected
		boolean stopListening = false;
		int tries = 0;
		while (!stopListening) {
			try {
				Thread.sleep(20000);				
				if (tries == 100) {
					stopListening = true; ms.close();} tries++;
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();	
				stopListening = true; // Allow graceful exit 
			}
			
		}
	}
	
	@Test
	public void testCanRunActiveMQ() throws MessagingException {
		MqJmsConnectionProperties connProps = new MqJmsConnectionProperties();
		connProps.setMqJmsProviderUrl("tcp://localhost:61616").setMqJmsConnectionFactory("queueConnectionFactory").setMqJmsInitialContextFactory("org.apache.activemq.jndi.ActiveMQInitialContextFactory");
		IMessagingService ms = MessagingServiceFactory.createMessagingServiceForConsumer("nJAMS_event", connProps);
		assertTrue(ms != null);
		ms.createConnection("topic");
		// Keep it alive for 220 seconds = time for 11 or 12 flow stats to be collected
		boolean stopListening = false;
		int tries = 0;
		while (!stopListening) {
			try {
				Thread.sleep(20000);				
				if (tries == 100) {
					stopListening = true; ms.close();} tries++;
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();	
				stopListening = true; // Allow graceful exit 
			}
			
		}
	}

}
