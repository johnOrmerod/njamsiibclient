package com.w3p.im.iib.mon.monitor.data.groovy;

import static org.junit.Assert.*

import com.w3p.im.iib.mon.monitor.data.MonitorEventsRequestForQueues
import javax.management.InstanceOfQueryExp

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

class TestParseMonitoringRequest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseMonitoringRequestWithTracing() {
		def request = '''
			<monitor>
			    <events tracing="true">
			        <integrationNodeName>IB9NODE</integrationNodeName>
			        <integrationServerName>PagerExecutionGroup</integrationServerName>
			        <flowName>TextMessenger</flowName>
			    </events>
			</monitor>
		'''
		
		MonitorEventsRequestForQueues result = new ParseMonitoringRequest().parseMonitoringRequest(request)
		assert result != null
		assertTrue(result.isTracingEnabled())
		
	}
	
	@Test
	public void testParseMonitoringRequestWithoutTracing() {
		def request = '''
			<monitor>
			    <events tracing="false">
			        <integrationNodeName>IB9NODE</integrationNodeName>
			        <integrationServerName>PagerExecutionGroup</integrationServerName>
			        <flowName>TextMessenger</flowName>
			    </events>
			</monitor>
		'''
		
		MonitorEventsRequestForQueues result = new ParseMonitoringRequest().parseMonitoringRequest(request)
		assert result != null
		assertFalse(result.isTracingEnabled())
		
	}

}
