package com.w3p.im.iib.mon.monitor.profile.groovy

import static org.junit.Assert.*

import java.util.ArrayList
import java.util.Enumeration
import java.util.List
import java.util.TreeMap.PrivateEntryIterator

import javax.swing.text.StyledEditorKit.ForegroundAction
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.w3p.api.config.proxy.AdministeredObject
import com.w3p.api.config.proxy.ApplicationProxy
import com.w3p.api.config.proxy.ConfigManagerProxyLoggedException
import com.w3p.api.config.proxy.ConfigManagerProxyPropertyNotInitializedException
import com.w3p.api.config.proxy.ExecutionGroupProxy
import com.w3p.api.config.proxy.FlowProxy
import com.w3p.api.config.proxy.MessageFlowProxy
import com.w3p.api.config.proxy.SubFlowProxy
import com.w3p.api.config.proxy.MessageFlowProxy.Node
import com.w3p.api.config.proxy.MessageFlowProxy.NodeConnection
import com.w3p.api.config.proxy.SharedLibraryProxy
import com.w3p.im.iib.mon.monitor.profile.groovy.CreateMonitoringProfile
import com.w3p.im.iib.mon.topology.model.AbstractTopology
import com.w3p.im.iib.mon.topology.model.Application
import com.w3p.im.iib.mon.topology.model.FlowNode
import com.w3p.im.iib.mon.topology.model.FlowNodeConnection
import com.w3p.im.iib.mon.topology.model.IntegrationServer
import com.w3p.im.iib.mon.topology.model.MessageFlow
import com.w3p.im.iib.mon.topology.model.SharedLibrary
import com.w3p.im.iib.mon.topology.model.SubFlow

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.io.IOException

class TestCreateMonitoringProfile {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCanCreateMonitoringProfileAF01() {
		CreateMonitoringProfile createProfile = new CreateMonitoringProfile()
		assertNotNull(createProfile)
		IntegrationServer is = createIntSvr("testIS")
		assertEquals("testIS", is.getName())
		// Create App
		Application app = createApplication("testApp")
		assertEquals("testApp", app.getName())
		MessageFlow mf = createAF01_HTTP_Input("AF01_HTTP_Input")
		assertEquals("AF01_HTTP_Input", mf.getName())		
		app.addTopologyItem(mf)
		is.addTopologyItem(app)
		// Create Shared Lib
		SharedLibrary sl = createSharedLib("testLib")
		assertEquals("testLib", sl.getName())
		SubFlow sf = createSF_Handle_Exception("SF_Handle_Exception")
		assertEquals("SF_Handle_Exception", sf.getName())
		sl.addTopologyItem(sf)
		is.addTopologyItem(sl)
		// Validate  MessageFlow config is as expected
		validateAF01Config(is)
		
//		def xsd = 'C:/Apps/IBM/IIB/10.0.0.8/server/sample/RecordReplay/MonitoringProfile.xsd'
//		def factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
//		File schemaLocation = new File(xsd);
//		def schema = factory.newSchema(new StreamSource(new FileReader(schemaLocation)))
//		def validator = schema.newValidator()
		
		for (Application appl : is.getApplications()) {
			for (MessageFlow mfl : appl.getMessageFlows()) {
				String monitoringProfile =  new CreateMonitoringProfile().createForFlow(is, mfl, null, null);
				println monitoringProfile //TODO parse this to Java in order to verify the  Mon Profile created.
				try {
//					Path profilePath = Files.write(Paths.get("E:\\w3p\\events\\njamsGen\\test", mf.getName()+"_MonitoringProfile.xml"), monitoringProfile.getBytes(), StandardOpenOption.CREATE);
					// Validate against xsd
//					def xmlFile = new File("E:\\w3p\\events\\njamsGen\\test", mf.getName()+"_MonitoringProfile.xml")
//					File xmlFile = profilePath.toFile()
//					validator.validate(new StreamSource(xmlFile))
				} catch (IOException e) {
					e.printStackTrace()
				} catch (Exception e) {
					e.printStackTrace()
				}
			}
		}
	}
	
//	@Test
	public void testCanCreateMonitoringProfileAF03() {
		CreateMonitoringProfile createProfile = new CreateMonitoringProfile()
		assertNotNull(createProfile)
		IntegrationServer is = createIntSvr("testIS")
		assertEquals("testIS", is.getName())
		// Create App
		Application app = createApplication("testApp")
		assertEquals("testApp", app.getName())
		MessageFlow mf = createAF03_MQ_to_HTTP_Reply("AF03_MQ_to_HTTP_Reply")
		assertEquals("AF03_MQ_to_HTTP_Reply", mf.getName())
		app.addTopologyItem(mf)
		is.addTopologyItem(app)
		// Create Shared Lib
		SharedLibrary sl = createSharedLib("testLib")
		assertEquals("testLib", sl.getName())
		SubFlow sf1 = createSF_Handle_Exception("SF_Handle_Exception")
		assertEquals("SF_Handle_Exception", sf1.getName())
		sl.addTopologyItem(sf1)
		SubFlow sf2 = createSF_IIB_Input_Basic("SF_IIB_Input_Basic")
		assertEquals("SF_IIB_Input_Basic", sf2.getName())
		sl.addTopologyItem(sf2)		
		is.addTopologyItem(sl)
		// Validate  MessageFlow config is as expected
		validateAF03Config(is)
		
		for (Application appl : is.getApplications()) {
			for (MessageFlow mfl : appl.getMessageFlows()) {
				String monitoringProfile =  new CreateMonitoringProfile().createForFlow(is, appl, mfl);
				println monitoringProfile //TODO parse this to Java in order to verify the  Mon Profile created.
//				try {
//					Files.write(Paths.get("E:\\w3p\\events\\njamsGen\\test", mf.getName()+"_MonitoringProfile.xml"), monitoringProfile.getBytes(), StandardOpenOption.CREATE);
//				} catch (IOException e) {
//					e.printStackTrace()
//				}
			}
		}
//		String monitoringProfile = new GenerateMonitoringProfiles().createForFlow(is)
//		println monitoringProfile //TODO parse this to Java in order to verify the  Mon Profile created.
//		Files.write(Paths.get("E:\\w3p\\events\\njamsGen", "AF03MonProfile.xml"), monitoringProfile.getBytes(), StandardOpenOption.CREATE);
	}

	private validateAF01Config(IntegrationServer is) {
		assertTrue(is.getApplications().size() == 1)
		assertEquals("testApp", is.getApplications().get(0).getName())
		Application appTest = is.getApplications().get(0)
		assertTrue(appTest.getMessageFlows().size() == 1)
		MessageFlow mfTest = appTest.getMessageFlows().get(0)
		assertEquals("AF01_HTTP_Input", mfTest.getName())
		List<FlowNode> nodes = mfTest.getFlowNodes()
		assertEquals(8, nodes.size())
		FlowNode inputNode = mfTest.getInputNode()
		assertEquals("HTTP Input", inputNode.name)
		List<String> targetNames = mfTest.getConnectionTargetNames()
		assertEquals(8, nodes.size())
	}

	private validateAF03Config(IntegrationServer is) {
		assertTrue(is.getApplications().size() == 1)
		assertEquals("testApp", is.getApplications().get(0).getName())
		Application appTest = is.getApplications().get(0)
		assertTrue(appTest.getMessageFlows().size() == 1)
		MessageFlow mfTest = appTest.getMessageFlows().get(0)
		assertEquals("AF03_MQ_to_HTTP_Reply", mfTest.getName())
		List<FlowNode> nodes = mfTest.getFlowNodes()
		assertEquals(4, nodes.size())
		FlowNode inputNode = mfTest.getInputNode()
		assertEquals("SF_IIB_Input_Basic_X", inputNode.name)
		List<String> targetNames = mfTest.getConnectionTargetNames()
		assertEquals(4, nodes.size())
	}
	
	private IntegrationServer createIntSvr(String name) {
		ExecutionGroupProxy isProxy = mock(ExecutionGroupProxy.class)
		when(isProxy.getName()).thenReturn(name)		
		IntegrationServer is = new IntegrationServer(isProxy);
		return is;
	}
	
	private Application createApplication(String name) {
		ApplicationProxy appProxy = mock(ApplicationProxy.class)
		when(appProxy.getName()).thenReturn(name)		
		Application app = new Application(appProxy);
		return app;
	}
	
	private SharedLibrary createSharedLib(String name) {
		SharedLibraryProxy slProxy = mock(SharedLibraryProxy.class)
		when(slProxy.getName()).thenReturn(name)
		SharedLibrary sl = new SharedLibrary(slProxy);
		return sl;
	}
	
	private SubFlow createSF_Handle_Exception(String name) {
		SubFlowProxy sfp = mock(SubFlowProxy.class)
		when(sfp.getName()).thenReturn(name)
		SubFlow sf = new SubFlow(sfp);
//		sf.setIncludeSchemaNames(false);
		// Create nodes
		List<FlowNodeConnection> connections = new ArrayList<>()
		connections.add(new FlowNodeConnection("Try Catch", "try", "SF_Handle_Exception", "in"))
		connections.add(new FlowNodeConnection("Try Catch", "catch", "Hard Failure", "in"))
		FlowNode fn = createFlowNodeAndConnections("Try Catch", null, "ComIbmTryCatchNode", connections)
		sf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("SF_Handle_Exception", "out", "FAILURE", "in"))
		connections.add(new FlowNodeConnection("SF_Handle_Exception", "out1", "MQ Reply", "in"))
		connections.add(new FlowNodeConnection("SF_Handle_Exception", "out2", "HTTP Reply", "in"))
		fn = createFlowNodeAndConnections("SF_Handle_Exception", null, "ComIbmComputeNode", connections)
		sf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("Hard Failure", "out", "Trace", "in"))
		fn = createFlowNodeAndConnections("Hard Failure", null, "ComIbmMQOutputNode", connections)
		sf.flowNodes.add(fn)
		
		connections.clear()
		fn = createFlowNodeAndConnections("FAILURE", null, "ComIbmMQOutputNode", connections)
		sf.flowNodes.add(fn)
		
		fn = createFlowNodeAndConnections("MQ Reply", null, "ComIbmMQReply", connections)
		sf.flowNodes.add(fn)
		
		fn = createFlowNodeAndConnections("HTTP Reply", null, "ComIbmWSReplyNode", connections)
		sf.flowNodes.add(fn)
		
		fn = createFlowNodeAndConnections("Trace", null, "ComIbmTraceNode", connections)
		sf.flowNodes.add(fn)
		
		return sf
	}
	
	private SubFlow createSF_IIB_Input_Basic(String name) {
		SubFlowProxy sfp = mock(SubFlowProxy.class)
		when(sfp.getName()).thenReturn(name)
		SubFlow sf = new SubFlow(sfp);
//		sf.setIncludeSchemaNames(false);
		// Create nodes
		List<FlowNodeConnection> connections = new ArrayList<>()
		connections.add(new FlowNodeConnection("Queue_Name_Promoted", "catch", "SF_Handle_Exception_X", "Input"))
		FlowNode fn = createFlowNodeAndConnections("Queue_Name_Promoted", null, "ComIbmMQInputNode", connections)
		sf.flowNodes.add(fn)
		
		connections.clear()
		fn = createFlowNodeAndConnections("Output", null, "OutputNode", connections)
		sf.flowNodes.add(fn)
		
		connections.clear()
		Properties properties = new Properties();
		properties.setProperty("subflowImplFile", "PaH_SubFlowLib.SF_Handle_Exception.subflow")
		properties.setProperty("subflowURI", "/apiv1/executiongroups/njamsTesting/sharedlibraries/PetsAtHomeLibrary/subflows/PaH_SubFlowLib.SF_Handle_Exception")		
		fn = createFlowNodeAndConnections("SF_Handle_Exception_X", properties, "SubFlowNode", connections)
		sf.flowNodes.add(fn)
		
		return sf
	}
	
	private MessageFlow createAF01_HTTP_Input(String name) {
		MessageFlowProxy mfp = mock(MessageFlowProxy.class)
		when(mfp.getName()).thenReturn(name)
		MessageFlow mf = new MessageFlow(mfp);
//		mf.setIncludeSchemaNames(false);
		// Create nodes
		List<FlowNodeConnection> connections = new ArrayList<>()
		connections.add(new FlowNodeConnection("HTTP Input", "catch", "SF_Handle_Exception_X", "Input"))
		connections.add(new FlowNodeConnection("HTTP Input", "out", "Flow Order", "in"))
		FlowNode fn = createFlowNodeAndConnections("HTTP Input", null, "ComIbmWSInputNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		Properties properties = new Properties();
		properties.setProperty("subflowImplFile", "PaH_SubFlowLib.SF_Handle_Exception.subflow")
		properties.setProperty("subflowURI", "/apiv1/executiongroups/njamsTesting/sharedlibraries/PetsAtHomeLibrary/subflows/PaH_SubFlowLib.SF_Handle_Exception")		
		fn = createFlowNodeAndConnections("SF_Handle_Exception_X", properties, "SubFlowNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("SOAP Extract", "out", "MQ Output", "in"))
		fn = createFlowNodeAndConnections("SOAP Extract", null, "ComIbmSOAPExtractNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("BuildMessage", "out", "MQ Output", "in"))
		connections.add(new FlowNodeConnection("BuildMessage", "out1", "SOAP Extract", "in"))
		fn = createFlowNodeAndConnections("BuildMessage", null, "ComIbmComputeNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		fn = createFlowNodeAndConnections("MQ Output", null, "ComIbmMQOutputNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("Flow Order", "first", "BuildMessage", "in"))
		connections.add(new FlowNodeConnection("Flow Order", "second", "BuildReplyOK", "in"))
		fn = createFlowNodeAndConnections("Flow Order", null, "ComIbmFlowOrderNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		fn = createFlowNodeAndConnections("HTTP Reply", null, "ComIbmWSReplyNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("BuildReplyOK", "out", "HTTP Reply", "in"))
		fn = createFlowNodeAndConnections("BuildReplyOK", null, "ComIbmComputeNode", connections)
		mf.flowNodes.add(fn)		
		
		return mf;
	}
	
	private MessageFlow createAF03_MQ_to_HTTP_Reply(String name) {
		MessageFlowProxy mfp = mock(MessageFlowProxy.class)
		when(mfp.getName()).thenReturn(name)
		MessageFlow mf = new MessageFlow(mfp);
//		mf.setIncludeSchemaNames(false);
		// Create nodes
		List<FlowNodeConnection> connections = new ArrayList<>()

		Properties properties = new Properties();
		properties.setProperty("subflowImplFile", "PaH_SubFlowLib.SF_IIB_Input_Basic.subflow")
		properties.setProperty("subflowURI", "/apiv1/executiongroups/njamsTesting/sharedlibraries/PetsAtHomeLibrary/subflows/PaH_SubFlowLib.SF_IIB_Input_Basic")
		connections.add(new FlowNodeConnection("SF_IIB_Input_Basic_X", "output", "BuildMessage", "in"))
		FlowNode fn = createFlowNodeAndConnections("SF_IIB_Input_Basic_X", properties, "SubFlowNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("BuildMessage", "out", "HTTP Reply", "in"))
		connections.add(new FlowNodeConnection("BuildMessage", "out1", "Add SOAP Envelope", "in"))
		fn = createFlowNodeAndConnections("BuildMessage", null, "ComIbmComputeNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		connections.add(new FlowNodeConnection("Add SOAP Envelope", "out", "HTTP Reply", "in"))
		fn = createFlowNodeAndConnections("Add SOAP Envelope", null, "ComIbmSOAPEnvelopeNode", connections)
		mf.flowNodes.add(fn)
		
		connections.clear()
		fn = createFlowNodeAndConnections("HTTP Reply", null, "ComIbmWSReplyNode", connections)
		mf.flowNodes.add(fn)
		
		return mf;
	}
	
	private FlowNode createFlowNodeAndConnections(String name, Properties properties, String type, List<FlowNodeConnection> connections) {
		MessageFlowProxy.Node node = mock(MessageFlowProxy.Node.class)
		when(node.getName()).thenReturn(name)
		when(node.getProperties()).thenReturn(properties)
		when(node.getType()).thenReturn(type)
		when(node.getType()).thenReturn(type)
		FlowNode fn = new FlowNode(node)
		connections.each {conn ->
			fn.getFlowNodeTargetConnections().add(conn)
		}
		
		return fn
	}

}
