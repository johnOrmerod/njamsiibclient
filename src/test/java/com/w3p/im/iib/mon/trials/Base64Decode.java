package com.w3p.im.iib.mon.trials;
import java.io.UnsupportedEncodingException;

import jakarta.xml.bind.DatatypeConverter;

public class Base64Decode {
    public static void main(String[] args) throws UnsupportedEncodingException {
        // IIB 9 Event payload 
    	String hello64 = 
    			"/v8APAA/AHgAbQBsACAAdgBlAHIAcwBpAG8AbgA9ACIAMQAuADAAIgA/AD4APABQAGEAZwBlAHIAPgA8AFQAZQB4AHQAPgBIAGUAbABsAG8AIABXAG8AcgBsAGQAIABQAG8AdwBlAHIAZQBkACAAYgB5ACAASQBCAE0ALgA8AC8AVABlAHgAdAA+ADwALwBQAGEAZwBlAHIAPg==";
//    			"ADwAPwB4AG0AbAAgAHYAZQByAHMAaQBvAG4APQAiADEALgAwACIAIABlAG4AYwBvAGQAaQBuAGcAPQAiAFUAVABGAC0AOAAiAD8APgANAAoAPABQAGEAZwBlAHIAPgANAAoAPABUAGUAeAB0AD4ASABlAGwAbABvACAASwBpAHQAdAB5ACAASwBpAHQAdAB5ADwALwBUAGUAeAB0AD4ADQAKADwALwBQAGEAZwBlAHIAPg==";
//        		"TUQgIAIAAAAAAAAACAAAAP////8AAAAAEQEAALAEAABNUVNUUiAgIAAAAAABAAAAQU1RIElCOVFNR1IgICAgICiBrVggSxACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIElCOVFNR1IgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEpvaG5PICAgICAgIBYBBRUAAADSseghUFu9IIrjUPLsAwAAAAAAAAAAAAALICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAcAAAAamFyICAgICAgICAgICAgICAgICAgICAgICAgIDIwMTcwMzA0MjAzOTQxODYgICAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAA/////wA8AD8AeABtAGwAIAB2AGUAcgBzAGkAbwBuAD0AIgAxAC4AMAAiACAAZQBuAGMAbwBkAGkAbgBnAD0AIgBVAFQARgAtADgAIgA/AD4ADQAKADwAUABhAGcAZQByAD4ADQAKADwAVABlAHgAdAA+AEgAZQBsAGwAbwAgAHkAbwB1ADwALwBUAGUAeAB0AD4ADQAKADwALwBQAGEAZwBlAHIAPg==";
//    	String helloHex = "003c003f0078006d006c002000760065007200730069006f006e003d00220031002e0030002200200065006e0063006f00640069006e0067003d0022005500540046002d00380022003f003e000d000a003c00500061006700650072003e000d000a003c0054006500780074003e00480065006c006c006f0020004b0069007400740079003c002f0054006500780074003e000d000a003c002f00500061006700650072003e";
        //
        // Decode a previously encoded string using decodeBase64 method and
        // passing the byte[] of the encoded string.
        //
    	
//    	byte[] bytes = DatatypeConverter.parseHexBinary(helloHex);
//    	String body = new String(bytes, "UTF-16"); 
    	
        byte[] decoded = DatatypeConverter.parseBase64Binary(hello64); //Base64.decodeBase64(hello.getBytes());
        String decodedString = new String(decoded, "UTF-16");

        //
        // Print the decoded array
        //
//        System.out.println(Arrays.toString(decoded));

        //
        // Convert the decoded byte[] back to the original string and print
        // the result.
        //

        System.out.println(decodedString); //body);
        System.out.println("pause");
//        int start = decodedString.indexOf("?");
//        String xml = decodedString.substring(start);
//        System.out.println("xml: "+xml);
    }
}
