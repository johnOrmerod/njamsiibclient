#!/bin/bash
PORT=$1
if [ -z "$PORT" ]; 
	then 
		PORT=8000
fi	
echo Wiremock $PORT
java -jar wiremock-standalone-2.23.2.jar --bind-address localhost --port $PORT --root-dir mocks --no-request-journal